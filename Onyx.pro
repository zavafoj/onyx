#-------------------------------------------------
#
# Project created by QtCreator 2015-01-19T21:07:24
#
#-------------------------------------------------

QT       += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG  += c++11

TARGET = Onyx
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        core/communication/frame.cpp \
        3rdpart/qtsingleapplication/qtlocalpeer.cpp \
        3rdpart/qtsingleapplication/qtlockedfile.cpp \
        3rdpart/qtsingleapplication/qtlockedfile_unix.cpp \
        3rdpart/qtsingleapplication/qtlockedfile_win.cpp \
        3rdpart/qtsingleapplication/qtsingleapplication.cpp \
        3rdpart/qtsingleapplication/qtsinglecoreapplication.cpp \
        widgets/components/optionswidget.cpp \
        core/structure/elements/object.cpp \
        core/structure/elements/train.cpp \
        core/structure/elements/balisa.cpp \
        core/structure/event.cpp \
        widgets/gfx/graphicsview.cpp \
        widgets/gfx/graphicsscene.cpp \
        core/structure/elements/crossover.cpp \
        core/structure/elements/semaphore.cpp \
        core/structure/elements/objectgroup.cpp \
        widgets/gfx/elements/mapbackground.cpp \
        widgets/components/draggabletreeview.cpp \
        core/structure/elements/objectitemmodel.cpp \
        widgets/gfx/elements/abstractgraphicselement.cpp \
        widgets/mapeditor.cpp \
        core/structure/elements/rail.cpp \
        widgets/gfx/elements/trainelement.cpp \
        widgets/gfx/elements/endpart.cpp \
        widgets/gfx/elements/railelement.cpp \
        widgets/gfx/elements/bezierpoint.cpp \
        widgets/gfx/elements/gridfitablegraphicsitem.cpp \
        widgets/gfx/elements/abstractrailelement.cpp \
        widgets/gfx/elements/crossoverelement.cpp \
        widgets/gfx/elements/labelelement.cpp \
        widgets/gfx/elements/directionsign.cpp \
        widgets/gfx/elements/terminatorelement.cpp \
        core/structure/elements/terminator.cpp \
        widgets/gfx/elements/signalelement.cpp \
        widgets/gfx/elements/semaphoreelement.cpp \
        widgets/gfx/elements/balisaelement.cpp \
        core/structure/elements/station.cpp \
        widgets/gfx/elements/stationelement.cpp \
        widgets/components/editorbutton.cpp \
        widgets/components/buttonbox.cpp \
        core/structure/elements/rootitem.cpp \
        widgets/components/objectpropertywidget.cpp \
        core/structure/elements/route.cpp \
        core/structure/elements/routemanager.cpp \
        widgets/components/serviceswidget.cpp \
        widgets/components/messageswidget.cpp \
        widgets/components/consoleinput.cpp \
    widgets/components/consolewidget.cpp \
    core/structure/elements/routeinspector.cpp \
    widgets/gfx/elements/trainnode.cpp \
    widgets/components/traindirectionbuttonbox.cpp \
    widgets/gfx/elements/detectionrect.cpp \
    core/structure/node.cpp

HEADERS  += mainwindow.h \
        core/communication/frame.h \
        3rdpart/qtsingleapplication/qtlocalpeer.h \
        3rdpart/qtsingleapplication/qtlockedfile.h \
        3rdpart/qtsingleapplication/qtsingleapplication.h \
        3rdpart/qtsingleapplication/qtsinglecoreapplication.h \
        widgets/components/optionswidget.h \
        core/structure/elements/object.h \
        core/structure/elements/train.h \
        core/structure/elements/balisa.h \
        core/structure/event.h \
        widgets/gfx/graphicsview.h \
        widgets/gfx/graphicsscene.h \
        core/structure/elements/crossover.h \
        core/structure/elements/semaphore.h \
        core/structure/elements/objectgroup.h \
        core/structure/elements/elements.h \
        widgets/gfx/elements/mapbackground.h \
        widgets/components/draggabletreeview.h \
        core/structure/elements/objectitemmodel.h \
        widgets/gfx/elements/abstractgraphicselement.h \
        widgets/mapeditor.h \
        core/structure/elements/rail.h \
        widgets/gfx/elements/trainelement.h \
        widgets/gfx/elements/endpart.h \
        widgets/gfx/elements/railelement.h \
        widgets/gfx/elements/bezierpoint.h \
        widgets/gfx/elements/gridfitablegraphicsitem.h \
        widgets/gfx/elements/abstractrailelement.h \
        widgets/gfx/elements/crossoverelement.h \
        widgets/gfx/elements/labelelement.h \
        widgets/gfx/elements/directionsign.h \
        widgets/gfx/elements/terminatorelement.h \
        core/structure/elements/terminator.h \
        widgets/gfx/elements/signalelement.h \
        widgets/gfx/elements/semaphoreelement.h \
        widgets/gfx/elements/balisaelement.h \
        core/structure/elements/station.h \
        widgets/gfx/elements/stationelement.h \
        widgets/components/editorbutton.h \
        widgets/components/buttonbox.h \
        core/structure/elements/rootitem.h \
        widgets/gfx/elements/graphicselements.h \
        widgets/components/objectpropertywidget.h \
        core/structure/elements/route.h \
        core/structure/elements/routemanager.h \
        widgets/components/serviceswidget.h \
        widgets/components/messageswidget.h \
        widgets/components/consoleinput.h \
    widgets/components/consolewidget.h \
    core/structure/elements/routeinspector.h \
    widgets/gfx/elements/trainnode.h \
    widgets/components/traindirectionbuttonbox.h \
    widgets/gfx/elements/detectionrect.h \
    core/structure/node.h

FORMS    += mainwindow.ui \
        widgets/components/optionswidget.ui \
        widgets/mapeditor.ui

INCLUDEPATH += $$PWD/3rdpart/qtsingleapplication


gcc: DESTDIR    =       $$OUT_PWD/../bin
msvc: DESTDIR   =       $$OUT_PWD/../binmsvc
OBJECTS_DIR     =       $$OUT_PWD/.obj
MOC_DIR         =       $$OUT_PWD/.moc
RCC_DIR         =       $$OUT_PWD/.rcc
UI_DIR          =       $$OUT_PWD/.gui

TRANSLATIONS        = \
                        translations/en_US.ts \
                        translations/pl_PL.ts

translations.path    = $${DESTDIR}/translations
translations.files   += translations/pl_PL.qm \
                        translations/en_US.qm

stylesheets.path    =   $${DESTDIR}/stylesheets
stylesheets.files +=   stylesheets/dark.css

INSTALLS += translations stylesheets

#win32:RC_ICONS += $$PWD/icon.ico

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    stylesheets/dark.css

DISTFILES += \
    papers/readme.md \
    map/PKM.pkm \
    map/PKM.xml \
    papers/praca_mgr.doc \
    uml/symulacja.jpg \
    uml/observer.jpg \
    uml/Movement.png \
    uml/ActivityDiagram1.png \
    uml/buttonbox.png \
    uml/sem.png \
    uml/ce.png \
    uml/re.png \
    uml/ageare.png \
    uml/architektura.png \
    uml/metoda_wytworcza.png \
    uml/mvc.png \
    uml/twozenie_komponentu.png \
    uml/Moveemnt.mdj \
    uml/Untitled.mdj \
    uml/main.mdj \
    uml/symulacja.mdj \
    uml/buttonbox.mdj \
    uml/ce.mdj \
    uml/sem.mdj \
    uml/re.mdj \
    uml/age, are.mdj \
    uml/struktura.mdj \
    uml/metoda wytw�rcza.mdj \
    uml/mvc.mdj \
    uml/tworzenie komponentu.mdj \
    uml/observer.mdj \
    uml/lic \
    papers/streszczenie.doc \
    papers/praca_mgr-ang.doc \
    papers/Fin/Virtual Model of PKM Laboratory_full.doc
