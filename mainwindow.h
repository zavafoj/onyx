#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QKeyEvent>

#include "widgets/mapeditor.h"
#include "widgets/components/consolewidget.h"
#include "core/structure/elements/objectitemmodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

public slots:
    void messageReceived(const QString &message);
    void installTranslations();
    void loadTranslation(const QString &translationFileName);
    void loadGlobalSettings();

protected slots:
    // envoronment
    void setFullScreen(bool fullscreen);
    void loadStylesheet(const QString &fileName);
    void stylesheetLoadRequested();
    void showAbout();
    void zoomIn();
    void zoomOut();
    void showMapEditor();
    void hideMapEditor();

    // configuration
    void clearConfiguration(bool ask = true);
    void saveConfiguration();
    void loadConfiguration();
    void handleOnReadException(const QString fileName);
    void loadMapFromEditor();

    // simulation
    void startSimulationClicked();
    void stopSimulationClicked();
    void setFontClicked();
    void translationChangeClicked();
    void setSimulationTick();
    void updateTimer();
    void setTime();

protected:
    void closeEvent(QCloseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void changeEvent(QEvent *event);
    void updateGlobalSetting(const QString &key, const QVariant &value);
    void setOptionsEnabled(bool enable);

private:
    Ui::MainWindow *ui;
    bool _fullscreen;
    QSystemTrayIcon *_trayIcon;
    RootItem *_rootItem;
    ObjectItemModel *_model;
    MapEditor *_mapEditor;
    ConsoleWidget *_console;
    QTimer *_simulationTimer;
};

#endif // MAINWINDOW_H
