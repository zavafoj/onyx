#ifndef GRAPHICSSCENE_H
#define GRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneWheelEvent>
#include <QGraphicsSceneDragDropEvent>
#include <QContextMenuEvent>
#include <QKeyEvent>
#include <QHash>
#include <QPropertyAnimation>

#include "elements/graphicselements.h"

#include "../../core/structure/elements/routeinspector.h"

//!
//! \brief The GraphicsScene class
//!
class GraphicsScene : public QGraphicsScene
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")
    Q_PROPERTY(QPointF centerPosition READ centerPosition WRITE setCenterPosition NOTIFY centerPositionChanged)

public:
    explicit GraphicsScene(QObject *parent = 0);
    virtual ~GraphicsScene();

    QPointF centerPosition() const;
    bool checkConnections();
    void uncheckConnections();

    void hideNodePoints(bool hide = true);

    void buildBranch(EndPart *node,
                     Route *currentRoute);

    void createRoutes();
    void clearRoutes();

    void findRouteByObject(Train *train, bool random = true) throw (QString);

    bool locked() const;
    void setLocked(bool locked);
    void lock();
    Route *availableRouteForStartingNode(Object *o);
    Route *availableRouteForEndingNode(Object *o);

    QHash <QObject*, QGraphicsItem*> _elements;
    QHash <Object*, QList<Route*> > _routesForObject;
    QList <TrainElement*> _trains;

    QGraphicsItem *focusedTrain() const;
    void setFocusedTrain(QGraphicsItem *focusedTrain);

signals:
    //!
    //! \brief centerPositionChanged
    //! \param centerPosition
    //!
    void centerPositionChanged(const QPointF &centerPosition);
    //!
    //! \brief objectSelected
    //! \param object
    //!
    void objectSelected(Object *object);
    //!
    //! \brief stopDemanded
    //!
    void stopDemanded();

public slots:
    void setCenterPosition(const QPointF &centerPosition);
    void hoverObject(Object *object);
    void placeElement(Object *object);
    void clear();
    void tick();
    void assignPathToTrain(Route *route, TrainElement *train);
    void showTrainRoute(Route *route);
    void advance();
    void demandStop(const QString &reason);
    void findAnoherPath(Train *train);
    void tryConnect(Train *train);
    void tryDisconnect(Train *train);

protected slots:
    void objectRemoved(QObject *object);
    void updateRequested();

protected:
    template<class ModelClass, class GraphicsClass>
    GraphicsClass* createElement(Object *o)
    {
        ModelClass *newElement = qobject_cast<ModelClass*>(o);
        if (newElement)
        {
            GraphicsClass *newGraphicsElement = new GraphicsClass(newElement);
            newGraphicsElement->setPos(o->position());
            this->addItem(newGraphicsElement);
            return (GraphicsClass*) newGraphicsElement;
        }
        else
            return nullptr;
    }

    void keyPressEvent(QKeyEvent *event);
    void addRouteForObject(Route *r, Object *o);

private:
    QPointF _centerPosition;
    QPropertyAnimation *_centerAnimation;
    bool _locked;
    QList <Route*> _routes;
    RouteInspector *_inspector;
    int it;
    QGraphicsItem *_focusedTrain;
};

#endif // GRAPHICSSCENE_H`
