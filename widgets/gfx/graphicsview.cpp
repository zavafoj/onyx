#include "graphicsview.h"
#include <qmath.h>
#include <QDebug>
#include <QScrollBar>
#include "../components/buttonbox.h"

//!
//! \brief GraphicsView::GraphicsView
//! \param parent
//!
GraphicsView::GraphicsView(QWidget *parent) :
    QGraphicsView(parent),
    _scaleFactor(1.0),
    _rotationEnabled(false),
    _rootItem(nullptr),
    _currentType(Object::OT_Unknown)
{
    this->_scene = new GraphicsScene(this);
    this->setScene(this->_scene);

    this->connect(this->_scene,         SIGNAL(objectSelected(Object*)),
                  this,                 SIGNAL(objectSelected(Object*)));

    // modification
    this->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
    this->setResizeAnchor(QGraphicsView::NoAnchor);
    this->setMouseTracking(true);

    // render options
    this->setRenderHints(QPainter::Antialiasing);
    this->setDragMode(QGraphicsView::RubberBandDrag);
}

//!
//! \brief GraphicsView::rootItem
//! \return
//!
RootItem *GraphicsView::rootItem() const
{
    return this->_rootItem;
}

//!
//! \brief GraphicsView::setRootItem
//! \param rootItem
//!
void GraphicsView::setRootItem(RootItem *rootItem)
{
    this->_rootItem = rootItem;

    this->connect(this->_rootItem,          SIGNAL(elementLoaded(Object*)),
                  this->_scene,             SLOT(placeElement(Object*)));
}

//!
//! \brief GraphicsView::currentType
//! \return
//!
Object::ObjectType GraphicsView::currentType() const
{
    return this->_currentType;
}

//!
//! \brief GraphicsView::scene
//! \return
//!
GraphicsScene *GraphicsView::getScene() const
{
    return this->_scene;
}

//!
//! \brief GraphicsView::reset
//!
void GraphicsView::reset()
{
    this->_rotationEnabled = false;
    this->centerOn(0, 0);
    this->resetMatrix();
}

//!
//! \brief GraphicsView::setCurrentType
//! \param currentType
//!
void GraphicsView::setCurrentType(const Object::ObjectType &currentType)
{
    this->_currentType = currentType;
}

//!
//! \brief GraphicsView::mousePressEvent
//! \param event
//!
void GraphicsView::mousePressEvent(QMouseEvent *event)
{
    QGraphicsItem *selectedItem = this->itemAt(event->pos());
    if (selectedItem)
    {
        switch (selectedItem->type())
        {
        case QGraphicsItem::UserType + Object::OT_Rail:
        case QGraphicsItem::UserType + Object::OT_Semaphore:
        case QGraphicsItem::UserType + Object::OT_Station:
            emit this->objectSelected(((RailElement*)selectedItem)->object());
            break;
        case QGraphicsItem::UserType + Object::OT_Terminator:
            emit this->objectSelected(((TerminatorElement*)selectedItem)->object());
            break;
        case QGraphicsItem::UserType + Object::OT_Crossover:
            emit this->objectSelected(((CrossoverElement*)selectedItem)->object());
            break;
        case QGraphicsItem::UserType + Object::OT_Balisa:
            emit this->objectSelected(((BalisaElement*)selectedItem)->balisa());
            break;
        case QGraphicsItem::UserType + Object::OT_Train:
            emit this->objectSelected(((TrainElement*)selectedItem)->train());
            ((TrainElement*)selectedItem)->showTrainRoute();
            break;
        default:
            this->_scene->setFocusedTrain(nullptr);
            emit this->objectSelected(nullptr);
            break;
        }
    }
    else
    {
        this->_scene->setFocusedTrain(nullptr);
        emit this->objectSelected(nullptr);
        switch (event->button())
        {
        case Qt::LeftButton:
        {
            if (this->_currentType != Object::OT_Unknown)
            {
                QPointF pos = this->mapToScene(event->pos());
                int posx = pos.x();
                posx -= posx % GRID_SIZE;
                pos.setX(posx);

                int posy = pos.y();
                posy -= posy % GRID_SIZE;
                pos.setY(posy);

                Object *createdObject = this->_rootItem->createElement(this->_currentType,
                                                                       pos);
                if (createdObject)
                {
                    this->_scene->placeElement(createdObject);
                    emit this->elementPlaced();
                }
            }
        }
            break;
        case Qt::RightButton:
        {
            this->_initialPosition = event->pos();
            this->_rotationEnabled = true;
            this->setCursor(Qt::SizeBDiagCursor);
        }
            break;
        case Qt::MidButton:
        {

        }
        default:
            break;
        }
    }
    QGraphicsView::mousePressEvent(event);
}

//!
//! \brief GraphicsView::mouseReleaseEvent
//! \param event
//!
void GraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    switch (event->button())
    {
    case Qt::RightButton:
        this->_rotationEnabled = false;
        break;
    default:
        break;
    }
    this->setCursor(Qt::ArrowCursor);

    QGraphicsView::mouseReleaseEvent(event);
}

//!
//! \brief GraphicsView::mouseMoveEvent
//! \param event
//!
void GraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    if (this->_rotationEnabled)
    {
        QPoint center = this->viewport()->geometry().center();
        QPoint postDelta = event->pos();

        qreal angle = qAtan2(postDelta.y() - center.y(),
                             postDelta.x() - center.x())
                - qAtan2(this->_initialPosition.y() - center.y(),
                         this->_initialPosition.x() - center.x());

        this->rotate(angle * 57);

        this->_initialPosition = event->pos();
    }
    else
    {
        QGraphicsView::mouseMoveEvent(event);
    }
}

//!
//! \brief GraphicsView::mouseDoubleClickEvent
//! \param event
//!
void GraphicsView::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        this->reset();
    }
    else if (event->button() == Qt::LeftButton)
    {
        QGraphicsItem *selectedItem = this->itemAt(event->pos());
        if (!selectedItem)
        {
            this->_scene->setFocusedTrain(nullptr);
            emit this->objectSelected(nullptr);
            return;
        }

        switch (selectedItem->type())
        {
        case QGraphicsItem::UserType + Object::OT_Rail:
        case QGraphicsItem::UserType + Object::OT_Semaphore:
        case QGraphicsItem::UserType + Object::OT_Station:
            this->_scene->hoverObject(((RailElement*)selectedItem)->object());
            break;
        case QGraphicsItem::UserType + Object::OT_Terminator:
            this->_scene->hoverObject(((TerminatorElement*)selectedItem)->object());
            break;
        case QGraphicsItem::UserType + Object::OT_Crossover:
            this->_scene->hoverObject(((CrossoverElement*)selectedItem)->object());
            break;
        case QGraphicsItem::UserType + Object::OT_Balisa:
            this->_scene->hoverObject(((BalisaElement*)selectedItem)->balisa());
            break;
        case QGraphicsItem::UserType + Object::OT_Train:
            this->_scene->hoverObject(((TrainElement*)selectedItem)->train());
            break;
        default:
            return;
        }
    }
    QGraphicsView::mouseDoubleClickEvent(event);
}

//!
//! \brief GraphicsView::wheelEvent
//! \param event
//!
void GraphicsView::wheelEvent(QWheelEvent *event)
{
    this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    qreal scaleFactor = qPow(2.0, event->delta() / 240.0);
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if(0.0005 < factor && factor < 100)
        scale(scaleFactor, scaleFactor);
    this->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
}
