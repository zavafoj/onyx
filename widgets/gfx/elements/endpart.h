#ifndef ENDPART_H
#define ENDPART_H

#include "gridfitablegraphicsitem.h"

class EndPart : public GridFitableGraphicsItem
{
public:
    explicit EndPart(QGraphicsItem *parent = 0);
    virtual ~EndPart();

    QRectF boundingRect() const
    {
        return QRectF(-4.5, -4.5, 9, 9);
    }

    virtual int type() const
    {
        return UserType + 69;
    }

    bool valid() const;
    void setValid(bool valid);
    bool checkValid();

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

    QVariant itemChange(GraphicsItemChange change,
                        const QVariant &value);

    bool isCloseToZero(int x);

private:
    bool _valid;
    QPointF _previousPos;
};

#endif // ENDPART_H
