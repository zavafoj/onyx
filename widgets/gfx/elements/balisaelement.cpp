#include "balisaelement.h"
#include <QMenu>
#include <QAction>
#include <QGraphicsScene>

//!
//! \brief BalisaElement::BalisaElement
//! \param b
//! \param parent
//!
BalisaElement::BalisaElement(Balisa *b, QGraphicsItem *parent) :
    QGraphicsItem(parent)
{
    this->_balisa = b;
    this->setFlag(QGraphicsItem::ItemIsMovable);
    this->setFlag(QGraphicsItem::ItemIsSelectable);
    this->setFlag(QGraphicsItem::ItemSendsGeometryChanges);

    this->setZValue(20001.0);
}

//!
//! \brief BalisaElement::~BalisaElement
//!
BalisaElement::~BalisaElement()
{
//    qDebug() << Q_FUNC_INFO;
    this->_balisa->deleteLater();
}

//!
//! \brief BalisaElement::lock
//! \param lock
//!
void BalisaElement::lock(bool lock)
{
    this->setFlag(QGraphicsItem::ItemIsMovable, !lock);
}

//!
//! \brief BalisaElement::itemChange
//! \param change
//! \param value
//! \return
//!
QVariant BalisaElement::itemChange(QGraphicsItem::GraphicsItemChange change,
                                   const QVariant &value)
{
    if (change == QGraphicsItem::ItemPositionHasChanged && scene() != nullptr)
    {
        this->_balisa->setPosition(value.toPointF());
    }

    return QGraphicsItem::itemChange(change, value);
}

//!
//! \brief BalisaElement::paint
//! \param painter
//!
void BalisaElement::paint(QPainter *painter,
                          const QStyleOptionGraphicsItem *,
                          QWidget *)
{
    painter->setPen(QPen(Qt::black));

    if (this->isSelected())
        painter->setBrush(QBrush(QColor(Qt::green).lighter()));
    else
        painter->setBrush(QBrush(QColor(Qt::blue).lighter()));

    painter->drawEllipse(QPointF(0.0, 0.0),
                         4.5, 4.5);

    painter->setBrush(QBrush(QColor(Qt::black)));
    painter->drawEllipse(QPointF(0.0, 0.0),
                         0.5, 0.5);
}

//!
//! \brief BalisaElement::contextMenuEvent
//! \param event
//!
void BalisaElement::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    qDebug() << Q_FUNC_INFO << event->pos();

    this->scene()->clearSelection();
    this->setSelected(true);

    QMenu m;
    m.setTitle(this->_balisa->metaObject()->className());
    m.addAction("Action #1");
    m.addAction("Action #2");
    m.addAction("Action #3");
    m.addAction("Action #4");
    m.exec(event->screenPos());
}

//!
//! \brief BalisaElement::balisa
//! \return
//!
Balisa *BalisaElement::balisa() const
{
    return _balisa;
}
