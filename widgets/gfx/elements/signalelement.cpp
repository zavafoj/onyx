#include "signalelement.h"
#include <QDebug>

//!
//! \brief SignalElement::~SignalElement
//!
SignalElement::SignalElement(Semaphore::SemaphoreStates state,
                             QGraphicsItem *parent) :
    QGraphicsItem(parent),
    _state(state)
{
    this->setZValue(-1);
}

//!
//! \brief SignalElement::~SignalElement
//!
SignalElement::~SignalElement()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief SignalElement::state
//! \return
//!
Semaphore::SemaphoreStates SignalElement::state() const
{
    return this->_state;
}

//!
//! \brief SignalElement::paint
//! \param painter
//!
void SignalElement::paint(QPainter *painter,
                          const QStyleOptionGraphicsItem *,
                          QWidget *)
{
    painter->setPen(QPen(Qt::black, 1.5));
    painter->drawLine(0.0, 0.0, 8.0, 0.0);
    switch (this->_state)
    {
    case Semaphore::SS_Green:
        painter->setBrush(QBrush(Qt::green));
        break;
    case Semaphore::SS_Yellow:
        painter->setBrush(QBrush(Qt::yellow));
        break;
    case Semaphore::SS_Red:
        painter->setBrush(QBrush(Qt::red));
        break;
    default:
        break;
    }

    painter->drawEllipse(QPoint(8.0, 0.0), 4, 4);
}

//!
//! \brief SignalElement::setMode
//! \param mode
//!
void SignalElement::setState(const Semaphore::SemaphoreStates &state)
{
    if (this->_state != state)
    {
        this->_state = state;
        this->update();
    }
}


