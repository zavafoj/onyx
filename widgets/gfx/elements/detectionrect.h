#ifndef DETECTIONRECT_H
#define DETECTIONRECT_H

#include <QGraphicsItem>
#include <QPainter>

#define DETECTION_RECT_ID 666

class DetectionRect : public QGraphicsItem
{
public:
    explicit DetectionRect(QGraphicsItem *parent = 0);

    QRectF boundingRect() const;

    virtual int type() const
    {
        return UserType + DETECTION_RECT_ID;
    }

    void setShown(bool shown);

    qreal a() const;
    void setA(qreal a);

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

private:
    QRectF _boundingRect;
    bool _shown;
};

#endif // DETECTIONRECT_H
