#ifndef BALISAELEMENT_H
#define BALISAELEMENT_H

#include <QGraphicsItem>
#include <QPainter>
#include <QWidget>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneContextMenuEvent>
#include "../../core/structure/elements/balisa.h"

class BalisaElement : public QGraphicsItem
{
public:
    explicit BalisaElement(Balisa *b, QGraphicsItem *parent = 0);
    virtual ~BalisaElement();

    QRectF boundingRect() const
    {
        return QRectF(-5, -5, 10, 10);
    }

    virtual int type() const
    {
        return UserType + Object::OT_Balisa;
    }

    void lock(bool lock);

    Balisa *balisa() const;

protected:
    QVariant itemChange(GraphicsItemChange change,
                        const QVariant &value);

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

private:
    Balisa *_balisa;
};

#endif // BALISAELEMENT_H
