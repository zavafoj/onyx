#include "terminatorelement.h"

//!
//! \brief TerminatorElement::TerminatorElement
//! \param terminator
//! \param parent
//!
TerminatorElement::TerminatorElement(Terminator *terminator, QGraphicsItem *parent):
    AbstractRailElement(terminator, parent)
{
    this->_branchNode = new EndPart(this);
    this->_branchNode->setPos(terminator->branchNodePosition());

    this->_boundingRect = QRectF(-15, -15, 30, 30);

    this->updateModel();
}

//!
//! \brief TerminatorElement::~TerminatorElement
//!
TerminatorElement::~TerminatorElement()
{
    this->_branchNode->setValid(false);

    this->updateModel();

    delete this->_branchNode;
}

//!
//! \brief TerminatorElement::availableConnections
//! \param node
//! \return
//!
QList<EndPart *> TerminatorElement::availableConnections(EndPart *node) const
{
    QList<EndPart *> elements;
    elements << this->_branchNode;
    return elements;
}

//!
//! \brief TerminatorElement::updateModel
//!
void TerminatorElement::updateModel()
{
    //                                Set Bounding Rect geometry
    // =============================================================================== //
    qreal xMin = qMin(this->_branchNode->x(), 0.0) - 5.0d;
    qreal yMin = qMin(this->_branchNode->y(), 0.0) - 5.0d;

    qreal xMax = qMax(this->_branchNode->x(), 0.0) + 5.0d;
    qreal yMax = qMax(this->_branchNode->y(), 0.0) + 5.0d;

    // =============================================================================== //

    //                                 Set Shape geometry
    // =============================================================================== //
    qreal dx = this->_branchNode->pos().x();
    qreal dy = this->_branchNode->pos().y();

    QPointF beginPointBotomRight =   QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF beginPointTopLeft    = - QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF beginPointTopRight   =   QPointF( SHAPEWIDTH, -SHAPEWIDTH);
    QPointF beginPointBottomLeft =   QPointF(-SHAPEWIDTH,  SHAPEWIDTH);

    QPointF endPointBotomRight = this->_branchNode->pos()  + QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF endPointTopLeft    = this->_branchNode->pos()  - QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF endPointTopRight   = this->_branchNode->pos()  + QPointF( SHAPEWIDTH, -SHAPEWIDTH);
    QPointF endPointBottomLeft = this->_branchNode->pos()  + QPointF(-SHAPEWIDTH,  SHAPEWIDTH);

    QPainterPath newShape;
    QPolygonF boundingPolygon;
    // Quarters - I and III
    if (qFuzzyCompare(dx, 0.0) || (dx > 0.0 && dy < 0.0) || (dx < 0.0 && dy > 0.0))
    {
        boundingPolygon << endPointTopLeft
                        << endPointBotomRight
                        << beginPointBotomRight
                        << beginPointTopLeft
                        << endPointTopLeft;
    }
    // Quarters - II and IV
    else if (qFuzzyCompare(dy, 0.0) || (dx < 0.0 && dy < 0.0) || (dx > 0.0 && dy > 0.0))
    {
        boundingPolygon << endPointBottomLeft
                        << endPointTopRight
                        << beginPointTopRight
                        << beginPointBottomLeft
                        << endPointBottomLeft;
    }
    newShape.addPolygon(boundingPolygon);

    // =============================================================================== //
    this->prepareGeometryChange();
    this->_shape = newShape;
    this->_boundingRect = QRectF(QPointF(xMin, yMin),
                                 QPointF(xMax, yMax));
    // =============================================================================== //

    Terminator *t = (Terminator *)this->_object;
    t->setBranchNodePosition(this->_branchNode->pos().toPoint());

    this->update();
}

//!
//! \brief TerminatorElement::lock
//! \param lock
//!
void TerminatorElement::lock(bool lock)
{
    this->_branchNode->setFlag(ItemIsMovable, !lock);

    this->setFlag(ItemIsMovable, !lock);
}

//!
//! \brief TerminatorElement::angle
//! \return
//!
qreal TerminatorElement::angle() const
{
    const QPointF branchNodePos = this->_branchNode->pos();

    return qAtan2(branchNodePos.y(),
                  branchNodePos.x()) * 180 / M_PI;
}

//!
//! \brief TerminatorElement::paint
//! \param painter
//!
void TerminatorElement::paint(QPainter *painter,
                              const QStyleOptionGraphicsItem */*option*/,
                              QWidget */*widget*/)
{
    if (isSelected())
        painter->setPen(QPen(QBrush(Qt::cyan), RAIL_WIDTH));
    else if (this->object()->isExhaused())
        painter->setPen(QPen(QBrush(Qt::red), RAIL_WIDTH));
    else
        painter->setPen(QPen(QBrush(Qt::darkCyan), RAIL_WIDTH));

    QLineF line(QPointF(),
               this->_branchNode->pos());

    painter->drawLine(line);

    QLineF orthogonal = line.normalVector();
    orthogonal.setLength(7.0);
    orthogonal.translate(orthogonal.p2()/-2.0);

    painter->setPen(QPen(QBrush(Qt::black), RAIL_WIDTH, Qt::SolidLine, Qt::RoundCap));
    painter->drawLine(orthogonal);
}

//!
//! \brief TerminatorElement::itemChange
//! \param change
//! \param value
//! \return
//!
QVariant TerminatorElement::itemChange(QGraphicsItem::GraphicsItemChange change,
                                       const QVariant &value)
{
    QVariant v = AbstractRailElement::itemChange(change, value);

    if (scene() != nullptr)
    {
        switch (change)
        {
        case QGraphicsItem::ItemPositionHasChanged:
        {
            this->_object->setPosition(value.toPointF());
        }
            break;
        default:
            break;
        }
    }
    return v;
}

//!
//! \brief TerminatorElement::branchNode
//! \return
//!
EndPart *TerminatorElement::branchNode() const
{
    return this->_branchNode;
}

//!
//! \brief TerminatorElement::setBranchNode
//! \param branchNode
//!
void TerminatorElement::setBranchNode(EndPart *branchNode)
{
    this->_branchNode = branchNode;
}


