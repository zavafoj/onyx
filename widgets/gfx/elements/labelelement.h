#ifndef LABELELEMENT_H
#define LABELELEMENT_H

#include <QGraphicsItem>
#include <QGraphicsSceneContextMenuEvent>
#include <QPainter>
#include "gridfitablegraphicsitem.h"

class LabelElement : public QGraphicsItem
{
public:
    explicit LabelElement(const QString &text = QString(),
                          QGraphicsItem *parent = 0);
    virtual ~LabelElement();

    QRectF boundingRect() const
    {
        return this->_boundingRect;
    }

    virtual int type() const
    {
        return UserType + 70;
    }

    void setText(const QString &text);
    QString text() const;

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

private:
    QRectF _boundingRect;
    QString _text;
};

#endif // LABELELEMENT_H
