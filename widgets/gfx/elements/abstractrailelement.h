#ifndef ABSTRACTRAILELEMENT_H
#define ABSTRACTRAILELEMENT_H

#include "abstractgraphicselement.h"
#include "endpart.h"
#define SHAPEWIDTH 3.0

//!
//! \brief The AbstractRailElement class
//!
class AbstractRailElement : public AbstractGraphicsElement
{
public:
    explicit AbstractRailElement(Object *object, QGraphicsItem *parent = 0);
    virtual ~AbstractRailElement();

    virtual QRectF boundingRect() const;
    virtual QPainterPath shape() const;

    virtual QList<EndPart *> availableConnections(EndPart *node = nullptr) const = 0;

    virtual void updateModel() = 0;

protected:
    virtual QVariant itemChange(QGraphicsItem::GraphicsItemChange change,
                        const QVariant &value);

protected:
    QRectF _boundingRect;
    QPainterPath _shape;
};

#endif // ABSTRACTRAILELEMENT_H
