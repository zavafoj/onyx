#ifndef TRAINNODE_H
#define TRAINNODE_H

#include <QGraphicsItem>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

class TrainNode : public QGraphicsItem
{

public:
    TrainNode(char type = 'f', QGraphicsItem *parent = 0);
    ~TrainNode();

    QRectF boundingRect() const
    {
        return QRectF(-0.25, -2.5, 0.5, 5);
    }

    virtual int type() const
    {
        return UserType + 72;
    }

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

private:
    char _type;
};

#endif // TRAINNODE_H
