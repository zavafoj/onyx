#include "directionsign.h"
#include <QPainter>
#include <QDebug>

//!
//! \brief DirectionSign::DirectionSign
//! \param parent
//!
DirectionSign::DirectionSign(QGraphicsItem *parent) :
    QGraphicsItem(parent)
{
    this->setZValue(1.0);
}

//!
//! \brief DirectionSign::~DirectionSign
//!
DirectionSign::~DirectionSign()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief DirectionSign::paint
//! \param painter
//!
void DirectionSign::paint(QPainter *painter,
                          const QStyleOptionGraphicsItem *,
                          QWidget *)
{
    painter->setPen(QPen(QColor("#F58E8E"), 0.25));
    painter->setBrush(QBrush(Qt::white));

    QPolygonF polygon;
    polygon.append(QPointF(-3.5, -3.0));
    polygon.append(QPointF(-3.5,  3.0));
    polygon.append(QPointF( 3.0,  0.0));
    painter->drawPolygon(polygon);
}

