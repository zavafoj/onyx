#ifndef TERMINATORELEMENT_H
#define TERMINATORELEMENT_H

#include <QGraphicsItem>
#include <QPainter>
#include <QWidget>

#include "abstractrailelement.h"
#include "endpart.h"
#include "../../core/structure/elements/terminator.h"

//!
//! \brief The TerminatorElement class
//!
class TerminatorElement : public AbstractRailElement
{
public:
    explicit TerminatorElement(Terminator *terminator, QGraphicsItem *parent = 0);
    virtual ~TerminatorElement();

    virtual QList<EndPart *> availableConnections(EndPart *node = nullptr) const;

    virtual void updateModel();
    virtual int type() const
    {
        return UserType + Object::OT_Terminator;
    }

    void lock (bool lock);

    static const QRectF availableRect()
    {
        return QRectF(-15, -15, 30, 30);
    }

    qreal angle() const;

    EndPart *branchNode() const;
    void setBranchNode(EndPart *branchNode);

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *,
               QWidget *);

    QVariant itemChange(QGraphicsItem::GraphicsItemChange change,
                    const QVariant &value);

private:
    EndPart *_branchNode;
    QPointF _safeBranchPosition;
};

#endif // TERMINATORELEMENT_H
