#include "semaphoreelement.h"
#include <QDebug>

#include <QMenu>
#include <QAction>

//!
//! \brief SemaphoreElement::SemaphoreElement
//! \param s
//! \param parent
//!
SemaphoreElement::SemaphoreElement(Semaphore *s, QGraphicsItem *parent) :
    RailElement(s, parent),
    _clock(0)
{
    this->_signal = new SignalElement(s->state(), this);

    this->_beginNode->setFlag(QGraphicsItem::ItemIsMovable, false);

    this->updateModel();
}

//!
//! \brief SemaphoreElement::~SemaphoreElement
//!
SemaphoreElement::~SemaphoreElement()
{
//    qDebug() << Q_FUNC_INFO;

    delete this->_signal;
}

//!
//! \brief SemaphoreElement::updateModel
//!
void SemaphoreElement::updateModel()
{
    RailElement::updateModel();

    qreal beginNodeX = this->_beginNode->x();
    qreal beginNodeY = this->_beginNode->y();

    qreal endNodeX = this->_endNode->x();
    qreal endNodeY = this->_endNode->y();

    qreal angle = qAtan2(endNodeY - beginNodeY,
                         endNodeX - beginNodeX) * 180 / M_PI;

    this->_signal->setRotation(angle + 90);

    // set signal element position (  Cx = Ax(1−X)+XBx, X-distance  ):
    this->_signal->setPos(endNodeX*(1-0.2) + 0.2*beginNodeX,
                          endNodeY*(1-0.2) + 0.2*beginNodeY);

    this->_signal->setState(((Semaphore*)this->_object)->state());

    this->update();
}

//!
//! \brief SemaphoreElement::isSimpleRail
//! \param node
//! \return
//!
bool SemaphoreElement::isSimpleRail(EndPart *node) const
{
    return node == this->_endNode;
}

//!
//! \brief SemaphoreElement::advance
//! \param phase
//!
void SemaphoreElement::advance(int phase)
{
    if (phase)
    {
        Semaphore *semaphore = (Semaphore*) this->object();

        switch (semaphore->state())
        {
        case Semaphore::SS_Yellow:
            if (this->_clock++ > 30)
            {
                this->setSemaphoreState(Semaphore::SS_Red);
                this->_clock = 0;
            }
            break;
        default:
            break;
        }
    }
}

//!
//! \brief SemaphoreElement::semaphoreState
//! \return
//!
Semaphore::SemaphoreStates SemaphoreElement::semaphoreState() const
{
    return ((Semaphore*)this->_object)->state();
}

//!
//! \brief SemaphoreElement::setSemaphoreState
//! \param state
//!
void SemaphoreElement::setSemaphoreState(const Semaphore::SemaphoreStates &state)
{
    ((Semaphore*)this->_object)->setState(state);

    this->_signal->setState(state);
}

//!
//! \brief SemaphoreElement::paint
//! \param painter
//!
void SemaphoreElement::paint(QPainter *painter,
                             const QStyleOptionGraphicsItem *, QWidget *)
{
    RailElement::paint(painter, nullptr, nullptr);
}

//!
//! \brief SemaphoreElement::contextMenuEvent
//! \param event
//!
void SemaphoreElement::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu m;
    QMenu menudefState(qApp->tr("Change State"), &m);
    QAction greenState(qApp->tr("GreenState"), &menudefState);
    greenState.setData(Semaphore::SS_Green);
    menudefState.addAction(&greenState);

    QAction yellowState(qApp->tr("YellowState"), &menudefState);
    yellowState.setData(Semaphore::SS_Yellow);
    menudefState.addAction(&yellowState);

    QAction redState(qApp->tr("RedState"), &menudefState);
    redState.setData(Semaphore::SS_Red);
    menudefState.addAction(&redState);

    m.addMenu(&menudefState);

    QAction *res = m.exec(event->screenPos());
    if (res)
    {
        this->setSemaphoreState((Semaphore::SemaphoreStates) res->data().toInt());
    }
}
