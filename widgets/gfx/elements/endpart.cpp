#include "endpart.h"
#include "abstractrailelement.h"
#include "terminatorelement.h"
#include "semaphoreelement.h"
#include "stationelement.h"
#include <QDebug>

//!
//! \brief EndPart::EndPart
//! \param parent
//!
EndPart::EndPart(QGraphicsItem *parent) :
    GridFitableGraphicsItem (parent),
    _valid(false)
{

}

//!
//! \brief EndPart::~EndPart
//!
EndPart::~EndPart()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief EndPart::paint
//! \param painter
//!
void EndPart::paint(QPainter *painter,
                    const QStyleOptionGraphicsItem *,
                    QWidget *)
{
    if (this->_valid)
        painter->setPen(Qt::green);
    else if (this->isSelected())
        painter->setPen(Qt::blue);
    else
        painter->setPen(Qt::red);

    painter->setBrush(Qt::white);
    painter->drawRect(-4.0, -4.0, 8.0, 8.0);
    painter->drawLine(QPointF(-3.5, -3.5), QPointF(3.5,  3.5));
    painter->drawLine(QPointF(-3.5,  3.5), QPointF(3.5, -3.5));
}

//!
//! \brief EndPart::itemChange
//! \param change
//! \param value
//! \return
//!
QVariant EndPart::itemChange(QGraphicsItem::GraphicsItemChange change,
                             const QVariant &value)
{
    QVariant v = GridFitableGraphicsItem::itemChange(change, value);
    if (scene() != nullptr)
    {
        switch (change)
        {
        case QGraphicsItem::ItemPositionChange:
        {
            // Terminator Exception
            TerminatorElement *parentTerminator = dynamic_cast<TerminatorElement*>(this->parentItem());
            if (parentTerminator)
            {
                QPointF point = v.toPointF();
                int x = point.x();
                int y = point.y();
                if (!parentTerminator->availableRect().contains(value.toPointF()))
                {
                    if      (isCloseToZero(x) && y > 0){ x = 0; y =  10;}
                    else if (isCloseToZero(x) && y < 0){ x = 0; y = -10;}

                    else if (x > 0 && isCloseToZero(y)){ x =  10; y = 0;}
                    else if (x < 0 && isCloseToZero(y)){ x = -10; y = 0;}

                    else if (x < 0 && y < 0) {x = -10; y = -10;}
                    else if (x < 0 && y > 0) {x = -10; y =  10;}
                    else if (x > 0 && y < 0) {x =  10; y = -10;}
                    else if (x > 0 && y > 0) {x =  10; y =  10;}

                    return QPoint(x, y);
                }
            }

            // Semaphore Exception
            SemaphoreElement *parentSemaphore = dynamic_cast<SemaphoreElement*>(this->parentItem());
            if (parentSemaphore)
            {
                QPointF point = v.toPointF();
                int x = point.x();
                int y = point.y();

                // Try to fit with external coordinates:
                if (x > 10)
                    x = 10;
                else if (x < -30)
                    x = -30;

                if (y > 20)
                    y = 20;
                else if (y < -20)
                    y = -20;

                return QPoint(x, y);
            }
        }
            break;
        case QGraphicsItem::ItemPositionHasChanged:
        {
            AbstractRailElement *parent = dynamic_cast<AbstractRailElement*>(this->parentItem());
            if (parent != nullptr)
            {
                parent->updateModel();
            }
        }
            break;
        default:
            break;
        }
    }

    return v;
}

//!
//! \brief EndPart::isCloseToZero
//! \param x
//! \return
//!
bool EndPart::isCloseToZero(int x)
{
    return qAbs(x - 5) <= 5;
}

//!
//! \brief EndPart::valid
//! \return
//!
bool EndPart::valid() const
{
    return this->_valid;
}

//!
//! \brief EndPart::setValid
//! \param valid
//!
void EndPart::setValid(bool valid)
{
    this->_valid = valid;

    if (this->parentItem())
        this->parentItem()->update();

}

//!
//! \brief EndPart::checkValid
//!
bool EndPart::checkValid()
{
    QList <EndPart*> collidingElements;
    foreach (QGraphicsItem *item, this->collidingItems())
    {
        EndPart *ep = dynamic_cast<EndPart*>(item);
        if (ep)
        {
            collidingElements.push_back(ep);
        }
    }
    if(collidingElements.size() == 1)
    {
        EndPart *connectedNode = collidingElements.takeLast();

        bool validState = connectedNode->parentItem() != this->parentItem();

        this->setValid(validState);
        connectedNode->setValid(validState);

        return true;
    }
    else
    {
        foreach (EndPart *ep, collidingElements)
        {
            ep->setValid(false);
        }
        this->setValid(false);
    }
    return false;
}

