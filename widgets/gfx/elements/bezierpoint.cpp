#include "bezierpoint.h"

//!
//! \brief BezierPoint::BezierPoint
//! \param parent
//!
BezierPoint::BezierPoint(QGraphicsItem *parent) :
    QGraphicsItem(parent)
{
    this->setFlag(QGraphicsItem::ItemIsSelectable);
    this->setFlag(QGraphicsItem::ItemIsMovable);
    this->setFlag(QGraphicsItem::ItemSendsGeometryChanges);
}

//!
//! \brief BezierPoint::~BezierPoint
//!
BezierPoint::~BezierPoint()
{

}

//!
//! \brief BezierPoint::boundingRect
//! \return
//!
QRectF BezierPoint::boundingRect() const
{
    return QRectF(-3, -3, 6, 6);
}

//!
//! \brief BezierPoint::paint
//! \param painter
//!
void BezierPoint::paint(QPainter *painter,
                        const QStyleOptionGraphicsItem *,
                        QWidget *)
{
    painter->setRenderHint(QPainter::Antialiasing);
    painter->setPen(this->isSelected() ? Qt::green : Qt::yellow);
    painter->setBrush(Qt::white);
    painter->drawEllipse(-3, -3, 6, 6);

    QGraphicsItem *parent = this->parentItem();
    if (parent != nullptr)
    {
        parent->update(parent->boundingRect());
    }
}

