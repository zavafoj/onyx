#include "labelelement.h"

#include <QApplication>
#include <QFontMetrics>
#include <QInputDialog>
#include <QDebug>
#include <QMenu>
#include <QGraphicsScene>

#include "stationelement.h"

//!
//! \brief LabelElement::LabelElement
//! \param text
//! \param parent
//!
LabelElement::LabelElement(const QString &text, QGraphicsItem *parent) :
    QGraphicsItem(parent),
    _boundingRect(QRectF(-50, -10, 100, 20))
{
    this->setFlag(QGraphicsItem::ItemIsSelectable);
    this->setFlag(QGraphicsItem::ItemStacksBehindParent);
    this->setZValue(-1);

    this->setText(text);
}

//!
//! \brief LabelElement::~LabelElement
//!
LabelElement::~LabelElement()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief LabelElement::setText
//!
void LabelElement::setText(const QString &text)
{
    if (this->_text != text)
    {
        this->_text = text;
        this->update();
    }
}

//!
//! \brief LabelElement::text
//! \return
//!
QString LabelElement::text() const
{
    return this->_text;
}

//!
//! \brief LabelElement::paint
//! \param painter
//!
void LabelElement::paint(QPainter *painter,
                         const QStyleOptionGraphicsItem *,
                         QWidget *)
{
    painter->save();
    painter->setPen(QPen(QBrush(Qt::black), 0.1, Qt::DotLine));
    painter->setBrush(QBrush(QColor(Qt::yellow).darker()));
    painter->drawRect(this->_boundingRect);
    painter->restore();

    painter->setFont(qApp->font());
    painter->drawText(this->_boundingRect,
                      this->_text,
                      QTextOption(Qt::AlignCenter));
}
