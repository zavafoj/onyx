#ifndef STATIONELEMENT_H
#define STATIONELEMENT_H

#include <QGraphicsSceneContextMenuEvent>

#include "../../core/structure/elements/station.h"
#include "railelement.h"
#include "labelelement.h"

//!
//! \brief The StationElement class
//!
class StationElement : public RailElement
{
public:
    explicit StationElement(Station *s, QGraphicsItem *parent = 0);
    virtual ~StationElement();

    virtual void updateModel();
    virtual int type() const
    {
        return UserType + Object::OT_Station;
    }

    void lock(bool lock);

    QString stationName() const;
    void setStationName(const QString &stationName);

    void switchNodes();

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

private:
    LabelElement *_stationLabel;
};

#endif // STATIONELEMENT_H
