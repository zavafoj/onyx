#ifndef SEMAPHOREELEMENT_H
#define SEMAPHOREELEMENT_H

#include <QGraphicsSceneContextMenuEvent>

#include "signalelement.h"
#include "railelement.h"

//!
//! \brief The SemaphoreElement class
//!
class SemaphoreElement : public RailElement
{
public:
    explicit SemaphoreElement(Semaphore *s, QGraphicsItem *parent = 0);
    virtual ~SemaphoreElement();

    virtual void updateModel();

    virtual int type() const
    {
        return UserType + Object::OT_Semaphore;
    }

    bool isSimpleRail(EndPart *node) const;
    void advance(int phase);

    Semaphore::SemaphoreStates semaphoreState() const;
    void setSemaphoreState(const Semaphore::SemaphoreStates &state);

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
private:
    SignalElement *_signal;
    int _clock;
};

#endif // SEMAPHOREELEMENT_H
