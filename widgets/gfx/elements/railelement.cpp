#include "railelement.h"
#include <QPointF>

//!
//! \brief RailElement::RailElement
//! \param r
//! \param parent
//!
RailElement::RailElement(Rail *r, QGraphicsItem *parent) :
    AbstractRailElement(r, parent)
{
    this->_beginNode = new EndPart(this);
    this->_beginNode->setPos(r->firstNodePosition());

    this->_endNode = new EndPart(this);
    this->_endNode->setPos(r->secondNodePosition());

    this->updateModel();
}

//!
//! \brief RailElement::~RailElement
//!
RailElement::~RailElement()
{
    this->_beginNode->setValid(false);
    this->_endNode->setValid(false);

    this->updateModel();

    delete this->_beginNode;
    delete this->_endNode;
}

//!
//! \brief RailElement::availableConnections
//! \param node
//! \return
//!
QList<EndPart *> RailElement::availableConnections(EndPart *node) const
{
    QList<EndPart *> elements;

    if (node == this->_beginNode || node == nullptr)
        elements << this->_endNode;
    else if (node == this->_endNode)
        elements << this->_beginNode;

    return elements;
}

//!
//! \brief RailElement::updateBoundingRect
//!
void RailElement::updateModel()
{
    //                                Set Bounding Rect geometry
    // =============================================================================== //
    qreal xMin = qMin(this->_beginNode->x(), this->_endNode->x()) - 5.0d;
    qreal yMin = qMin(this->_beginNode->y(), this->_endNode->y()) - 5.0d;

    qreal xMax = qMax(this->_beginNode->x(), this->_endNode->x()) + 5.0d;
    qreal yMax = qMax(this->_beginNode->y(), this->_endNode->y()) + 5.0d;

    // =============================================================================== //

    //                                 Set Shape geometry
    // =============================================================================== //
    qreal dx = this->_endNode->pos().x() - this->_beginNode->pos().x();
    qreal dy = this->_endNode->pos().y() - this->_beginNode->pos().y();

    QPointF beginPointBotomRight = this->_beginNode->pos()  + QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF beginPointTopLeft    = this->_beginNode->pos()  - QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF beginPointTopRight   = this->_beginNode->pos()  + QPointF( SHAPEWIDTH, -SHAPEWIDTH);
    QPointF beginPointBottomLeft = this->_beginNode->pos()  + QPointF(-SHAPEWIDTH,  SHAPEWIDTH);

    QPointF endPointBotomRight = this->_endNode->pos()  + QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF endPointTopLeft    = this->_endNode->pos()  - QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF endPointTopRight   = this->_endNode->pos()  + QPointF( SHAPEWIDTH, -SHAPEWIDTH);
    QPointF endPointBottomLeft = this->_endNode->pos()  + QPointF(-SHAPEWIDTH,  SHAPEWIDTH);

    QPainterPath newShape;
    QPolygonF boundingPolygon;
    // Quarters - I and III
    if (qFuzzyCompare(dx, 0.0) || (dx > 0.0 && dy < 0.0) || (dx < 0.0 && dy > 0.0))
    {
        boundingPolygon << endPointTopLeft
                        << endPointBotomRight
                        << beginPointBotomRight
                        << beginPointTopLeft
                        << endPointTopLeft;
    }
    // Quarters - II and IV
    else if (qFuzzyCompare(dy, 0.0) || (dx < 0.0 && dy < 0.0) || (dx > 0.0 && dy > 0.0))
    {
        boundingPolygon << endPointBottomLeft
                        << endPointTopRight
                        << beginPointTopRight
                        << beginPointBottomLeft
                        << endPointBottomLeft;
    }
    newShape.addPolygon(boundingPolygon);

    // =============================================================================== //
    this->prepareGeometryChange();
    this->_shape = newShape;
    this->_boundingRect = QRectF(QPointF(xMin, yMin),
                                 QPointF(xMax, yMax));
    // =============================================================================== //

    Rail *r = (Rail *)this->_object;
    r->setFirstNodePosition(this->_beginNode->pos().toPoint());
    r->setSecondNodePosition(this->_endNode->pos().toPoint());

    this->update();
}

//!
//! \brief RailElement::lock
//! \param lock
//!
void RailElement::lock(bool lock)
{
    this->_beginNode->setFlag(ItemIsMovable, !lock);
    this->_endNode->setFlag(ItemIsMovable, !lock);

    this->setFlag(ItemIsMovable, !lock);
}

//!
//! \brief RailElement::angle
//! \return
//!
qreal RailElement::angle() const
{
    const QPointF pnt = this->_endNode->pos() - this->_beginNode->pos();

    return qAtan2(pnt.y(),
                  pnt.x()) * 180 / M_PI;
}

//!
//! \brief RailElement::paint
//! \param painter
//!
void RailElement::paint(QPainter *painter,
                        const QStyleOptionGraphicsItem *,
                        QWidget *)
{
    if (isSelected())
        painter->setPen(QPen(QBrush(Qt::cyan), RAIL_WIDTH));
    else if (this->object()->isExhaused())
        painter->setPen(QPen(QBrush(Qt::red), RAIL_WIDTH));
    else
        painter->setPen(QPen(QBrush(Qt::darkCyan), RAIL_WIDTH));

    QLineF line(this->_beginNode->pos(),
               this->_endNode->pos());

    painter->drawLine(line);
}

//!
//! \brief RailElement::itemChange
//! \param change
//! \param value
//! \return
//!
QVariant RailElement::itemChange(QGraphicsItem::GraphicsItemChange change,
                                 const QVariant &value)
{
    QVariant v = AbstractRailElement::itemChange(change, value);

    if (scene() != nullptr)
    {
        switch (change)
        {
        case QGraphicsItem::ItemPositionHasChanged:
        {
            this->_object->setPosition(this->pos().toPoint());
        }
            break;
        default:
            break;
        }
    }
    return v;
}

//!
//! \brief RailElement::endNode
//! \return
//!
EndPart *RailElement::endNode() const
{
    return this->_endNode;
}

//!
//! \brief RailElement::setEndNode
//! \param endNode
//!
void RailElement::setEndNode(EndPart *endNode)
{
    this->_endNode = endNode;
}

//!
//! \brief RailElement::beginNode
//! \return
//!
EndPart *RailElement::beginNode() const
{
    return this->_beginNode;
}

//!
//! \brief RailElement::setBeginNode
//! \param beginNode
//!
void RailElement::setBeginNode(EndPart *beginNode)
{
    this->_beginNode = beginNode;
}

