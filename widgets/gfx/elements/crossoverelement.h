#ifndef CROSSROADELEMENT_H
#define CROSSROADELEMENT_H

#include "abstractrailelement.h"
#include "endpart.h"
#include "../../core/structure/elements/crossover.h"
#include "directionsign.h"

class CrossoverElement : public AbstractRailElement
{
public:
    explicit CrossoverElement(Crossover *co, QGraphicsItem *parent = 0);
    virtual ~CrossoverElement();

    virtual QList<EndPart *> availableConnections(EndPart *node = nullptr) const;

    virtual void updateModel();

    virtual int type() const
    {
        return UserType + Object::OT_Crossover;
    }

    void lock (bool lock);

    EndPart *beginNode() const;
    void setBeginNode(EndPart *beginNode);

    EndPart *endNode() const;
    void setEndNode(EndPart *endNode);

    EndPart *branchNode() const;
    void setBranchNode(EndPart *branchNode);

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

    QVariant itemChange(QGraphicsItem::GraphicsItemChange change,
                    const QVariant &value);

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

private:
    EndPart *_beginNode;
    EndPart *_endNode;
    EndPart *_branchNode;

    bool _locked;
};

#endif // CROSSROADELEMENT_H
