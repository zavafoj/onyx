#include "detectionrect.h"


//!
//! \brief DetectionRect::DetectionRect
//! \param te
//! \param parent
//!
DetectionRect::DetectionRect(QGraphicsItem *parent) :
    QGraphicsItem(parent),
    _shown(false)
{
    this->setFlag(QGraphicsItem::ItemSendsGeometryChanges);
}

//!
//! \brief DetectionRect::boundingRect
//! \return
//!
QRectF DetectionRect::boundingRect() const
{
    return this->_boundingRect;
}

//!
//! \brief DetectionRect::setShown
//! \param shown
//!
void DetectionRect::setShown(bool shown)
{
    if (this->_shown != shown)
    {
        this->_shown = shown;
        this->update();
    }
}

//!
//! \brief DetectionRect::setA
//! \param a
//!
void DetectionRect::setA(qreal a)
{
    this->prepareGeometryChange();

    this->_boundingRect = QRectF(-a / 2.0d, -a / 2.0d,
                                 a, a);

    this->_boundingRect.setWidth(a);
    this->_boundingRect.setHeight(a);

    this->update();
}

//!
//! \brief DetectionRect::paint
//! \param painter
//!
void DetectionRect::paint(QPainter *painter,
                          const QStyleOptionGraphicsItem *,
                          QWidget *)
{
    if (this->_shown)
    {
        painter->setPen(Qt::green);
        painter->setBrush(QBrush(QColor(0, 127, 255, 85)));

        painter->drawRect(this->_boundingRect);
    }
}
