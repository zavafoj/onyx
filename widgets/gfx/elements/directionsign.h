#ifndef DIRECTIONSIGN_H
#define DIRECTIONSIGN_H

#include "gridfitablegraphicsitem.h"

class DirectionSign : public QGraphicsItem
{

public:
    explicit DirectionSign(QGraphicsItem *parent = 0);
    virtual ~DirectionSign();

    QRectF boundingRect() const
    {
        return QRectF(-3.5, -3.5, 7.0, 7.0);
    }

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);
};

#endif // DIRECTIONSIGN_H
