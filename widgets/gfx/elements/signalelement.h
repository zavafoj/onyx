#ifndef SIGNALELEMENT_H
#define SIGNALELEMENT_H

#include <QGraphicsItem>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include "../../core/structure/elements/semaphore.h"

//!
//! \brief The SignalElement class
//!
class SignalElement : public QGraphicsItem
{

public:
    SignalElement(Semaphore::SemaphoreStates state, QGraphicsItem *parent = 0);
    virtual ~SignalElement();

    QRectF boundingRect() const
    {
        return QRectF(-0.75, -5.0, 13.5, 10.0);
    }

    virtual int type() const
    {
        return UserType + 71;
    }


    Semaphore::SemaphoreStates state() const;
    void setState(const Semaphore::SemaphoreStates &state);

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

private:
    Semaphore::SemaphoreStates _state;
};

#endif // SIGNALELEMENT_H
