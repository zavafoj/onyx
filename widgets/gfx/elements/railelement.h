#ifndef RAILELEMENT_H
#define RAILELEMENT_H

#include "../../core/structure/elements/rail.h"
#include "endpart.h"
#include "abstractrailelement.h"

//!
//! \brief The RailElement class
//!
class RailElement : public AbstractRailElement
{
public:
    explicit RailElement(Rail *r, QGraphicsItem *parent = 0);
    virtual ~RailElement();

    virtual QList<EndPart *> availableConnections(EndPart *node = nullptr) const;

    virtual void updateModel();

    virtual int type() const
    {
        return UserType + Object::OT_Rail;
    }

    void lock(bool lock);
    qreal angle() const;

    EndPart *beginNode() const;
    void setBeginNode(EndPart *beginNode);

    EndPart *endNode() const;
    void setEndNode(EndPart *endNode);

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);

    virtual QVariant itemChange(QGraphicsItem::GraphicsItemChange change,
                        const QVariant &value);

protected:
    EndPart *_beginNode;
    EndPart *_endNode;
};

#endif // RAILELEMENT_H
