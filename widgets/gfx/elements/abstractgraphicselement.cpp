#include "abstractgraphicselement.h"
#include <QGraphicsScene>

//!
//! \brief AbstractGraphicsElement::AbstractGraphicsElement
//! \param o
//! \param parent
//!
AbstractGraphicsElement::AbstractGraphicsElement(Object *o, QGraphicsItem *parent):
    GridFitableGraphicsItem(parent),
    _locked(false)
{
    this->_object = o;
}

//!
//! \brief AbstractGraphicsElement::~AbstractGraphicsElement
//!
AbstractGraphicsElement::~AbstractGraphicsElement()
{
    this->_object->deleteLater();
}

//!
//! \brief AbstractGraphicsElement::itemChange
//! \param change
//! \param value
//! \return
//!
QVariant AbstractGraphicsElement::itemChange(QGraphicsItem::GraphicsItemChange change,
                                             const QVariant &value)
{
    return GridFitableGraphicsItem::itemChange(change, value);
}

//!
//! \brief AbstractGraphicsElement::mouseDoubleClickEvent
//! \param event
//!
void AbstractGraphicsElement::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug() << event->pos();
    if (this->scene())
    {
        this->scene()->setFocusItem(this);
    }

    GridFitableGraphicsItem::mouseDoubleClickEvent(event);
}

//!
//! \brief AbstractGraphicsElement::object
//! \return
//!
Object *AbstractGraphicsElement::object() const
{
    return this->_object;
}

//!
//! \brief AbstractGraphicsElement::setObject
//! \param object
//!
void AbstractGraphicsElement::setObject(Object *object)
{
    this->_object = object;
}

