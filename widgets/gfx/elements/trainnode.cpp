#include "trainnode.h"

TrainNode::TrainNode(char type, QGraphicsItem *parent) :
    QGraphicsItem(parent),
    _type(type)
{

}

TrainNode::~TrainNode()
{

}

void TrainNode::paint(QPainter *painter,
                      const QStyleOptionGraphicsItem *,
                      QWidget *)
{
    if (this->_type == 'f')
        painter->setPen(Qt::green);
    else
        painter->setPen(Qt::red);

    painter->drawEllipse(this->boundingRect());
}
