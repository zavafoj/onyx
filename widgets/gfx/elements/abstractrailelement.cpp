#include "abstractrailelement.h"
#include <QPainterPath>

//!
//! \brief AbstractRailElement::AbstractRailElement
//! \param object
//! \param parent
//!
AbstractRailElement::AbstractRailElement(Object *object, QGraphicsItem *parent):
    AbstractGraphicsElement(object, parent)
{

}

//!
//! \brief AbstractRailElement::~AbstractRailElement
//!
AbstractRailElement::~AbstractRailElement()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief AbstractRailElement::boundingRect
//! \return
//!
QRectF AbstractRailElement::boundingRect() const
{
    return this->_boundingRect;
}

////!
////! \brief AbstractRailElement::shape
////! \return
////!
QPainterPath AbstractRailElement::shape() const
{
    return this->_shape;
}

//!
//! \brief AbstractRailElement::itemChange
//! \param change
//! \param value
//! \return
//!
QVariant AbstractRailElement::itemChange(QGraphicsItem::GraphicsItemChange change,
                                         const QVariant &value)
{
    QVariant v = AbstractGraphicsElement::itemChange(change, value);

    if (scene() != nullptr)
    {
        switch (change)
        {
        case QGraphicsItem::ItemPositionHasChanged:
            this->updateModel();
            this->_object->setPosition(v.toPointF());
            break;
        case QGraphicsItem::ItemSelectedChange:
            foreach (QGraphicsItem *item, this->childItems())
            {
                item->setSelected(false);
            }
            break;
        default:
            break;
        }
    }

    return v;
}

