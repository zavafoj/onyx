#ifndef GRIDFITABLEGRAPHICSITEM_H
#define GRIDFITABLEGRAPHICSITEM_H

#include <QGraphicsItem>
#include <QPainter>
#include <QWidget>

#define RAIL_WIDTH 1.5d
#define GRID_SIZE 10

class GridFitableGraphicsItem : public QGraphicsItem
{
public:
    explicit GridFitableGraphicsItem(QGraphicsItem *parent = 0);
    virtual ~GridFitableGraphicsItem();

protected:
    virtual QVariant itemChange(QGraphicsItem::GraphicsItemChange change,
                        const QVariant &value);
};

#endif // GRIDFITABLEGRAPHICSITEM_H
