#ifndef BEZIERPOINT_H
#define BEZIERPOINT_H

#include <QGraphicsItem>
#include <QPainter>
#include <QWidget>

//!
//! \brief The BezierPoint class
//!
class BezierPoint : public QGraphicsItem
{
public:
    explicit BezierPoint(QGraphicsItem *parent = 0);
    virtual ~BezierPoint();

    QRectF boundingRect() const;

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem */*option*/,
               QWidget */*widget*/);
};

#endif // BEZIERPOINT_H
