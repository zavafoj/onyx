#include "trainelement.h"
#include <qmath.h>
#include <QDialog>
#include <QDoubleSpinBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMenu>
#include <QAction>

#include "../../widgets/gfx/graphicsscene.h"

#include "railelement.h"

//!
//! \brief TrainElement::TrainElement
//! \param t
//! \param parent
//!
TrainElement::TrainElement(Train *t, QGraphicsItem *parent) :
    QGraphicsItem(parent),
    _speed(0.0),
    _acceleration(0.0),
    _currentRoute(nullptr),
    _isDeccelerating(false),
    _brakeS(0),
    _balisaElement(nullptr),
    _boundingRect(QRectF(-10, -2.5, 20, 5)),
    _isHidden(false)
{
    this->setZValue(50001.0);
    this->setFlag(ItemIsMovable);
    this->setFlag(ItemIsSelectable);
    this->setFlag(QGraphicsItem::ItemSendsGeometryChanges);

    this->setTrain(t);

    this->_frontRoutingNode = new TrainNode('f', this);
//    this->_frontRoutingNode->setFlag(ItemStacksBehindParent);
    this->_frontRoutingNode->setPos(1.0, 0);

    this->_endRoutingNode = new TrainNode('b', this);
//    this->_endRoutingNode->setFlag(ItemStacksBehindParent);
    this->_endRoutingNode->setPos(-1.0, 0);

    this->_frontNode = new TrainNode('f', this);
    this->_frontNode->setPos(9.5, 0);

    this->_endNode = new TrainNode('b', this);
    this->_endNode->setPos(-9.5, 0);

    this->_detectionRect = new DetectionRect(this);
    this->_detectionRect->setPos(10, 0);
}

//!
//! \brief TrainElement::~TrainElement
//!
TrainElement::~TrainElement()
{
    delete this->_frontNode;
    delete this->_frontRoutingNode;

    delete this->_endNode;
    delete this->_endRoutingNode;

    delete this->_detectionRect;

    this->_train->deleteLater();
}

//!
//! \brief TrainElement::boundingRect
//! \return
//!
QRectF TrainElement::boundingRect() const
{
    return this->_boundingRect;
}

//!
//! \brief TrainElement::route
//! \return
//!
Route *TrainElement::route() const
{
    return this->_currentRoute;
}

//!
//! \brief TrainElement::paint
//! \param painter
//!
void TrainElement::paint(QPainter *painter,
                         const QStyleOptionGraphicsItem */*option*/,
                         QWidget */*widget*/)
{
    painter->setRenderHint(QPainter::Antialiasing);
    painter->setPen(this->isSelected() ? Qt::darkGreen : Qt::darkBlue);
    painter->setBrush(Qt::white);

    painter->drawRect(this->boundingRect());
}

//!
//! \brief TrainElement::contextMenuEvent
//! \param event
//!
void TrainElement::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu m;
    QMenu menudefState(qApp->tr("Change State"), &m);
    QAction greenState(qApp->tr("GreenState"), &menudefState);
    greenState.setData(Semaphore::SS_Green);
    menudefState.addAction(&greenState);

    m.addMenu(&menudefState);

    QAction *res = m.exec(event->screenPos());
    if (res)
    {
        this->_train->setMovement(Train::TM_Moving_ACC);
    }
}

//!
//! \brief TrainElement::itemChange
//! \param change type of change
//! \param value event value
//! \return
//!
QVariant TrainElement::itemChange(QGraphicsItem::GraphicsItemChange change,
                                  const QVariant &value)
{
    if (scene() != nullptr)
    {
        switch (change) {
        case ItemPositionHasChanged:
        {
            qDebug() << "posChanged";
            this->_train->setPosition(value.toPointF());

            qreal rotation = 0;
            foreach (QGraphicsItem *item, this->collidingItems())
            {
                RailElement *re = dynamic_cast <RailElement*> (item);
                if (re)
                {
                    rotation = re->angle();
                    break;
                }

                TerminatorElement *te = dynamic_cast <TerminatorElement*> (item);
                if (te)
                {
                    rotation = te->angle();
                    break;
                }
            }

            this->setRotation(rotation);
        }
            break;
        case ItemSelectedHasChanged:
            this->_detectionRect->setShown(value.toBool());
        default:
            break;
        }
    }

    return QGraphicsItem::itemChange(change, value);
}

//!
//! \brief TrainElement::isHidden
//! \return
//!
bool TrainElement::isHidden() const
{
    return this->_isHidden;
}

//!
//! \brief TrainElement::setIsHidden
//! \param isHidden
//!
void TrainElement::setIsHidden(bool isHidden)
{
    if (this->_isHidden != isHidden)
    {
        this->_isHidden = isHidden;
        this->setVisible(!isHidden);
    }
}

//!
//! \brief TrainElement::decceleration
//! \return
//!
qreal TrainElement::decceleration() const
{
    return this->_decceleration;
}

//!
//! \brief TrainElement::connectWithNeariestTrain
//!
void TrainElement::connectWithNeariestTrain() throw (QString)
{
    if (this->_train->addedComponents() > 2 || !this->_currentRoute)
    {
        throw QString (QObject::tr("Train has maximum amount of components"));
    }
    else if (this->train()->willBeAdded() || this->train()->addDemand())
    {
        throw QString (QObject::tr("Another action in progress"));
    }
    else
    {
        foreach (QGraphicsItem *item, this->_detectionRect->collidingItems())
        {
            if (item->type() == QGraphicsItem::UserType + Object::OT_Train && item != this)
            {
                TrainElement *collidingTrain = (TrainElement *) item;
                // check if coll train is on the same route
                qDebug() << "AddDemand!";
                if (collidingTrain->train()->addedComponents() > 0)
                {
                    throw QString (QObject::tr("Unable to add complex trains"));
                }
                if (this->isCollidingWithTrain(collidingTrain))
                {
                    this->train()->setMovement(Train::TM_Moving_ACC);
                    this->train()->setAddDemand(true);
                    collidingTrain->train()->setWillBeAdded(true);
                }
            }
        }
    }
}

//!
//! \brief TrainElement::disconnectTrain
//!
void TrainElement::disconnectTrain() throw (QString)
{
//    if (this->_train)
    if (this->_train->addedComponents() == 0)
    {
        throw QString (QObject::tr("Train has no components"));
    }
    else if (this->train()->willBeAdded() || this->train()->addDemand())
    {
        throw QString (QObject::tr("Another action in progress"));
    }
    else
    {
        if (this->_currentRoute)
        {
            foreach (QGraphicsItem *item, this->_detectionRect->collidingItems())
            {
                if (item->type() == QGraphicsItem::UserType + Object::OT_Train && item != this)
                {
                    TrainElement *collidingTrain = (TrainElement *) item;
                    // check if coll train is on the same route
                    if (this->isCollidingWithTrain(collidingTrain))
                    {
                        throw QString (QObject::tr("No enought space to split trains"));
                    }
                }
            }

            bool isForward = this->_train->direction() == Train::TD_Forward;
            bool canSpawn = isForward
                    ? (this->_leftLength - 50.0d > 0.0d)
                    : (this->_currentLength - 50.0d > 0.0d);

            if (canSpawn)
            {
                GraphicsScene *gscene = (GraphicsScene*) this->scene();
                Train *t = this->_train->popTrain();

                TrainElement *te = (TrainElement *) gscene->_elements[t];

                qreal asdf = ((this->_train->addedComponents() + 1) * 11.0d) / (this->_totalLength + (this->_train->addedComponents() * 11.0d));
//                qreal asdf2 = ((this->_train->addedComponents()) * 11.0d) / (this->_totalLength + (this->_train->addedComponents() * 11.0d));
                te->setRoute(this->_currentRoute);

                te->setIsHidden(false);
                this->_train->setAddDemand(false);
                te->train()->setWillBeAdded(false);

                te->setPathProgress(isForward
                                    ? this->_pathProgress + asdf
                                    : this->_pathProgress - asdf);

                this->setPathProgress(isForward
                                    ? this->_pathProgress - asdf
                                    : this->_pathProgress + asdf);

                qreal elements = this->_train->addedComponents();
                qreal width = this->_train->addedComponents() + 1;
                this->_boundingRect = QRectF(-10.0d * width,
                                             -2.5d,
                                              20.0d * width,
                                              5.0d);

                this->_frontRoutingNode->setPos(1.0d +(10.0d * elements), 0);
                this->_endRoutingNode->setPos(-1.0d -(10.0d * elements), 0);

                this->_frontNode->setPos(10.0d * width - 0.5d, 0);
                this->_endNode->setPos(-10.0d * width + 0.5d, 0);

                this->update();

                te->train()->setDirection(isForward ? Train::TD_Forward : Train::TD_Backward);
                this->_train->setDirection(isForward ? Train::TD_Backward : Train::TD_Forward);
            }
        }
    }
}

//!
//! \brief TrainElement::isCollidingWithTrain
//! \param collidingTrain
//! \return
//!
bool TrainElement::isCollidingWithTrain(TrainElement *collidingTrain)
{
    if (collidingTrain->isHidden() || collidingTrain == this)
        return false;
    if (!collidingTrain->route())
    {
        if (this->_currentRoute->painterPath().
                intersects(collidingTrain->boundingRect().
                           translated(collidingTrain->pos())))
        {
            return true;
        }
    }
    else
    {
        if (this->_currentRoute->painterPath().intersects(collidingTrain->route()->painterPath())
                && this->_currentRoute->zLevel() == collidingTrain->route()->zLevel())
        {
            if (this->train()->priority() <= collidingTrain->train()->priority())
            {
                return true;
            }
            else
            {
                if (this->_currentRoute->painterPath().
                        intersects(collidingTrain->boundingRect().
                                   translated(collidingTrain->pos())))
                {
                    return true;
                }
            }
        }
    }
    return false;
}

//!
//! \brief TrainElement::addTrain
//! \param element
//!
void TrainElement::addTrain(TrainElement *element)
{
    this->_train->addTrain(element->train());
    element->setIsHidden(true);
    qreal elements = this->_train->addedComponents();
    qreal width = this->_train->addedComponents() + 1;
    this->_boundingRect = QRectF(-10.0d * width,
                                 -2.5d,
                                  20.0d * width,
                                  5.0d);

    this->_frontRoutingNode->setPos(1.0d +(10.0d * elements), 0);
    this->_endRoutingNode->setPos(-1.0d -(10.0d * elements), 0);

    this->_frontNode->setPos(10.0d * width - 0.5d, 0);
    this->_endNode->setPos(-10.0d * width + 0.5d, 0);

    qreal trainLength = 10.0d;
    qreal percent = trainLength / this->_totalLength;
    this->_pathProgress = this->_train->direction() == Train::TD_Forward ?
                (this->_pathProgress + percent) :
                (this->_pathProgress - percent) ;

    qDebug() << "PathProgressAndPercent" << this->_pathProgress << percent;
    this->setPathProgress(this->_pathProgress);

    this->_train->setVelocity(0.0d);
    this->_speed = 0.0d;
    this->_train->setMovement(Train::TM_Stopped);
    this->_train->setAddDemand(false);
    element->setPos(-2000, -2000);
    element->setRoute(nullptr);

    this->update();
}

//!
//! \brief TrainElement::popTrain
//! \return
//!
TrainElement *TrainElement::popTrain()
{

}

//!
//! \brief TrainElement::acceleration
//! \return
//!
qreal TrainElement::acceleration() const
{
    return this->_acceleration;
}

//!
//! \brief TrainElement::speed
//! \return
//!
qreal TrainElement::speed() const
{
    return this->_speed;
}

//!
//! \brief TrainElement::brakeS
//! \return
//!
qreal TrainElement::brakeS() const
{
    return this->_brakeS;
}

//!
//! \brief TrainElement::pathProgress
//! \return
//!
qreal TrainElement::pathProgress() const
{
    return this->_pathProgress;
}

//!
//! \brief TrainElement::setPathProgress
//! \param pathProgress
//!
void TrainElement::setPathProgress(const qreal &pathProgress)
{
    this->_pathProgress = pathProgress;

    if (this->_currentRoute)
    {
        this->setPos(this->_currentRoute->painterPath().pointAtPercent(pathProgress));
        this->setRotation(this->_currentRoute->painterPath().angleAtPercent(pathProgress));
    }
}

//!
//! \brief TrainElement::train
//! \return
//!
Train *TrainElement::train() const
{
    return this->_train;
}

//!
//! \brief TrainElement::setTrain
//! \param train
//!
void TrainElement::setTrain(Train *train)
{
    this->_train = train;

    this->_acceleration = this->_train->acceleration() * (1.0/36.0);
    this->_decceleration = this->_train->decceleration() * (1.0/36.0);
}

//!
//! \brief TrainElement::setRoute
//! \param route
//!
void TrainElement::setRoute(Route *route)
{
    bool wasEmpty = this->_currentRoute == nullptr && route != nullptr;
    if (this->_currentRoute && route != this->_currentRoute)
    {
        qDebug() << "Setting exhaused to false" << this->_currentRoute->elements().first()->isExhaused();
        this->_currentRoute->setExhaused(false);
    }

    this->_currentRoute = route;

    if (route)
    {
        if (this->_train->direction() == Train::TD_Forward)
            this->_pathProgress = wasEmpty
                ? (this->_train->addedComponents() * 10.0d / route->painterPath().length())
                : 0.0d;
        else
            this->_pathProgress = wasEmpty
                ? (1.0 - (this->_train->addedComponents() * 10.0d / route->painterPath().length()))
                : 1.0d;

        this->_vmax = ((qreal) qMin(this->_currentRoute->vMax(),
                                    this->_train->maxVelocity())) * (1.0/36.0);
    }
}

//!
//! \brief TrainElement::showTrainRoute
//!
void TrainElement::showTrainRoute()
{
    if (this->_currentRoute)
        ((GraphicsScene*) this->scene())->showTrainRoute(this->_currentRoute);
}

//!
//! \brief TrainElement::findAvailablePath
//! \param node
//!
void TrainElement::findAvailablePath(TrainNode *node)
{
    GraphicsScene *gscene = (GraphicsScene*) this->scene();
    bool isForward = this->_train->direction() == Train::TD_Forward;

    foreach (QGraphicsItem *item, node->collidingItems())
    {
        switch (item->type())
        {
        case QGraphicsItem::UserType + Object::OT_Semaphore:
        {
            Semaphore *semaphore = (Semaphore*) ((SemaphoreElement*) item)->object();
            if (semaphore->state() != Semaphore::SS_Green && node == this->_frontRoutingNode)
                continue;

            Route *r = isForward
                    ? gscene->availableRouteForStartingNode(semaphore)
                    : gscene->availableRouteForEndingNode(semaphore);

            if (r)
            {
                if (this->_train->movement() == Train::TM_Stopped)
                    this->_train->setMovement(Train::TM_Moving_ACC);

                gscene->assignPathToTrain(r, this);
                return;
            }
        }
            break;
        case QGraphicsItem::UserType + Object::OT_Terminator:
        {
            if (this->_train->movement() == Train::TM_Stopped)
                continue;

            this->_train->setDirection(Train::TD_Forward);

            Object *object = (Semaphore*) ((SemaphoreElement*) item)->object();

            Route *r = gscene->availableRouteForStartingNode(object);

            if (r)
            {
                gscene->assignPathToTrain(r, this);
                return;
            }
        }
            break;
        }
    }
}

//!
//! \brief TrainElement::lock
//! \param lock
//!
void TrainElement::lock(bool lock)
{
    this->setFlag(QGraphicsItem::ItemIsMovable, !lock);
    this->setFlag(QGraphicsItem::ItemSendsGeometryChanges, !lock);
}

//!
//! \brief TrainElement::advance
//! \param phase
//!
void TrainElement::advance(int phase)
{
    if (this->isHidden())
            return;

    qreal trainLength = this->_train->addedComponents() * 10.0d;
    GraphicsScene *scene = (GraphicsScene*) this->scene();
    bool isForward = this->_train->direction() == Train::TD_Forward;
    this->_brakeS = 0.5 *(( this->_speed * this->_speed) / this->_decceleration);

    // detekcja kolizji
    if (!phase)
    {
        qreal trains = this->_train->addedComponents() + 1;
        qreal a = this->_brakeS + (this->_train->addDemand() ? -1.0d : 40.0d);
        this->_detectionRect->setA(a);
        this->_detectionRect->setPos((qreal) (isForward
                                     ?  a / 2.0d + trains * 10.0d
                                     : -a / 2.0d - trains * 10.0d), 0);

        if (!this->_currentRoute)
            return;

        QList <QGraphicsItem *> collidingItems = this->collidingItems();

        bool noBalisa = this->_balisaElement == nullptr;
        BalisaElement *found = nullptr;
        // collision detection
        foreach (QGraphicsItem *item, collidingItems)
        {
            switch (item->type())
            {
            case QGraphicsItem::UserType + Object::OT_Balisa:
            {
                if (!found)
                    found = (BalisaElement*) item;
            }
                break;
            case QGraphicsItem::UserType + Object::OT_Train:
            {
                TrainElement *te = (TrainElement *) item;
                if (te->train()->willBeAdded())
                {
                    this->addTrain(te);
                    return;
                }
                else if (te->train()->addDemand() && !this->isHidden())
                {
                    te->train()->setAddDemand(false);
                    return;
                }
                else if (te->isHidden() || this->isHidden())
                {
                    return;
                }
                if (te->route() == nullptr || this->_currentRoute == nullptr)
                {
                    this->_train->appendLog(Object::tr("Collision with Train %1").arg(this->_train->objectName()));
                    scene->demandStop(Object::tr("Train %1 Collision!").arg(this->_train->objectName()));
                    return;
                }
                if (te->route()->zLevel() == this->_currentRoute->zLevel())
                {
                    this->_train->appendLog(Object::tr("Collision with Train %1").arg(this->_train->objectName()));
                    scene->demandStop(Object::tr("Train %1 Collision!").arg(this->_train->objectName()));
                    return;
                }
            }
                break;
            default:
                break;
            }
        }
        this->_balisaElement = found;
        if (found && noBalisa)
        {
            this->_train->appendLog(QString("Gathering information from Balisa %1")
                                    .arg(found->balisa()->objectName()));

            found->balisa()->appendLog(QString("Sending information to Train %1")
                                       .arg(this->_train->objectName()));
        }

        // collision avoidance
        QList <QGraphicsItem *> avoidance = this->_detectionRect->collidingItems(Qt::IntersectsItemBoundingRect);
        bool collides = false;
        foreach (QGraphicsItem *item, avoidance)
        {
            if (item == this)
                continue;

            TrainElement *collidingTrain = nullptr;
            switch (item->type())
            {
            case UserType + DETECTION_RECT_ID:
            {
                collidingTrain = (TrainElement *) item->parentItem();
            }
                break;
            case UserType + Object::OT_Train:
            {
                collidingTrain = (TrainElement *) item;
            }
                break;
            default:
                break;
            }

            if (collidingTrain)
            {
                collides = this->isCollidingWithTrain(collidingTrain);
                if (collides)
                    break;
            }
        }

        this->_train->setDistanceAlert(collides);
    }
    else
    {
        if (this->_currentRoute)
        {
            // Moving decission
            this->_totalLength = this->_currentRoute->painterPath().length();
            this->_currentLength = this->_totalLength * this->_pathProgress;
            this->_leftLength = this->_totalLength - this->_currentLength;
            this->_currentLength -= trainLength;
            this->_leftLength -= trainLength;

            qreal percent = trainLength / this->_totalLength;
            qreal max = 1.0d - percent;
            qreal min = 0.0d + percent;

            if (this->_train->movement() != Train::TM_Stopped)
            {
                qreal dt = 1;
                if (!isForward)
                {
                    qreal temp = this->_currentLength;
                    this->_currentLength = this->_leftLength;
                    this->_leftLength = temp;
                }

                Object *first = this->_currentRoute->elements().first();
                Object *last = this->_currentRoute->elements().last();

                Route *nextRoute = isForward ? (last->type() == Object::OT_Terminator)
                                                ? nullptr
                                                : scene->availableRouteForStartingNode(last)
                                             : (first->type() == Object::OT_Terminator)
                                                ? nullptr
                                                : scene->availableRouteForEndingNode(first);

                if (this->_brakeS >= this->_leftLength + 1)
                {
                    if (nextRoute)
                    {
                        bool isValid = true;
                        Semaphore *semaphore = qobject_cast<Semaphore*>(nextRoute->elements().first());
                        if (semaphore)
                        {
                            isValid = semaphore->state() == Semaphore::SS_Green;
                        }

                        if (isValid)
                        {
                            qDebug() << Q_FUNC_INFO << "getting new Vmax";
                            qreal routevmax = (qreal) qMin(nextRoute->vMax(),
                                                      this->_train->maxVelocity());

                            this->_vmax = qMin(routevmax * (1.0/36.0),
                                               this->_vmax);

                            // oblicz pozostałą drogę
                            qreal nextLength = nextRoute->painterPath().length();
                            if (this->_brakeS >= nextLength + this->_leftLength)
                                this->_isDeccelerating = true;
                            else
                                this->_isDeccelerating = false;
                        }
                        else
                            this->_isDeccelerating = true;
                    }
                    else
                        this->_isDeccelerating = true;
                }

                if (this->_train->stopDemand()
                        || this->_isDeccelerating
                        || this->_train->distanceAlert()
                        || this->_speed / this->_vmax > 1.05)
                {
                    this->_train->setMovement(Train::TM_Moving_DEC);
                    qreal tempSpeed = this->_speed - (this->_decceleration * dt);

                    this->_speed = qMax(tempSpeed, 0.0);

                    if (this->_speed == 0.0)
                    {
                        if (this->train()->distanceAlert())
                        {
                            if (this->_train->stopDemand())
                            {
                                this->_train->setMovement(Train::TM_Stopped);
                                this->_isDeccelerating = false;
                            }
                        }
                        else
                        {
                            this->_train->setMovement(Train::TM_Stopped);
                            this->_isDeccelerating = false;
                        }
                    }
                }
                else if (this->_vmax <= this->_speed)
                {
                    this->_train->setMovement(Train::TM_Moving_AVG);
                    this->_speed = this->_vmax;
                }
                else
                {
                    this->_train->setMovement(Train::TM_Moving_ACC);

                    qreal tempSpeed = this->_speed + this->_acceleration * dt;
                    this->_speed = (tempSpeed > this->_vmax) ? this->_vmax : tempSpeed;
                }

                this->_pathProgress = isForward
                        ? qMin(this->_pathProgress + (this->_speed * dt) / this->_totalLength,
                               max)
                        : qMax(this->_pathProgress - (this->_speed * dt) / this->_totalLength,
                               min);
            }
            bool needsToStop = false;
            if (isForward)
            {
                if (this->_pathProgress >= max)
                    needsToStop = true;
            }
            else
            {
                if (this->_pathProgress <= min)
                    needsToStop = true;
            }

            if (needsToStop)
            {
                // check if last element is terminator
                Object *obj = isForward ? this->_currentRoute->elements().last()
                                        : this->_currentRoute->elements().first();

                if (obj->type() == Object::OT_Terminator)
                {
                    scene->assignPathToTrain(nullptr, this);
                }

                else
                {
                    if (this->_train->direction() == Train::TD_Forward)
                        this->findAvailablePath(this->_frontRoutingNode);
                    else
                        this->findAvailablePath(this->_endRoutingNode);
                }

                if (this->_currentRoute == nullptr)
                {
                    this->_isDeccelerating = false;
                    this->_speed = 0.0;
                    this->_train->setMovement(Train::TM_Stopped);
                }

                return;
            }

            this->setPos(this->_currentRoute->painterPath().pointAtPercent(this->_pathProgress));
            this->setRotation(-this->_currentRoute->painterPath().angleAtPercent(this->_pathProgress));

            this->_train->setVelocity(this->_speed * 36.0d);
        }
        else
        {
            if (isForward)
                this->findAvailablePath(this->_frontRoutingNode);
            else
                this->findAvailablePath(this->_endRoutingNode);

            if (this->_currentRoute)
            {
                this->_isDeccelerating = false;
                this->_train->setMovement(Train::TM_Moving_ACC);
            }
            else
            {
                this->_train->setMovement(Train::TM_Stopped);
            }
        }
    }
}
