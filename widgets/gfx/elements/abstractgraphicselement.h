#ifndef ABSTRACTGRAPHICSELEMENT_H
#define ABSTRACTGRAPHICSELEMENT_H

#include "gridfitablegraphicsitem.h"
#include "../../core/structure/elements/object.h"
#include <QGraphicsSceneMouseEvent>

class AbstractGraphicsElement : public GridFitableGraphicsItem
{
    Q_CLASSINFO("Author", "Wojciech Ossowski")

public:
    explicit AbstractGraphicsElement(Object *o, QGraphicsItem *parent = 0);
    virtual ~AbstractGraphicsElement();

    Object *object() const;
    void setObject(Object *object);

protected:
    virtual QVariant itemChange(QGraphicsItem::GraphicsItemChange change,
                        const QVariant &value);

    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

protected:
    Object *_object;
    bool _locked;
};

#endif // ABSTRACTGRAPHICSELEMENT_H
