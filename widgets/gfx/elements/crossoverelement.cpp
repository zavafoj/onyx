#include "crossoverelement.h"

#include <QMenu>
#include <QAction>

//!
//! \brief CrossoverElement::CrossoverElement
//! \param co
//! \param parent
//!
CrossoverElement::CrossoverElement(Crossover *co, QGraphicsItem *parent):
    AbstractRailElement(co, parent),
    _locked(false)
{
    this->_beginNode = new EndPart(this);
    this->_beginNode->setPos(co->firstNodePosition());

    this->_endNode = new EndPart(this);
    this->_endNode->setPos(co->secondNodePosition());

    this->_branchNode = new EndPart(this);
    this->_branchNode->setPos(co->branchNodePosition());

    this->updateModel();
}

//!
//! \brief CrossoverElement::~CrossoverElement
//!
CrossoverElement::~CrossoverElement()
{
    this->_beginNode->setValid(false);
    this->_endNode->setValid(false);
    this->_branchNode->setValid(false);

    delete this->_beginNode;
    delete this->_endNode;
    delete this->_branchNode;
}

//!
//! \brief CrossoverElement::availableConnections
//! \param node
//! \return
//!
QList<EndPart *> CrossoverElement::availableConnections(EndPart *node) const
{
    // create empty list of connections
    QList<EndPart *> elements;

    // if argument is equal to ‘begin node’
    if (node == this->_beginNode)
    {
        // push back 2 available connections to list
        elements << this->_endNode;
        elements << this->_branchNode;
    }
    // else if movement is backward
    else if (node == this->_endNode ||
             node == this->_branchNode)
    {
        // push back begin node to list
        elements << this->_beginNode;
    }

    return elements;
}


//!
//! \brief CrossoverElement::updateBoundingRect
//!
void CrossoverElement::updateModel()
{
    // Create bounding rect
    qreal xMin = qMin(qMin(this->_beginNode->x(), this->_endNode->x()),
                      this->_branchNode->x()) - 5.0d;
    qreal yMin = qMin(qMin(this->_beginNode->y(), this->_endNode->y()),
                      this->_branchNode->y()) - 5.0d;

    qreal xMax = qMax(qMax(this->_beginNode->x(), this->_endNode->x()),
                      this->_branchNode->x()) + 5.0d;
    qreal yMax = qMax(qMax(this->_beginNode->y(), this->_endNode->y()),
                      this->_branchNode->y()) + 5.0d;

    //                                 Set Shape geometry
    // =============================================================================== //
    Crossover *co = (Crossover *)this->_object;
    bool branch = co->state() == Crossover::CS_Branch;

    qreal dx = (branch ? this->_branchNode->pos().x() : this->_endNode->pos().x()) - this->_beginNode->pos().x();
    qreal dy = (branch ? this->_branchNode->pos().y() : this->_endNode->pos().y()) - this->_beginNode->pos().y();

    QPointF beginPointBotomRight = this->_beginNode->pos()  + QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF beginPointTopLeft    = this->_beginNode->pos()  - QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF beginPointTopRight   = this->_beginNode->pos()  + QPointF( SHAPEWIDTH, -SHAPEWIDTH);
    QPointF beginPointBottomLeft = this->_beginNode->pos()  + QPointF(-SHAPEWIDTH,  SHAPEWIDTH);

    QPointF endPointBotomRight = (branch ? this->_branchNode->pos() : this->_endNode->pos())  + QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF endPointTopLeft    = (branch ? this->_branchNode->pos() : this->_endNode->pos())  - QPointF( SHAPEWIDTH,  SHAPEWIDTH);
    QPointF endPointTopRight   = (branch ? this->_branchNode->pos() : this->_endNode->pos())  + QPointF( SHAPEWIDTH, -SHAPEWIDTH);
    QPointF endPointBottomLeft = (branch ? this->_branchNode->pos() : this->_endNode->pos())  + QPointF(-SHAPEWIDTH,  SHAPEWIDTH);

    QPainterPath newShape;
    QPolygonF boundingPolygon;
    // Quarters - I and III
    if (qFuzzyCompare(dx, 0.0) || (dx > 0.0 && dy < 0.0) || (dx < 0.0 && dy > 0.0))
    {
        boundingPolygon << endPointTopLeft
                        << endPointBotomRight
                        << beginPointBotomRight
                        << beginPointTopLeft
                        << endPointTopLeft;
    }
    // Quarters - II and IV
    else if (qFuzzyCompare(dy, 0.0) || (dx < 0.0 && dy < 0.0) || (dx > 0.0 && dy > 0.0))
    {
        boundingPolygon << endPointBottomLeft
                        << endPointTopRight
                        << beginPointTopRight
                        << beginPointBottomLeft
                        << endPointBottomLeft;
    }
    newShape.addPolygon(boundingPolygon);

    // =============================================================================== //
    this->prepareGeometryChange();
    this->_boundingRect = QRectF(QPointF(xMin, yMin),
                                 QPointF(xMax, yMax));
    this->_shape = newShape;

    // =============================================================================== //
    co->setFirstNodePosition(this->_beginNode->pos().toPoint());
    co->setSecondNodePosition(this->_endNode->pos().toPoint());
    co->setBranchNodePosition(this->_branchNode->pos().toPoint());

    if (this->_endNode->pos().y() - this->_branchNode->pos().y() == 0 ||
        this->_endNode->pos().x() - this->_branchNode->pos().x() == 0)
    {
        if (co->state() == Crossover::CS_Unknown)
        {
            co->setState(Crossover::CS_Main);
        }
    }
    else
    {
        co->setState(Crossover::CS_Unknown);
    }

    this->update();
}

//!
//! \brief CrossoverElement::lock
//! \param lock
//!
void CrossoverElement::lock(bool lock)
{
    this->_branchNode->setFlag(ItemIsMovable, !lock);
    this->_beginNode->setFlag(ItemIsMovable, !lock);
    this->_endNode->setFlag(ItemIsMovable, !lock);

    this->setFlag(ItemIsMovable, !lock);
}

//!
//! \brief CrossoverElement::paint
//! \param painter
//!
void CrossoverElement::paint(QPainter *painter,
                             const QStyleOptionGraphicsItem *,
                             QWidget *)
{
    Crossover *crossover = (Crossover*)this->_object;

    if (crossover->state() == Crossover::CS_Unknown)
        painter->setPen(QPen(QBrush(Qt::red), RAIL_WIDTH, Qt::SolidLine, Qt::RoundCap));
    else if (isSelected())
        painter->setPen(QPen(QBrush(Qt::cyan), RAIL_WIDTH, Qt::SolidLine, Qt::RoundCap));
    else if (this->object()->isExhaused())
        painter->setPen(QPen(QBrush(Qt::red), RAIL_WIDTH, Qt::SolidLine, Qt::RoundCap));
    else
        painter->setPen(QPen(QBrush(Qt::darkCyan), RAIL_WIDTH, Qt::SolidLine, Qt::RoundCap));

    QLineF line(this->_beginNode->pos(),
               this->_endNode->pos());

    // connect begin with end
    switch (crossover->state())
    {
    case Crossover::CS_Main:
        painter->drawLine(line);

        painter->setPen(QPen(QBrush(QColor(Qt::darkCyan).darker()),
                             RAIL_WIDTH/4, Qt::DotLine, Qt::RoundCap));

        painter->drawLine(this->_beginNode->pos(),
                          this->_branchNode->pos());
        break;
    case Crossover::CS_Branch:
        painter->drawLine(this->_beginNode->pos(),
                          this->_branchNode->pos());

        painter->setPen(QPen(QBrush(QColor(Qt::darkCyan).darker()),
                             RAIL_WIDTH/4, Qt::DotLine, Qt::RoundCap));

        painter->drawLine(line);
        break;
    default:
        painter->drawLine(line);

        painter->setPen(QPen(QBrush(QColor(Qt::red).darker()),
                             RAIL_WIDTH/4, Qt::DotLine, Qt::RoundCap));

        painter->drawLine(this->_beginNode->pos(),
                          this->_branchNode->pos());
        break;
    }
}


//!
//! \brief RailElement::itemChange
//! \param change
//! \param value
//! \return
//!
QVariant CrossoverElement::itemChange(QGraphicsItem::GraphicsItemChange change,
                                 const QVariant &value)
{
    QVariant v = AbstractRailElement::itemChange(change, value);

    if (scene() != nullptr)
    {
        switch (change)
        {
        case QGraphicsItem::ItemPositionHasChanged:
        {
            this->_object->setPosition(value.toPointF());
        }
            break;
        default:
            break;
        }
    }
    return v;
}

//!
//! \brief CrossoverElement::contextMenuEvent
//! \param event
//!
void CrossoverElement::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    Crossover *co = (Crossover*)this->object();
    if (co->state() == Crossover::CS_Unknown)
        return;

    QMenu m;
    QMenu menuState(qApp->tr("Change State"), &m);
    QAction mainState(qApp->tr("Main Rail"), &menuState);
    mainState.setData(Crossover::CS_Main);
    mainState.setCheckable(true);
    mainState.setChecked(co->state() == Crossover::CS_Main);
    menuState.addAction(&mainState);

    QAction branchState(qApp->tr("Branch Rail"), &menuState);
    branchState.setData(Crossover::CS_Branch);
    branchState.setCheckable(true);
    branchState.setChecked(co->state() == Crossover::CS_Branch);
    menuState.addAction(&branchState);

    m.addMenu(&menuState);

    QAction *res = m.exec(event->screenPos());
    if (res)
    {
        co->setState((Crossover::CrossoverStates) res->data().toInt());
        this->updateModel();
    }
}

//!
//! \brief CrossoverElement::branchNode
//! \return
//!
EndPart *CrossoverElement::branchNode() const
{
    return this->_branchNode;
}

//!
//! \brief CrossoverElement::setBranchNode
//! \param branchNode
//!
void CrossoverElement::setBranchNode(EndPart *branchNode)
{
    this->_branchNode = branchNode;
}

//!
//! \brief CrossoverElement::endNode
//! \return
//!
EndPart *CrossoverElement::endNode() const
{
    return this->_endNode;
}

//!
//! \brief CrossoverElement::setEndNode
//! \param endNode
//!
void CrossoverElement::setEndNode(EndPart *endNode)
{
    this->_endNode = endNode;
}

//!
//! \brief CrossoverElement::beginNode
//! \return
//!
EndPart *CrossoverElement::beginNode() const
{
    return this->_beginNode;
}

void CrossoverElement::setBeginNode(EndPart *beginNode)
{
    this->_beginNode = beginNode;
}

