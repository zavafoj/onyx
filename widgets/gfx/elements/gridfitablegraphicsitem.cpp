#include "gridfitablegraphicsitem.h"
#include <QDebug>

//!
//! \brief GridFitableGraphicsItem::GridFitableGraphicsItem
//! \param parent
//!
GridFitableGraphicsItem::GridFitableGraphicsItem(QGraphicsItem *parent):
    QGraphicsItem(parent)
{
    this->setFlag(QGraphicsItem::ItemIsSelectable);
    this->setFlag(QGraphicsItem::ItemIsMovable);
    this->setFlag(QGraphicsItem::ItemSendsGeometryChanges);
}

//!
//! \brief GridFitableGraphicsItem::~GridFitableGraphicsItem
//!
GridFitableGraphicsItem::~GridFitableGraphicsItem()
{

}

//!
//! \brief GridFitableGraphicsItem::itemChange
//! \param change
//! \param value
//! \return
//!
QVariant GridFitableGraphicsItem::itemChange(QGraphicsItem::GraphicsItemChange change,
                                             const QVariant &value)
{
    if (change == QGraphicsItem::ItemPositionChange && scene() != nullptr)
    {
        QPointF pos = value.toPointF();
        int posx = pos.x();
        posx -= posx % GRID_SIZE;
        pos.setX(posx);

        int posy = pos.y();
        posy -= posy % GRID_SIZE;
        pos.setY(posy);
        return pos;
    }

    return QGraphicsItem::itemChange(change, value);
}
