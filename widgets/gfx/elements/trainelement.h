#ifndef TRAINELEMENT_H
#define TRAINELEMENT_H

#include "abstractgraphicselement.h"
#include "balisaelement.h"
#include "trainnode.h"
#include "../../core/structure/elements/train.h"
#include "../../core/structure/elements/route.h"

#include "detectionrect.h"

class TrainElement : public QGraphicsItem
{
public:
    explicit TrainElement(Train *t, QGraphicsItem *parent = 0);
    virtual ~TrainElement();

    virtual int type() const
    {
        return UserType + Object::OT_Train;
    }

    QRectF boundingRect() const;
    Route *route() const;

    Train *train() const;
    void setTrain(Train *train);
    void setRoute(Route *route);
    void showTrainRoute();
    void findAvailablePath(TrainNode *node);

    void lock(bool lock);

    void advance(int phase);

    qreal pathProgress() const;
    void setPathProgress(const qreal &pathProgress);

    qreal brakeS() const;
    qreal speed() const;
    qreal acceleration() const;
    qreal decceleration() const;

    void connectWithNeariestTrain() throw (QString);
    void disconnectTrain() throw (QString);
    bool isCollidingWithTrain(TrainElement *collidingTrain);

    void addTrain(TrainElement *element);
    TrainElement *popTrain();

    bool isHidden() const;
    void setIsHidden(bool isHidden);

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *,
               QWidget *);

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

    QVariant itemChange(GraphicsItemChange change,
                        const QVariant &value);

private:
    qreal _angle;
    qreal _pathProgress;
    qreal _speed;

    qreal _acceleration;
    qreal _decceleration;
    qreal _vmax;

    qreal _totalLength;
    qreal _currentLength;
    qreal _leftLength;

    Route *_currentRoute;
    QPainterPath _path;

    Train *_train;

    TrainNode *_frontRoutingNode;
    TrainNode *_endRoutingNode;

    TrainNode *_frontNode;
    TrainNode *_endNode;

    bool _isDeccelerating;
    qreal _brakeS;

    BalisaElement *_balisaElement;
    DetectionRect *_detectionRect;
    QRectF _boundingRect;

    bool _isHidden;
    qreal minVal;
    qreal maxVal;
};

#endif // TRAINELEMENT_H
