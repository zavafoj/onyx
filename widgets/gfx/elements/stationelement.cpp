#include "stationelement.h"

#include <QMenu>
#include <QAction>

//!
//! \brief StationElement::StationElement
//! \param s
//! \param parent
//!
StationElement::StationElement(Station *s, QGraphicsItem *parent):
    RailElement(s, parent)
{
    this->_stationLabel = new LabelElement(s->stationName(), this);
    this->_stationLabel->setPos(  0.0,
                                -22.0);

    this->_endNode->setFlag(QGraphicsItem::ItemIsMovable, false);
    this->_beginNode->setFlag(QGraphicsItem::ItemIsMovable, false);

    this->updateModel();
}

//!
//! \brief StationElement::~StationElement
//!
StationElement::~StationElement()
{
//    qDebug() << Q_FUNC_INFO;

    delete this->_stationLabel;
}

//!
//! \brief StationElement::updateModel
//!
void StationElement::updateModel()
{
    RailElement::updateModel();

    if (this->_beginNode->y() == this->_endNode->y())
    {
        if (this->_beginNode->pos().x() > 0)
        {
            this->_stationLabel->setPos(  0.0,
                                         22.0);
        }
        else
        {
            this->_stationLabel->setPos(  0.0,
                                        -22.0);
        }
        this->_stationLabel->setRotation(0);
    }
    else if (this->_beginNode->x() == this->_endNode->x())
    {
        if (this->_beginNode->pos().y() > 0)
        {
            this->_stationLabel->setPos( 22.0,
                                          0.0);
            this->_stationLabel->setRotation(90);
        }
        else
        {
            this->_stationLabel->setPos(-22.0,
                                          0.0);
            this->_stationLabel->setRotation(-90);
        }
    }

    this->_stationLabel->setText(((Station*)this->_object)->stationName());
}

//!
//! \brief StationElement::lock
//! \param lock
//!
void StationElement::lock(bool lock)
{
    this->setFlag(ItemIsMovable, !lock);
    this->_locked = lock;
}

//!
//! \brief StationElement::stationName
//! \return
//!
QString StationElement::stationName() const
{
   return this->_stationLabel->text();
}

//!
//! \brief StationElement::setStationName
//! \param stationName
//!
void StationElement::setStationName(const QString &stationName)
{
    Station *station = (Station*) this->_object;
    station->setStationName(stationName);
}

//!
//! \brief StationElement::switchNodes
//!
void StationElement::switchNodes()
{
    switch(this->_beginNode->pos().toPoint().x())
    {
    case -60:
    {
        this->_beginNode->setPos(0, -60);
        this->_endNode->setPos(0,    60);
    }
        break;
    case 0:
    {
        if (this->_beginNode->pos().y() < 0.0)
        {
            this->_beginNode->setPos(60, 0);
            this->_endNode->setPos(-60,  0);
        }
        else
        {
            this->_beginNode->setPos(-60, 0);
            this->_endNode->setPos(60,    0);
        }
    }
        break;
    case 60:
    {
        this->_beginNode->setPos(0, 60);
        this->_endNode->setPos(0,  -60);
    }
        break;
    default:
        qDebug() << "CHUJ";
        break;
    }
}

//!
//! \brief StationElement::paint
//! \param painter
//!
void StationElement::paint(QPainter *painter,
                           const QStyleOptionGraphicsItem *,
                           QWidget *)
{
    RailElement::paint(painter, nullptr, nullptr);
}

//!
//! \brief StationElement::contextMenuEvent
//! \param event
//!
void StationElement::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    if (this->_locked)
        return;

    QMenu m;
    QAction actionChangDirection(qApp->tr("Rotate"), &m);
    m.addAction(&actionChangDirection);
    QAction *res = m.exec(event->screenPos());

    if (res == &actionChangDirection)
    {
        this->switchNodes();
    }
}

