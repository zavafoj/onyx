#include "graphicsscene.h"
#include <QMessageBox>
#include <QGraphicsView>
#include <QTimer>
#include <QGraphicsItemAnimation>
#include <qmath.h>

#define SCENERECT QRectF(-1920, -1080, 3840, 2160)

//!
//! \brief GraphicsScene::GraphicsScene
//! \param parent
//!
GraphicsScene::GraphicsScene(QObject *parent) :
    QGraphicsScene(parent),
    _centerAnimation(nullptr),
    _locked(false),
    _focusedTrain(nullptr)
{
    this->setBackgroundBrush(QBrush(Qt::lightGray));
    this->setSceneRect(SCENERECT);

    this->_inspector = new RouteInspector(this);
}

//!
//! \brief GraphicsScene::~GraphicsScene
//!
GraphicsScene::~GraphicsScene()
{

}

//!
//! \brief GraphicsScene::hoverObject
//! \param object
//!
void GraphicsScene::hoverObject(Object *object)
{
    QGraphicsItem *item = this->_elements[object];
    if (item)
    {
        this->_focusedTrain = item->type() == QGraphicsItem::UserType + Object::OT_Train
                                        ? item
                                        : nullptr;

        this->_centerAnimation = new QPropertyAnimation(this, "centerPosition", this);
        this->_centerAnimation->setDuration(400);
        this->_centerAnimation->setEasingCurve(QEasingCurve::OutCubic);

        QGraphicsView *parentView = ((QGraphicsView *)this->parent());
        const QPointF currentCenter = parentView->mapToScene(parentView->viewport()->rect().center());

        this->_centerAnimation->setStartValue(currentCenter);
        this->_centerAnimation->setEndValue(item->pos());

        this->_centerAnimation->start(QAbstractAnimation::DeleteWhenStopped);

        emit this->objectSelected(object);
    }
}

//!
//! \brief GraphicsScene::placeElement
//! \param o
//!
void GraphicsScene::placeElement(Object *object)
{
    if (this->_locked)
        return;

    QGraphicsItem *graphicsItem = nullptr;
    switch (object->type())
    {
    case Object::OT_Rail:
        graphicsItem = this->createElement<Rail, RailElement>(object);
        break;
    case Object::OT_Crossover:
        graphicsItem = this->createElement<Crossover, CrossoverElement>(object);
        this->connect(object,       SIGNAL(modelUpdated()),
                      this,         SLOT(updateRequested()));
        break;
    case Object::OT_Semaphore:
        graphicsItem = this->createElement<Semaphore, SemaphoreElement>(object);
        this->connect(object,       SIGNAL(modelUpdated()),
                      this,         SLOT(updateRequested()));
        break;
    case Object::OT_Station:
        graphicsItem = this->createElement<Station, StationElement>(object);
        this->connect(object,       SIGNAL(modelUpdated()),
                      this,         SLOT(updateRequested()));
        break;
    case Object::OT_Terminator:
        graphicsItem = this->createElement<Terminator, TerminatorElement>(object);
        break;
    case Object::OT_Balisa:
        graphicsItem = this->createElement<Balisa, BalisaElement>(object);
        break;
    case Object::OT_Train:
        graphicsItem = this->createElement<Train, TrainElement>(object);
        break;
    default:
        break;
    }

    if (graphicsItem)
    {
        this->_elements.insert(object, graphicsItem);
        this->connect(object,           SIGNAL(destroyed(QObject*)),
                      this,             SLOT(objectRemoved(QObject*)));
    }
}

//!
//! \brief GraphicsScene::clear
//!
void GraphicsScene::clear()
{
    QGraphicsScene::clear();
    this->clearRoutes();
    this->_elements.clear();
    this->_routesForObject.clear();
    this->_trains.clear();
    this->_focusedTrain = nullptr;
}

//!
//! \brief GraphicsScene::objectRemoved
//! \param object
//!
void GraphicsScene::objectRemoved(QObject *object)
{
    this->_elements.remove(object);
}

//!
//! \brief GraphicsScene::updateRequested
//! \param object
//!
void GraphicsScene::updateRequested()
{
    Object *sender = qobject_cast<Object*>(this->sender());

    if (!sender)
        return;

    switch (sender->type())
    {
    case Object::OT_Crossover:
        ((CrossoverElement *)this->_elements[sender])->updateModel();
        break;
    case Object::OT_Station:
        ((StationElement *)this->_elements[sender])->updateModel();
        break;
    case Object::OT_Semaphore:
        ((SemaphoreElement *)this->_elements[sender])->updateModel();
        break;
    default:
        break;
    }
}

//!
//! \brief GraphicsScene::keyPressEvent
//! \param event
//!
void GraphicsScene::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Delete:
        if (!this->_locked)
        {
            if (!this->selectedItems().isEmpty())
            {
                try
                {
                    QList<QGraphicsItem *> listToDelete;
                    foreach (QGraphicsItem *item, this->selectedItems())
                    {
                        AbstractRailElement *element = dynamic_cast<AbstractRailElement*>(item);
                        if (element)
                        {
                            foreach (QGraphicsItem *child, element->childItems())
                            {
                                EndPart *endPart = dynamic_cast<EndPart*>(child);
                                if (endPart)
                                    if (endPart->valid())
                                        throw tr("Unable to remove connected element.\n"
                                                 "Change its position and try again");
                            }
                            listToDelete.push_back(element);
                            continue;
                        }

                        BalisaElement *balisa = dynamic_cast<BalisaElement*>(item);
                        if (balisa)
                        {
                            listToDelete.push_back(balisa);
                        }

                        TrainElement *train = dynamic_cast<TrainElement*>(item);
                        if (train)
                        {
                            listToDelete.push_back(train);
                        }
                    }
                    foreach(QGraphicsItem *item, listToDelete)
                    {
                        this->removeItem(item);
                        delete item;
                    }
                    this->update();
                }
                catch (QString &errstr)
                {
                    QMessageBox b;
                    b.setWindowTitle(tr("Error"));
                    b.setIcon(QMessageBox::Critical);
                    b.setText(errstr);
                    b.addButton(tr("Ok"),QMessageBox::AcceptRole);
                    b.exec();
                }
            }
        }
        break;
    case Qt::Key_A:
    {
        if (this->selectedItems().size() != 2)
            break;

        if (!this->_locked)
            break;

        if (this->_routes.size() == 0)
            break;

        QList<Object*> selectedObjects;
        foreach (QGraphicsItem *item, this->selectedItems())
        {
            switch (item->type())
            {
            case QGraphicsItem::UserType + Object::OT_Semaphore:
            case QGraphicsItem::UserType + Object::OT_Terminator:
            {
                AbstractRailElement *are = (AbstractRailElement*) item;
                selectedObjects.push_back(are->object());
            }
                break;
            }
        }

        if (selectedObjects.size() == 2)
        {
            QList <Route*> tempRoutes;
            foreach (Route *r, this->_routes)
            {
                const QList <Object *> elements = r->elements();

                if ((elements.first() == selectedObjects[0] &&
                     elements.last()  == selectedObjects[1]) ||
                    (elements.first() == selectedObjects[1] &&
                     elements.last()  == selectedObjects[0]))
                {
                    tempRoutes.push_back(r);
                }
            }

            if (tempRoutes.isEmpty())
                break;

            Route *r = tempRoutes.last();
            r->setActive();

            foreach (QGraphicsItem* item, this->items())
            {
                item->setSelected(false);
            }

            foreach (Object *routeElement, r->elements())
            {
                foreach (QGraphicsItem *item, this->items())
                {
                    AbstractGraphicsElement *element = dynamic_cast<AbstractGraphicsElement*>(item);
                    if (element)
                    {
                        if (routeElement == element->object())
                            element->setSelected(true);
                    }
                }
            }

//            r->createPath();
//            qDebug() << r->path();
//            QPolygonF polygon;
//            foreach (QPointF point, r->path())
//            {
//                polygon << point;
//            }
//            QPainterPath pp;
//            pp.addPolygon(polygon);

//            this->addPath(pp);

            break;
        }
    }
        break;
    case Qt::Key_R:
    {
//        this->clearSelection();
        this->setSceneRect(this->itemsBoundingRect());
        QImage image(this->sceneRect().size().toSize(), QImage::Format_ARGB32);
        image.fill(Qt::transparent);

        QPainter painter(&image);
        painter.setRenderHint(QPainter::HighQualityAntialiasing);
        this->render(&painter);
        image.save("file_name.png");
    }
        break;
    default:
        break;
    }

    QGraphicsScene::keyPressEvent(event);
}

//!
//! \brief GraphicsScene::addRouteForObject
//! \param r
//! \param o
//!
void GraphicsScene::addRouteForObject(Route *r, Object *o)
{
    if (!this->_routesForObject.keys().contains(o))
    {
        QList<Route*> emptyList;
        this->_routesForObject.insert(o, emptyList);
    }
    this->_routesForObject[o].push_back(r);
}

//!
//! \brief GraphicsScene::focusedTrain
//! \return
//!
QGraphicsItem *GraphicsScene::focusedTrain() const
{
    return this->_focusedTrain;
}

//!
//! \brief GraphicsScene::setFocusedTrain
//! \param focusedTrain
//!
void GraphicsScene::setFocusedTrain(QGraphicsItem *focusedTrain)
{
    this->_focusedTrain = focusedTrain;
}

//!
//! \brief GraphicsScene::locked
//! \return
//!
bool GraphicsScene::locked() const
{
    return this->_locked;
}

//!
//! \brief GraphicsScene::setLocked
//! \param locked
//!
void GraphicsScene::setLocked(bool locked)
{
    this->_locked = locked;
}

//!
//! \brief GraphicsScene::lock
//!
void GraphicsScene::lock()
{
    foreach (QGraphicsItem *item, this->items())
    {
        switch (item->type())
        {
        case QGraphicsItem::UserType + Object::OT_Rail:
        case QGraphicsItem::UserType + Object::OT_Semaphore:
            ((RailElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Station:
            ((StationElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Terminator:
            ((TerminatorElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Crossover:
            ((CrossoverElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Balisa:
            ((BalisaElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Train:
            ((TrainElement*)item)->lock(true);
            break;
        default:
            break;
        }
    }

    this->_locked = false;
}

//!
//! \brief GraphicsScene::availableRoutesForStartingNode
//! \param o
//! \return
//!
Route *GraphicsScene::availableRouteForStartingNode(Object *o)
{
    foreach(Route *route, this->_routes)
    {
        if (route->state() == Route::RS_Unavailable)
            continue;

//        if (route->exhaused())
//            continue;

        if (route->elements().first() == o)
            return route;
    }
    return nullptr;
}

//!
//! \brief GraphicsScene::availableRouteForEndingNode
//! \param o
//! \return
//!
Route *GraphicsScene::availableRouteForEndingNode(Object *o)
{
    foreach(Route *route, this->_routes)
    {
        if (route->state() == Route::RS_Unavailable)
            continue;

//        if (route->exhaused())
//            continue;

        if (route->elements().last() == o)
            return route;
    }
    return nullptr;
}

//!
//! \brief GraphicsScene::centerPosition
//! \return
//!
QPointF GraphicsScene::centerPosition() const
{
    return this->_centerPosition;
}

//!
//! \brief GraphicsScene::checkConnections
//! \return
//!
bool GraphicsScene::checkConnections()
{
    bool empty = true;
    foreach (QGraphicsItem *item, this->items())
    {
        switch (item->type())
        {
        case QGraphicsItem::UserType + 69:
            if (!((EndPart*)item)->checkValid())
            {
//                qDebug() << item->mapToScene(item->pos());
                Object * obj = ((AbstractGraphicsElement*)item->parentItem())->object();
                QString name = obj->objectName().isEmpty() ? QString("id: %1").arg(obj->getUniqueId()) : obj->objectName();
                throw tr("%1 %2\nis not properly connected")
                        .arg(Object::getObjectTypeTrString(obj->type()))
                        .arg(name);
            }

            empty = false;
            break;
        case QGraphicsItem::UserType + Object::OT_Rail:
        case QGraphicsItem::UserType + Object::OT_Semaphore:
            ((RailElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Station:
            ((StationElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Terminator:
            ((TerminatorElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Crossover:
        {
            Crossover* obj = (Crossover*) ((CrossoverElement*)item)->object();
            if (obj->state() == Crossover::CS_Unknown)
            {
                QString name = obj->objectName().isEmpty() ? QString("id: %1").arg(obj->getUniqueId()) : obj->objectName();
                throw tr("%1 %2\nis in invalid state")
                        .arg(Object::getObjectTypeTrString(obj->type()))
                        .arg(name);
            }
            ((CrossoverElement*)item)->lock(true);
        }
            break;
        case QGraphicsItem::UserType + Object::OT_Balisa:
            ((BalisaElement*)item)->lock(true);
            break;
        case QGraphicsItem::UserType + Object::OT_Train:
            ((TrainElement*)item)->lock(true);
            break;
        default:
            break;
        }
    }

    if (empty)
        throw tr("Map is empty");

    this->_locked = true;
    return true;
}

//!
//! \brief GraphicsScene::uncheckConnections
//! \return
//!
void GraphicsScene::uncheckConnections()
{
    foreach (QGraphicsItem *item, this->items())
    {
        switch (item->type())
        {
        case QGraphicsItem::UserType + 69:
            ((EndPart*)item)->setValid(false);
            break;
        case QGraphicsItem::UserType + Object::OT_Rail:
        case QGraphicsItem::UserType + Object::OT_Semaphore:
            ((RailElement*)item)->lock(false);
            break;
        case QGraphicsItem::UserType + Object::OT_Station:
            ((StationElement*)item)->lock(false);
            break;
        case QGraphicsItem::UserType + Object::OT_Terminator:
            ((TerminatorElement*)item)->lock(false);
            break;
        case QGraphicsItem::UserType + Object::OT_Crossover:
            ((CrossoverElement*)item)->lock(false);
            break;
        case QGraphicsItem::UserType + Object::OT_Balisa:
            ((BalisaElement*)item)->lock(false);
            break;
        case QGraphicsItem::UserType + Object::OT_Train:
            ((TrainElement*)item)->lock(false);
            break;
        default:
            break;
        }
    }

    this->_locked = false;
}

//!
//! \brief GraphicsScene::hideNodePoints
//! \param hide
//!
void GraphicsScene::hideNodePoints(bool hide)
{
    foreach (QGraphicsItem *item, this->items())
    {
        if (item->type() == QGraphicsItem::UserType + 69)
        {
            item->setVisible(!hide);
        }
    }
}

//!
//! \brief GraphicsScene::findNodes
//! \param node
//! \param currentRoute
//! \return
//!
void GraphicsScene::buildBranch(EndPart *node,
                                Route *currentRoute)
{
    foreach (QGraphicsItem *item, node->collidingItems())
    {
        if (item->type() == QGraphicsItem::UserType + 69)
        {
            EndPart *nodePart = (EndPart *) item;
            AbstractRailElement *parent = ((AbstractRailElement*) nodePart->parentItem());
            if (parent)
            {
                switch (parent->type())
                {
                case  QGraphicsItem::UserType + Object::OT_Terminator:
                    currentRoute->pushToRoute(parent->object());
                    this->addRouteForObject(currentRoute, parent->object());
                    return;
                case  QGraphicsItem::UserType + Object::OT_Semaphore:
                {
                    SemaphoreElement *semaphore = (SemaphoreElement*) parent;
                    currentRoute->pushToRoute(semaphore->object());
                    this->addRouteForObject(currentRoute, parent->object());

                    if (!semaphore->isSimpleRail(nodePart))
                        return;

                    buildBranch(semaphore->availableConnections(nodePart)[0],
                                currentRoute);
                }
                    break;
                case  QGraphicsItem::UserType + Object::OT_Crossover:
                {
                    CrossoverElement *crossover = (CrossoverElement*) parent;
                    // jeżeli jest to branchNode to MUSI być branchState
                    if (nodePart == crossover->branchNode())
                    {
                        currentRoute->pushToRoute((Crossover*) crossover->object(),
                                                  Crossover::CS_Branch);

                        this->addRouteForObject(currentRoute, parent->object());

                        buildBranch(crossover->availableConnections(nodePart)[0],
                                    currentRoute);
                    }
                    else if (nodePart == crossover->endNode())
                    {
                        currentRoute->pushToRoute((Crossover*) crossover->object(),
                                                  Crossover::CS_Main);

                        this->addRouteForObject(currentRoute, parent->object());

                        buildBranch(crossover->availableConnections(nodePart)[0],
                                    currentRoute);
                    }
                    else
                    {
                        // Backup route
                        Route *r = new Route(currentRoute);
                        // Push main branch to route
                        currentRoute->pushToRoute((Crossover*) crossover->object(),
                                                  Crossover::CS_Main);

                        this->addRouteForObject(currentRoute, parent->object());

                        buildBranch(crossover->availableConnections(nodePart)[0],
                                    currentRoute);

                        r->pushToRoute((Crossover*) crossover->object(),
                                       Crossover::CS_Branch);
                        this->addRouteForObject(r, parent->object());

                        // Branch route
                        buildBranch(crossover->availableConnections(nodePart)[1],
                                    r);

                        this->_routes.push_back(r);
                    }
                }
                    break;
                default:
                    qDebug() << Object::getObjectTypeString(parent->object()->type());
                    currentRoute->pushToRoute(parent->object());
                    this->addRouteForObject(currentRoute, parent->object());
                    buildBranch(parent->availableConnections(nodePart)[0],
                                currentRoute);
                    break;
                }
            }
        }
    }
}

//!
//! \brief GraphicsScene::tick
//!
void GraphicsScene::tick()
{
    if (this->_routes.size() == 0)
        return;

    int val = it % this->_routes.size();

    Route *r = this->_routes.at(val);

    r->setActive();

    foreach (QGraphicsItem* item, this->items())
    {
        item->setSelected(false);
    }

    foreach (Object *routeElement, r->elements())
    {
        foreach (QGraphicsItem *item, this->items())
        {
            AbstractGraphicsElement *element = dynamic_cast<AbstractGraphicsElement*>(item);
            if (element)
            {
                if (routeElement == element->object())
                    element->setSelected(true);
            }
        }
    }


    it++;
}

//!
//! \brief GraphicsScene::assignPathToTrain
//! \param route
//! \param train
//!
void GraphicsScene::assignPathToTrain(Route *route, TrainElement *train)
{
    train->setRoute(route);
    if (route)
    {
        route->setExhaused(true);

        Semaphore *s = qobject_cast<Semaphore*>(route->elements().first());
        if (s)
            s->setState(Semaphore::SS_Yellow);
    }
    this->update();
}

//!
//! \brief GraphicsScene::showTrainRoute
//! \param route
//!
void GraphicsScene::showTrainRoute(Route *route)
{
    foreach (Object *routeElement, route->elements())
    {
        foreach (QGraphicsItem *item, this->items())
        {
            AbstractGraphicsElement *element = dynamic_cast<AbstractGraphicsElement*>(item);
            if (element)
            {
                if (routeElement == element->object())
                    element->setSelected(true);
            }
        }
    }

}

//!
//! \brief GraphicsScene::advance
//!
void GraphicsScene::advance()
{
    if (this->_focusedTrain)
    {
        ((QGraphicsView *)this->parent())->centerOn(this->_focusedTrain->pos());
    }

    QGraphicsScene::advance();
}

//!
//! \brief GraphicsScene::demandStop
//!
void GraphicsScene::demandStop(const QString &reason)
{
    Q_UNUSED (reason)

    emit this->stopDemanded();

    QMessageBox::critical(this->views().first(),
                          tr("Simulation Error"),
                          reason);

}

//!
//! \brief GraphicsScene::findAnoherPath
//! \param train
//!
void GraphicsScene::findAnoherPath(Train *train)
{
    try
    {
        this->findRouteByObject(train);
    }
    catch (const QString &err)
    {
        QMessageBox::critical(this->views().first(),
                              tr("Error"),
                              err);
    }
}

//!
//! \brief GraphicsScene::tryConnect
//! \param train
//!
void GraphicsScene::tryConnect(Train *train)
{
    try
    {
        if (train->movement() == Train::TM_Stopped)
        {
            TrainElement *requestedTrain = (TrainElement*) this->_elements[train];
            requestedTrain->connectWithNeariestTrain();
        }
        else
        {
            throw QString(tr("Train has to be stopped."));
        }
    }
    catch (const QString &errmsg)
    {
        QMessageBox::critical(this->views().first(),
                              tr("Error"),
                              errmsg);
    }
}

//!
//! \brief GraphicsScene::tryDisconnect
//! \param train
//!
void GraphicsScene::tryDisconnect(Train *train)
{
    try
    {
        if (train->movement() == Train::TM_Stopped)
        {
            TrainElement *requestedTrain = (TrainElement*) this->_elements[train];
            requestedTrain->disconnectTrain();
        }
        else
        {
            throw QString(tr("Train has to be stopped."));
        }
    }
    catch (const QString &errmsg)
    {
        QMessageBox::critical(this->views().first(),
                              tr("Error"),
                              errmsg);
    }
}

//!
//! \brief GraphicsScene::createRoutes
//!
void GraphicsScene::createRoutes()
{
    this->clearRoutes();

    QHash<Terminator*, TerminatorElement*> terminators;
    QHash<Semaphore*, SemaphoreElement*> semaphores;
    QHash<Train*, TrainElement*> trains;

    // Filling terminators & semaphores list
    foreach (QObject *object, this->_elements.keys())
    {
        Object *modelObject = (Object*) object;

        switch (modelObject->type())
        {
        case Object::OT_Terminator:
        {
            Terminator *terminator = (Terminator*) object;
            TerminatorElement *terminatorElement = (TerminatorElement*) this->_elements[object];
            terminators.insert(terminator, terminatorElement);
        }
            break;
        case Object::OT_Semaphore:
        {
            Semaphore *semaphore = (Semaphore*) object;
            semaphore->setState(Semaphore::SS_Red);
            SemaphoreElement *semaphoreElement = (SemaphoreElement*) this->_elements[object];
            semaphores.insert(semaphore, semaphoreElement);
        }
            break;
        case Object::OT_Train:
        {
            Train *train = (Train*) object;
            this->connect(train,        SIGNAL(findPathRequired(Train*)),
                          this,         SLOT(findAnoherPath(Train*)));
            this->connect(train,        SIGNAL(connectionRequested(Train*)),
                          this,         SLOT(tryConnect(Train*)));
            this->connect(train,        SIGNAL(disconnectionRequested(Train*)),
                          this,         SLOT(tryDisconnect(Train*)));
            TrainElement *trainElement = (TrainElement*) this->_elements[object];
            this->_trains.push_back(trainElement);
            trains.insert(train, trainElement);
        }
            break;
        default:
            break;
        }
    }

    // Building road - starting from terminators
    foreach (Terminator *terminator, terminators.keys())
    {
        QList <EndPart*> nodes = terminators[terminator]->availableConnections();
        EndPart* node = nodes.takeFirst();
        Route *r = new Route(this);
        r->pushToRoute(terminator);
        this->addRouteForObject(r, terminator);

        this->buildBranch(node, r);

        this->_routes.push_back(r);
        //  todo: fillme
    }

    // Finish the job with semaphores
    foreach (Semaphore *semaphore, semaphores.keys())
    {
        Route *r = new Route(this);
        QList <EndPart*> nodes = semaphores[semaphore]->availableConnections();
        EndPart* node = nodes.takeFirst();
        r->pushToRoute(semaphore);
        this->addRouteForObject(r, semaphore);

        this->buildBranch(node, r);

        this->_routes.push_back(r);
    }

    // Init states
    foreach (Route *route, this->_routes)
    {
        route->initState();
        route->createPath();
    }

    // Paths are ready, assign paths to trains
    foreach (Train *train, trains.keys())
    {
        try
        {
            this->findRouteByObject(train, false);
        }
        catch (...)
        {
            TrainElement *trainElement = trains[train];
            this->removeItem(trainElement);
            this->_elements.remove(train);
            delete trainElement;
            this->update();
        }
    }
}

//!
//! \brief GraphicsScene::clearRoutes
//!
void GraphicsScene::clearRoutes()
{
    qDeleteAll(this->_routes);
    this->_routes.clear();
    this->_routesForObject.clear();
    this->_trains.clear();
}

//!
//! \brief GraphicsScene::findRouteByObject
//! \param t
//!
void GraphicsScene::findRouteByObject(Train *train, bool random) throw (QString)
{
    Object *routeElement = nullptr;
    TrainElement *trainElement = (TrainElement*) this->_elements[train];
    foreach (QGraphicsItem *item, trainElement->collidingItems())
    {
        switch (item->type())
        {
        case QGraphicsItem::UserType + Object::OT_Train:
            routeElement = nullptr;
            break;
        case QGraphicsItem::UserType + Object::OT_Rail:
            routeElement = ((RailElement *) item)->object();
            break;
        case QGraphicsItem::UserType + Object::OT_Station:
            routeElement = ((StationElement *) item)->object();
            break;
        }
    }
    if (routeElement)
    {
        QList <int> availableIndexes;
        if (this->_routesForObject[routeElement].isEmpty())
        {
            throw tr("No routes for object");
        }

        for (int i = 0; i < this->_routesForObject[routeElement].size(); i++)
        {
            const Route *route = this->_routesForObject[routeElement].at(i);
            if (route->state() == Route::RS_Available)
                availableIndexes << i;
        }

        if (availableIndexes.isEmpty())
            throw tr("No available routes for object");

        Route *r = random
                ? this->_routesForObject[routeElement].at(availableIndexes.at(this->it++ % availableIndexes.size()))
                : this->_routesForObject[routeElement].at(availableIndexes.first());

        if (r)
        {
            if (r->state() == Route::RS_Unavailable)
                throw tr("Route is not available");

            QLineF trainLine(trainElement->boundingRect().topLeft(),
                             trainElement->boundingRect().bottomRight());
            trainLine.translate(trainElement->pos());

            qreal length = 0.0;
            const QList <QPointF> points = r->path();
            for (int i = 0; i < (points.size() == 2) ? 1 : points.size() - 2; i++)
            {
                QPointF intersectionPoint(0, 0);
                QLineF line (points[i], points[i+1]);
                if (trainLine.intersect(line, &intersectionPoint) == QLineF::BoundedIntersection)
                {
                    length += QLineF(line.p1(), intersectionPoint).length();
                    break;
                }
                length += line.length();
            }

            this->assignPathToTrain(r, trainElement);

            qreal percent = r->painterPath().percentAtLength(length);
            trainElement->setPathProgress(percent);

            qDebug() << r;
        }
        else
        {
            throw tr("Route does not exists");
        }
    }
    else
    {
        throw tr("No such route element");
    }
}

//!
//! \brief GraphicsScene::setCenterPosition
//! \param centerPosition
//!
void GraphicsScene::setCenterPosition(const QPointF &centerPosition)
{
    if (this->_centerPosition != centerPosition)
    {
        this->_centerPosition = centerPosition;
        emit this->centerPositionChanged(centerPosition);

        ((QGraphicsView *)this->parent())->centerOn(centerPosition);
    }
}
