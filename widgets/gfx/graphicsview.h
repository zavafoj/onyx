#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QKeyEvent>

#include "graphicsscene.h"
#include "../../core/structure/elements/rootitem.h"

class GraphicsView : public QGraphicsView
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit GraphicsView(QWidget *parent = 0);

    RootItem *rootItem() const;
    void setRootItem(RootItem *rootItem);

    Object::ObjectType currentType() const;

    GraphicsScene *getScene() const;

signals:
    //!
    //! \brief elementPlaced
    //!
    void elementPlaced();
    //!
    //! \brief elementSelected
    //!
    void objectSelected(Object *object);

public slots:
    void reset(void);
    void setCurrentType(const Object::ObjectType &currentType);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    qreal _scaleFactor;
    GraphicsScene *_scene;
    bool _rotationEnabled;
    QPoint _initialPosition;
    RootItem *_rootItem;
    Object::ObjectType _currentType;
};

#endif // GRAPHICSVIEW_H
