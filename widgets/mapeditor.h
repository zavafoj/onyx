#ifndef MAPEDITOR_H
#define MAPEDITOR_H

#include <QWidget>
#include <QToolButton>
#include <QCloseEvent>
#include "components/buttonbox.h"
#include "core/structure/elements/objectitemmodel.h"

namespace Ui {
class MapEditor;
}

//!
//! \brief The MapEditor class
//!
class MapEditor : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit MapEditor(QWidget *parent = 0);
    virtual ~MapEditor();

    Object::ObjectType type() const;
    ButtonBox *buttonBox() const;

    RootItem *rootItem() const;

    bool valid() const;

signals:
    //!
    //! \brief copyDataRequested
    //!
    void copyDataRequested();
    //!
    //! \brief hidden
    //!
    void hidden();

public slots:
    void show();
    void clear();

protected slots:
    void elementChanged(Object::ObjectType newType);

protected:
    void retranslate();
    void changeEvent(QEvent *event);
    void closeEvent(QCloseEvent *event);
    void setCurrentElement(const QString &elementString);

private slots:
    void unlockButtonClicked();
    void lockButtonClicked();

private:
    Ui::MapEditor *ui;
    Object::ObjectType _type;

    RootItem *_rootItem;
    ObjectItemModel *_model;
    bool _valid;
};

#endif // MAPEDITOR_H
