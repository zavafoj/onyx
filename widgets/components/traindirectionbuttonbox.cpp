#include "traindirectionbuttonbox.h"

//!
//! \brief TrainDirectionButtonBox::TrainDirectionButtonBox
//! \param t
//! \param parent
//!
TrainDirectionButtonBox::TrainDirectionButtonBox(Train *t, QWidget *parent)
    : QWidget(parent),
      _train(t)
{
    this->_layout = new QGridLayout (this);

    this->_backwardButton = new QPushButton(this);
    this->_backwardButton->setCheckable(true);
    this->connect(this->_backwardButton,        SIGNAL(clicked()),
                  this,                         SLOT(directionButtonClicked()));
    this->_layout->addWidget(this->_backwardButton, 0, 0, 1, 1);

    this->_forwardButton = new QPushButton(this);
    this->_forwardButton->setCheckable(true);
    this->connect(this->_forwardButton,        SIGNAL(clicked()),
                  this,                         SLOT(directionButtonClicked()));
    this->_layout->addWidget(this->_forwardButton, 0, 1, 1, 1);

    this->_stopButton = new QPushButton(this);
    this->connect(this->_stopButton,            SIGNAL(clicked()),
                  this,                         SLOT(stopButtonClicked()));
    this->_layout->addWidget(this->_stopButton, 1, 0, 1, 2);

    this->connect(this->_train,                 SIGNAL(directionChanged(Train::TrainDirection)),
                  this,                         SLOT(setTrainDirection(Train::TrainDirection)));

    this->connect(this->_train,                 SIGNAL(movemendChanged(Train::TrainMovement)),
                  this,                         SLOT(setTrainMovement(Train::TrainMovement)));

    this->_findRouteButton = new QPushButton(this);
    this->connect(this->_findRouteButton,       SIGNAL(clicked()),
                  this->_train,                 SLOT(findAnotherPath()));
    this->_layout->addWidget(this->_findRouteButton, 2, 0, 1, 2);

    this->_infoLabel = new QLabel(this);
    this->_layout->addWidget(this->_infoLabel, 3, 0, 1, 1);

    this->_amountLabel = new QLabel(this);
    this->_amountLabel->setText(QString::number(t->addedComponents()));
    this->_layout->addWidget(this->_amountLabel, 3, 1, 1, 1);

    this->_joinButton = new QPushButton(this);
    this->connect(this->_joinButton,            SIGNAL(clicked()),
                  this->_train,                 SLOT(requestConnection()));

    this->_layout->addWidget(this->_joinButton, 4, 0, 1, 2);

    this->_unjoinButton = new QPushButton(this);
    this->connect(this->_unjoinButton,          SIGNAL(clicked()),
                  this->_train,                 SLOT(requestDisconnection()));

    this->_layout->addWidget(this->_unjoinButton, 5, 0, 1, 2);

    this->retranslate();

    this->setTrainDirection(t->direction());
    this->setTrainMovement(t->movement());
}

//!
//! \brief TrainDirectionButtonBox::~TrainDirectionButtonBox
//!
TrainDirectionButtonBox::~TrainDirectionButtonBox()
{

}

//!
//! \brief TrainDirectionButtonBox::setTrainDirection
//! \param direction
//!
void TrainDirectionButtonBox::setTrainDirection(Train::TrainDirection direction)
{
    Train *sender = qobject_cast<Train*>(this->sender());

    // jezeli sygnal nadany przez GUI
    if (!sender)
    {
        qDebug() << "Nadane z GUI";
        if (this->_train->movement() == Train::TM_Stopped)
        {
            this->_train->setDirection(direction);
        }
    }

    qDebug() << "Nadane z Traina";
    this->_currentDirection = this->_train->direction();

    bool forwardDir = this->_currentDirection == Train::TD_Forward;

    this->_forwardButton->setChecked(forwardDir);
    this->_backwardButton->setChecked(!forwardDir);
}

//!
//! \brief TrainDirectionButtonBox::setTrainMovement
//! \param movement
//!
void TrainDirectionButtonBox::setTrainMovement(Train::TrainMovement movement)
{
    if (movement == Train::TM_Stopped)
        this->_stopButton->setText("Start");
    else
        this->_stopButton->setText("Stop");
}

//!
//! \brief TrainDirectionButtonBox::retranslate
//!
void TrainDirectionButtonBox::retranslate()
{
    this->_forwardButton->setText(tr("Forward"));
    this->_backwardButton->setText(tr("Backward"));

    this->_stopButton->setText(tr("Start"));
    this->_findRouteButton->setText(tr("Change route"));

    this->_infoLabel->setText(tr("Trains:"));
    this->_joinButton->setText(tr("Join Trains"));
    this->_unjoinButton->setText(tr("Disconnect Trains"));
}

//!
//! \brief TrainDirectionButtonBox::directionButtonClicked
//!
void TrainDirectionButtonBox::directionButtonClicked()
{
    QPushButton *button = qobject_cast<QPushButton*>(this->sender());
    if (!button)
        return;

    if (button == this->_backwardButton)
    {
        this->setTrainDirection(Train::TD_Backward);
    }
    else
    {
        this->setTrainDirection(Train::TD_Forward);
    }
}

//!
//! \brief TrainDirectionButtonBox::stopButtonClicked
//!
void TrainDirectionButtonBox::stopButtonClicked()
{
    if (this->_train->movement() != Train::TM_Stopped)
        this->_train->setStopDemand(true);
    else
        this->_train->setMovement(Train::TM_Moving_ACC);
}
