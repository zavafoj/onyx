#ifndef CONSOLEWIDGET_H
#define CONSOLEWIDGET_H

#include <QWidget>
#include <QTextEdit>
#include <QGridLayout>

#include "consoleinput.h"
#include "../../core/structure/elements/elements.h"

//!
//! \brief The ConsoleWidget class
//!
class ConsoleWidget : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit ConsoleWidget(QWidget *parent = 0);
    virtual ~ConsoleWidget();

    void initialize();

    RootItem *rootItem() const;
    void setRootItem(RootItem *rootItem);

signals:

public slots:
    void appendConsole(const Object::LogEntry &entry);
    void appendConsole(const QString &entry);
    void objectInserted(Object *object);

protected slots:
    void consoleQuery(const QString &query);

private:
    QGridLayout *_layout;
    QTextEdit *_console;
    ConsoleInput *_input;
    RootItem *_rootItem;
};

#endif // CONSOLEWIDGET_H
