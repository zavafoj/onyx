#ifndef CONSOLEINPUT_H
#define CONSOLEINPUT_H

#include <QLineEdit>
#include <QKeyEvent>
#include <QEvent>

//!
//! \brief The Console class
//!
class ConsoleInput : public QLineEdit
{
    Q_OBJECT
    Q_CLASSINFO ("TemplateAuthor", "Wojciech Ossowski")
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit ConsoleInput(QWidget *parent = 0);
    virtual ~ConsoleInput (void);

signals:
    void consoleQuery(const QString &query);

protected:
    bool eventFilter(QObject *object, QEvent *event);
    QStringList _commands;
    int _commandIndex;
};

#endif // CONSOLEINPUT_H
