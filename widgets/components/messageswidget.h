#ifndef MESSAGESWIDGET_H
#define MESSAGESWIDGET_H

#include <QWidget>
#include <QTextEdit>
#include <QHBoxLayout>

#include "../../core/structure/elements/elements.h"

//!
//! \brief The MessagesWidget class
//!
class MessagesWidget : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit MessagesWidget(QWidget *parent = 0);
    virtual ~MessagesWidget();

signals:

public slots:
    void clear(void);
    void setCurrentObject(Object *object);

protected slots:
    void objectDestroyed();

protected slots:
    void appendActiveLog(const Object::LogEntry &entry);

private:
    Object *_currentObject;
    QTextEdit *_log;
    QHBoxLayout *_layout;
};

#endif // MESSAGESWIDGET_H
