#ifndef TRAINDIRECTIONBUTTONBOX_H
#define TRAINDIRECTIONBUTTONBOX_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include "../../core/structure/elements/train.h"

class TrainDirectionButtonBox : public QWidget
{
    Q_OBJECT
public:
    explicit TrainDirectionButtonBox(Train *t, QWidget *parent = 0);
    virtual ~TrainDirectionButtonBox();

signals:
    void directionChanged(Train::TrainDirection direction);

public slots:
    void setTrainDirection(Train::TrainDirection direction);
    void setTrainMovement(Train::TrainMovement movement);
    void retranslate();

protected slots:
    void directionButtonClicked();
    void stopButtonClicked();

private:
    Train *_train;
    Train::TrainDirection _currentDirection;
    Train::TrainMovement _currentMovement;

    QPushButton *_backwardButton;
    QPushButton *_forwardButton;
    QPushButton *_stopButton;
    QPushButton *_findRouteButton;
    QPushButton *_joinButton;
    QPushButton *_unjoinButton;
    QLabel *_infoLabel;
    QLabel *_amountLabel;
    QGridLayout *_layout;
};

#endif // TRAINDIRECTIONBUTTONBOX_H
