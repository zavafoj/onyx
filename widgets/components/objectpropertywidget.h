#ifndef OBJECTPROPERTYWIDGET_H
#define OBJECTPROPERTYWIDGET_H

#include <QScrollArea>
#include <QGridLayout>
#include <QGroupBox>
#include <QVBoxLayout>
#include "traindirectionbuttonbox.h"
#include "../../core/structure/elements/elements.h"

//!
//! \brief The ObjectPropertyWidget class
//!
class ObjectPropertyWidget : public QGroupBox
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit ObjectPropertyWidget(QWidget *parent = 0);
    virtual ~ObjectPropertyWidget();

    bool isLocked() const;
    void setIsLocked(bool isLocked);

signals:

public slots:
    void clear(void);
    void setCurrentObject(Object *object);
    void updateInterface(void);

protected slots:
    void comboBoxItemSelected(int index);

private:
    Object *_currentObject;
    QScrollArea *_area;

    QVBoxLayout *_layout;
    QGridLayout *_areaLayout;
    QList <QWidget*> _elements;
    bool _isLocked;
};

#endif // OBJECTPROPERTYWIDGET_H
