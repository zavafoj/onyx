#ifndef EDITORBUTTON_H
#define EDITORBUTTON_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QVBoxLayout>
#include "../../core/structure/elements/object.h"

//!
//! \brief The EditorButton class
//!
class EditorButton : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit EditorButton(Object::ObjectType type, QWidget *parent = 0);
    virtual ~EditorButton();

    Object::ObjectType type() const;
    bool isToggled() const;
    void setToggled(bool toggled);

signals:
    //!
    //! \brief itemToggled
    //! \param toggled
    //!
    void itemChecked(bool toggled);
    //!
    //! \brief clicked
    //!
    void clicked();

public slots:
    void setCheckable(bool checkable);

protected slots:
    void retranslate();

protected:
    void changeEvent(QEvent *event);

private:
    QLabel *_titleLabel;
    QPushButton *_button;
    QVBoxLayout *_layout;
    Object::ObjectType _type;
};

#endif // EDITORBUTTON_H
