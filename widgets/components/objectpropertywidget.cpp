#include "objectpropertywidget.h"
#include <QtWidgets>

//!
//! \brief ObjectPropertyWidget::ObjectPropertyWidget
//! \param parent
//!
ObjectPropertyWidget::ObjectPropertyWidget(QWidget *parent) :
    QGroupBox(parent),
    _currentObject(nullptr),
    _isLocked(false)
{
    this->_area = new QScrollArea(this);
    this->_areaLayout = new QGridLayout(this);
    this->_area->setLayout(this->_areaLayout);

    this->_layout = new QVBoxLayout(this);
    this->_layout->addWidget(this->_area);
    this->setLayout(this->_layout);
}

//!
//! \brief ObjectPropertyWidget::~ObjectPropertyWidget
//!
ObjectPropertyWidget::~ObjectPropertyWidget()
{

}

//!
//! \brief ObjectPropertyWidget::clear
//!
void ObjectPropertyWidget::clear()
{
    if (this->_currentObject != nullptr)
    {
        this->disconnect(this->_currentObject, 0, this, 0);
        this->_currentObject = nullptr;

        qDeleteAll(this->_elements);
        this->_elements.clear();
        this->setTitle("");
    }
}

//!
//! \brief ObjectPropertyWidget::setCurrentObject
//! \param object
//!
void ObjectPropertyWidget::setCurrentObject(Object *object)
{
    this->clear();

    if (object)
    {
        this->_currentObject = object;
        this->connect(object,       SIGNAL(destroyed()),
                      this,         SLOT(clear()));
    }

    this->updateInterface();
}

//!
//! \brief ObjectPropertyWidget::updateInterface
//!
void ObjectPropertyWidget::updateInterface()
{
    if (this->_currentObject != nullptr)
    {
        int row = 0;
        this->setTitle(tr("%1 id: %2").arg(Object::getObjectTypeTrString(this->_currentObject->type()))
                       .arg(QString::number(this->_currentObject->getUniqueId())));
        {
            QLabel *label = new QLabel(tr("Object Name"),this->_area);
            this->_areaLayout->addWidget(label, row, 0, 1, 1);
            this->_elements.push_back(label);

            QLineEdit *edit = new QLineEdit(this->_currentObject->objectName(), this->_area);
            this->connect(edit,                             SIGNAL(textEdited(QString)),
                          this->_currentObject,             SLOT(setName(QString)));

            this->_areaLayout->addWidget(edit, row++, 1, 1, 1);
            this->_elements.push_back(edit);
        }

        switch (this->_currentObject->type())
        {
        case Object::OT_Rail:
        {
            Rail *r = (Rail*) this->_currentObject;
            {
                QLabel *label = new QLabel(tr("Z-Value"),this->_area);
                this->_areaLayout->addWidget(label, row, 0, 1, 1);
                this->_elements.push_back(label);

                QSpinBox *edit = new QSpinBox(this->_area);
                edit->setReadOnly(this->_isLocked);
                edit->setRange(-100, 100);
                edit->setValue(r->zLevel());
                this->connect(edit,                             SIGNAL(valueChanged(int)),
                              r,                                SLOT(setZLevel(int)));
                this->_areaLayout->addWidget(edit, row++, 1, 1, 1);
                this->_elements.push_back(edit);
            }
        }
            break;
        case Object::OT_Semaphore:
        {
            Semaphore *s = (Semaphore*) this->_currentObject;
            {
                QLabel *label = new QLabel(tr("State"),this->_area);
                this->_areaLayout->addWidget(label, row, 0, 1, 1);
                this->_elements.push_back(label);

                QComboBox *edit = new QComboBox;
                edit->addItem(tr("Green"),  Semaphore::SS_Green);
                edit->addItem(tr("Yellow"), Semaphore::SS_Yellow);
                edit->addItem(tr("Red"),    Semaphore::SS_Red);

                edit->setCurrentIndex((int) s->state());

                this->connect(edit,         SIGNAL(currentIndexChanged(int)),
                              this,         SLOT(comboBoxItemSelected(int)));

                this->_areaLayout->addWidget(edit, row++, 1, 1, 1);
                this->_elements.push_back(edit);
            }
            {
                QLabel *label = new QLabel(tr("Maximum Speed"),this->_area);
                this->_areaLayout->addWidget(label, row, 0, 1, 1);
                this->_elements.push_back(label);

                QSpinBox *edit = new QSpinBox(this->_area);
                edit->setReadOnly(this->_isLocked);
                edit->setRange(10, 200);
                edit->setValue(s->vMax());
                this->connect(edit,                             SIGNAL(valueChanged(int)),
                              s,                                SLOT(setVMax(int)));
                this->_areaLayout->addWidget(edit, row++, 1, 1, 1);
                this->_elements.push_back(edit);
            }
        }
            break;
        case Object::OT_Crossover:
        {
            Crossover *co = (Crossover*) this->_currentObject;

            if (co->state() != Crossover::CS_Unknown)
            {
                QLabel *label = new QLabel(tr("State"),this->_area);
                this->_areaLayout->addWidget(label, row, 0, 1, 1);
                this->_elements.push_back(label);

                QComboBox *edit = new QComboBox(this->_area);

                edit->addItem(tr("Main Rail"), Crossover::CS_Main);
                edit->addItem(tr("Branch Rail"), Crossover::CS_Branch);

                edit->setCurrentIndex((co->state() == Crossover::CS_Main) ? 0 : 1);

                this->connect(edit,         SIGNAL(currentIndexChanged(int)),
                              this,         SLOT(comboBoxItemSelected(int)));

                this->_areaLayout->addWidget(edit, row++, 1, 1, 1);
                this->_elements.push_back(edit);
            }
        }
            break;
        case Object::OT_Balisa:
            break;
        case Object::OT_Station:
        {
            Station *s = (Station*) this->_currentObject;

            QLabel *stationNameLabel = new QLabel(tr("Station Name"),this->_area);
            this->_areaLayout->addWidget(stationNameLabel, row, 0, 1, 1);
            this->_elements.push_back(stationNameLabel);

            QLineEdit *stationNameEdit = new QLineEdit(s->stationName(), this->_area);
            stationNameEdit->setReadOnly(this->_isLocked);
            this->connect(stationNameEdit,                  SIGNAL(textEdited(QString)),
                          s,                                SLOT(setStationName(QString)));
            this->_areaLayout->addWidget(stationNameEdit, row++, 1, 1, 1);
            this->_elements.push_back(stationNameEdit);
        }
            break;
        case Object::OT_Train:
        {
            Train *t = (Train*) this->_currentObject;

            // masa
            {
                QLabel *priorityLabel= new QLabel(tr("Train priority"), this->_area);
                this->_areaLayout->addWidget(priorityLabel, row, 0, 1, 1);
                this->_elements.push_back(priorityLabel);

                QSpinBox *prioritySpinBox = new QSpinBox(this->_area);
                prioritySpinBox->setReadOnly(this->_isLocked);
                prioritySpinBox->setRange(0, 200);
                prioritySpinBox->setValue(t->priority());
                this->connect(t,                    SIGNAL(priorityChanged(int)),
                              prioritySpinBox,      SLOT(setValue(int)));

                this->connect(prioritySpinBox,      SIGNAL(valueChanged(int)),
                              t,                    SLOT(setPriority(int)));

                this->_areaLayout->addWidget(prioritySpinBox, row++, 1, 1, 1);
                this->_elements.push_back(prioritySpinBox);
            }

            // masa
            {
                QLabel *massLabel = new QLabel(tr("Train Mass [t]"), this->_area);
                this->_areaLayout->addWidget(massLabel, row, 0, 1, 1);
                this->_elements.push_back(massLabel);

                QDoubleSpinBox *massSpinBox = new QDoubleSpinBox(this->_area);
                massSpinBox->setReadOnly(this->_isLocked);
                massSpinBox->setRange(1, 200);
                massSpinBox->setValue(t->mass());
                this->connect(t,                    SIGNAL(massChanged(qreal)),
                              massSpinBox,          SLOT(setValue(double)));

                this->connect(massSpinBox,          SIGNAL(valueChanged(double)),
                              t,                    SLOT(setMass(qreal)));

                this->_areaLayout->addWidget(massSpinBox, row++, 1, 1, 1);
                this->_elements.push_back(massSpinBox);
            }

            // przyspieszenie
            {
                QLabel *accLabel= new QLabel(tr("Train Acceleration [m/s^2]"), this->_area);
                this->_areaLayout->addWidget(accLabel, row, 0, 1, 1);
                this->_elements.push_back(accLabel);

                QDoubleSpinBox *accSpinBox= new QDoubleSpinBox(this->_area);
                accSpinBox->setReadOnly(this->_isLocked);
                accSpinBox->setRange(0.0000000000001, 10);
                accSpinBox->setValue(t->acceleration());
                this->connect(t,                    SIGNAL(accelerationChanged(qreal)),
                              accSpinBox,           SLOT(setValue(double)));

                this->connect(accSpinBox,           SIGNAL(valueChanged(double)),
                              t,                    SLOT(setAcceleration(qreal)));

                this->_areaLayout->addWidget(accSpinBox, row++, 1, 1, 1);
                this->_elements.push_back(accSpinBox);
            }

            // spowolnienie
            {
                QLabel *deccLabel= new QLabel(tr("Train Decceleration [m/s^2]"), this->_area);
                this->_areaLayout->addWidget(deccLabel, row, 0, 1, 1);
                this->_elements.push_back(deccLabel);

                QDoubleSpinBox *deccSpinBox = new QDoubleSpinBox(this->_area);
                deccSpinBox->setReadOnly(this->_isLocked);
                deccSpinBox->setRange(0.0000000000001, 10);
                deccSpinBox->setValue(t->decceleration());
                this->connect(t,                    SIGNAL(deccelerationChanged(qreal)),
                              deccSpinBox,          SLOT(setValue(double)));

                this->connect(deccSpinBox,          SIGNAL(valueChanged(double)),
                              t,                    SLOT(setDecceleration(qreal)));

                this->_areaLayout->addWidget(deccSpinBox, row++, 1, 1, 1);
                this->_elements.push_back(deccSpinBox);
            }
            // vMax
            {
                QLabel *vMaxLabel = new QLabel(tr("Maximum Velocity [km/h]"), this->_area);
                this->_areaLayout->addWidget(vMaxLabel, row, 0, 1, 1);
                this->_elements.push_back(vMaxLabel);

                QSpinBox *vMaxSpinBox = new QSpinBox(this->_area);
                vMaxSpinBox->setReadOnly(this->_isLocked);
                vMaxSpinBox->setRange(20, 200);
                vMaxSpinBox->setValue(t->maxVelocity());
                this->connect(t,                    SIGNAL(maxVelocityChanged(int)),
                              vMaxSpinBox,          SLOT(setValue(int)));

                this->connect(vMaxSpinBox,          SIGNAL(valueChanged(int)),
                              t,                    SLOT(setMaxVelocity(int)));

                this->_areaLayout->addWidget(vMaxSpinBox, row++, 1, 1, 1);
                this->_elements.push_back(vMaxSpinBox);
            }

            // prędkość
            QLabel *velocityLabel = new QLabel(tr("Train Velocity [km/h]"), this->_area);
            this->_areaLayout->addWidget(velocityLabel, row, 0, 1, 1);
            this->_elements.push_back(velocityLabel);

            QSpinBox *velocitySpinBox = new QSpinBox(this->_area);
            velocitySpinBox->setReadOnly(true);
            velocitySpinBox->setValue(t->velocity());
            this->connect(t,                    SIGNAL(velocityChanged(int)),
                          velocitySpinBox,      SLOT(setValue(int)));

            this->_areaLayout->addWidget(velocitySpinBox, row++, 1, 1, 1);
            this->_elements.push_back(velocitySpinBox);

            // kontrola ruchu
            if (this->_isLocked)
            {
                TrainDirectionButtonBox *trainDirBB = new TrainDirectionButtonBox(t, this);
                this->_areaLayout->addWidget(trainDirBB, row, 0, 6, 2);
                this->_elements.push_back(trainDirBB);
                row += 6;
            }

            break;
        }
        default:
            break;
        }

        this->_areaLayout->addItem(new QSpacerItem(10, 10,
                                                   QSizePolicy::Minimum,
                                                   QSizePolicy::MinimumExpanding),
                                   row, 0);
    }
}

//!
//! \brief ObjectPropertyWidget::comboBoxItemSelected
//! \param index
//!
void ObjectPropertyWidget::comboBoxItemSelected(int index)
{
    Q_UNUSED (index)

    if (!this->_currentObject)
        return;

    QComboBox *combo = qobject_cast<QComboBox*>(this->sender());
    if (!combo)
        return;

    QVariant data = combo->currentData();
    switch (this->_currentObject->type())
    {
    case Object::OT_Semaphore:
        ((Semaphore*) this->_currentObject)->setState((Semaphore::SemaphoreStates) data.toInt());
        break;
    case Object::OT_Crossover:
        ((Crossover*) this->_currentObject)->setState((Crossover::CrossoverStates) data.toInt());
        break;
    default:
        break;
    }
}

//!
//! \brief ObjectPropertyWidget::isLocked
//! \return
//!
bool ObjectPropertyWidget::isLocked() const
{
    return this->_isLocked;
}

//!
//! \brief ObjectPropertyWidget::setIsLocked
//! \param isLocked
//!
void ObjectPropertyWidget::setIsLocked(bool isLocked)
{
    this->_isLocked = isLocked;
}
