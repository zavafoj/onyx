#include "buttonbox.h"
#include <QSpacerItem>

#include "../mapeditor.h"
//!
//! \brief ButtonBox::ButtonBox
//! \param parent
//!
ButtonBox::ButtonBox(QWidget *parent) :
    QToolBox(parent),
    _currentType(Object::OT_Unknown)
{
    // Build Railroad Objects
    this->_railroadObjects = new QWidget(this);
    this->_railroadObjectsLayout = new QGridLayout(this->_railroadObjects);
    // Create Elements List
    QList <Object::ObjectType> railroadTypes;
    railroadTypes << Object::OT_Rail
                  << Object::OT_Crossover
                  << Object::OT_Semaphore
                  << Object::OT_Station
                  << Object::OT_Terminator;

    this->addButtonsToLayout(railroadTypes, this->_railroadObjectsLayout);
    // Add Spacers
    this->_railroadObjectsLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding,
                                                          QSizePolicy::MinimumExpanding),
                                          this->_railroadObjectsLayout->rowCount(),    // row
                                          0,    // column
                                          1,    // rowspan
                                          2);   // colspan

    this->_railroadObjectsLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding,
                                                          QSizePolicy::MinimumExpanding),
                                          this->_railroadObjectsLayout->rowCount()-1,     // row
                                          this->_railroadObjectsLayout->columnCount());   // coumn

    this->addItem(this->_railroadObjects, QString());

    // Build Dynamic Objects
    this->_dynamicObjects = new QWidget(this);
    this->_dynamicObjectsLayout = new QGridLayout(this->_dynamicObjects);
    // Add Spacers
    this->_dynamicObjectsLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding,
                                                         QSizePolicy::MinimumExpanding),
                                         this->_dynamicObjectsLayout->rowCount(),    // row
                                         0,    // column
                                         1,    // rowspan
                                         2);   // colspan

    this->_dynamicObjectsLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding,
                                                         QSizePolicy::MinimumExpanding),
                                         this->_dynamicObjectsLayout->rowCount()-1,     // row
                                         this->_dynamicObjectsLayout->columnCount());   // coumn
    // Create Elements List
    QList <Object::ObjectType> dynamicTypes;
    dynamicTypes << Object::OT_Balisa << Object::OT_Train;

    this->addButtonsToLayout(dynamicTypes, this->_dynamicObjectsLayout);

//    // Add train Placement button
    this->addItem(this->_dynamicObjects, QString());

    this->retranslate();
}

//!
//! \brief ButtonBox::~ButtonBox
//!
ButtonBox::~ButtonBox()
{

}

//!
//! \brief ButtonBox::currentType
//! \return
//!
Object::ObjectType ButtonBox::currentType() const
{
    return this->_currentType;
}

//!
//! \brief ButtonBox::clear
//!
void ButtonBox::clear()
{
    QList <EditorButton*> allButtons = this->findChildren<EditorButton*>();
    foreach (EditorButton *button, allButtons)
    {
        if (button->isToggled())
            button->setToggled(false);
    }

    this->setCurrentType(Object::OT_Unknown);
}

//!
//! \brief ButtonBox::addButtonsToLayout
//! \param buttonType
//! \param layout
//!
void ButtonBox::addButtonsToLayout(const QList<Object::ObjectType> &buttonTypes,
                                   QGridLayout *layout)
{
    int row = 0;
    int i = 0;
    foreach (Object::ObjectType type, buttonTypes)
    {
        EditorButton *button = new EditorButton(type, this);
        this->connect(button,               SIGNAL(itemChecked(bool)),
                      this,                 SLOT(elementToggled(bool)));

        int inc = i % 2;
        layout->addWidget(button, row, inc, Qt::AlignHCenter | Qt::AlignTop);
        row+=inc;
        i++;
    }
}

//!
//! \brief ButtonBox::retranslate
//!
void ButtonBox::retranslate()
{
    this->setItemText(this->indexOf(this->_railroadObjects), tr("Railroad Objects"));
    this->setItemText(this->indexOf(this->_dynamicObjects), tr("Dynamic Objects"));
}

//!
//! \brief ButtonBox::changeEvent
//! \param event
//!
void ButtonBox::changeEvent(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::LanguageChange :
    {
        this->retranslate();
    }
        break;

    default:
    {

    }
    }

    QWidget::changeEvent(event);
}

//!
//! \brief ButtonBox::contextMenuEvent
//! \param event
//!
void ButtonBox::contextMenuEvent(QContextMenuEvent *event)
{
    this->clear();
    QToolBox::contextMenuEvent(event);
}

//!
//! \brief ButtonBox::elementToggled
//! \param toggled
//!
void ButtonBox::elementToggled(bool toggled)
{
    qDebug() << this->sender();
    if (toggled)
    {
        EditorButton *clickedButton = qobject_cast<EditorButton*>(this->sender());
        if (clickedButton)
        {
            QList <EditorButton*> allButtons = this->findChildren<EditorButton*>();
            foreach (EditorButton *button, allButtons)
            {
                if (button->isToggled() && button != clickedButton)
                    button->setToggled(false);
            }
            this->setCurrentType(clickedButton->type());
        }
    }
    else
    {
        this->setCurrentType(Object::OT_Unknown);
    }
}

//!
//! \brief ButtonBox::setCurrentType
//! \param newType
//!
void ButtonBox::setCurrentType(Object::ObjectType newType)
{
    if (this->_currentType != newType)
    {
        this->_currentType = newType;
        emit this->currentTypeChanged(newType);
    }
}

//!
//! \brief ButtonBox::placeTrainClicked
//!
void ButtonBox::placeTrainClicked()
{
    qDebug() << Q_FUNC_INFO << this->sender();
}

//!
//! \brief ButtonBox::locked
//! \return
//!
bool ButtonBox::locked() const
{
    return this->_locked;
}

//!
//! \brief ButtonBox::setLocked
//! \param locked
//!
void ButtonBox::setLocked(bool locked)
{
    this->clear();
    this->_locked = locked;

    this->_railroadObjects->setDisabled(locked);
    this->_dynamicObjects->setDisabled(locked);
}


