#include <QFileDialog>
#include <QDebug>
#include <QFile>
#include <QDataStream>
#include <QHeaderView>
#include <QMessageBox>
#include <QApplication>
#include <QClipboard>
#include <QVBoxLayout>
#include <QtWidgets>

#include "draggabletreeview.h"

DraggableTreeView::DraggableTreeView(QWidget *parent) :
    QTreeView(parent)
{
    this->setSelectionMode(QAbstractItemView::SingleSelection);

    this->connect(this,                     SIGNAL(doubleClicked(QModelIndex)),
                  this,                     SLOT(selectItem(QModelIndex)));

    this->setContextMenuPolicy(Qt::CustomContextMenu);

    this->connect(this,                     SIGNAL(customContextMenuRequested(QPoint)),
                  this,                     SLOT(showContextMenu(QPoint)));
}

//!
//! \brief DraggableTreeView::~DraggableTreeView
//!
DraggableTreeView::~DraggableTreeView()
{

}

//!
//! \brief DraggableTreeView::setModel
//! \param model
//!
void DraggableTreeView::setModel(ObjectItemModel *model)
{
    this->_model = model;
    QTreeView::setModel(model);
    this->connect(model,        SIGNAL(dataChanged(QModelIndex,QModelIndex)),
                  this,         SLOT(update(QModelIndex)));

    this->header()->resizeSection(0, 160);
}

//!
//! \brief DraggableTreeView::deleteItem
//!
void DraggableTreeView::deleteItem()
{
    if (this->_model)
    {
        Object *o = static_cast<Object*> (this->currentIndex().internalPointer());
        if (!o)
            return;

        this->_model->removeRow(this->currentIndex().row(),
                                this->currentIndex().parent());
    }
}

//!
//! \brief DraggableTreeView::elementDoubleClicked
//!
void DraggableTreeView::selectItem(QModelIndex index)
{
    if (index.isValid())
    {
        Object *o = static_cast<Object*> (index.internalPointer());

        if (!o)
            return;

        if (o->type() == Object::OT_Group)
            return;

        emit this->itemSelected(o);
    }
}

//!
//! \brief DraggableTreeView::copy
//!
void DraggableTreeView::copy()
{
    QClipboard *c = QApplication::clipboard();
    c->clear();

    QMimeData *mimeData = this->_model->mimeData(this->selectedIndexes());

    c->setMimeData(mimeData);
}

//!
//! \brief DraggableTreeView::paste
//!
void DraggableTreeView::paste()
{
    QClipboard *c = QApplication::clipboard();
    const QMimeData *mimeData = c->mimeData();

    Qt::DropAction action = Qt::CopyAction;
    QModelIndex currentIndex = this->currentIndex();
    this->_model->dropMimeData(mimeData,
                               action,
                               currentIndex.row(),
                               currentIndex.column(),
                               currentIndex);
}

//!
//! \brief DraggableTreeView::showContextMenu
//! \param pos
//!
void DraggableTreeView::showContextMenu(const QPoint &pos)
{
    QModelIndex current = this->indexAt(pos);

    Object *o = static_cast<Object*> (current.internalPointer());
    if (!o)
        o = this->_model->rootItem();

    QMenu menu(this);
    QAction actionDisplay(&menu);
    actionDisplay.setText(tr("Display"));

    QAction actionRename(&menu);
    actionRename.setText(tr("Rename"));

    if (current.isValid())
    {
        menu.addAction(&actionDisplay);

        menu.addSeparator();

        if (o->type() != Object::OT_Group)
        {
            menu.addSeparator();
            menu.addAction(&actionRename);
        }
    }

    QAction *selected = menu.exec(this->mapToGlobal(pos));
    if (selected == &actionDisplay)
    {
        this->selectItem(current);
    }
    else if (selected == &actionRename)
    {
        QDialog d(this);
        d.setWindowIcon(QIcon(":/gfx/icons/Tools.png"));
        d.setWindowTitle(tr("New Name"));
        QVBoxLayout layout(&d);
        QLabel label (d.windowTitle(), &d);
        layout.addWidget(&label);
        QLineEdit edit(o->objectName(), &d);
        layout.addWidget(&edit);
        QPushButton button (tr("Submit"));
        layout.addWidget(&button);
        d.setLayout(&layout);
        d.connect(&button,       SIGNAL(clicked()),
                  &d,            SLOT(accept()));

        if (d.exec() == QDialog::Accepted)
        {
            o->setObjectName(edit.text());
        }

        this->_model->update();
    }
}

//!
//! \brief DraggableTreeView::keyPressEvent
//! \param event
//!
void DraggableTreeView::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_X:
        if (event->modifiers() != Qt::ControlModifier)
            break;
    case Qt::Key_Delete:
        this->deleteItem();
        break;
    case Qt::Key_Return:
    case Qt::Key_Enter:
    {
        Object *o = static_cast<Object*>(this->currentIndex().internalPointer());
        if (o)
            emit this->itemSelected(o);
    }
        break;
    case Qt::Key_C:
    {
        if (event->modifiers() == Qt::ControlModifier)
            this->copy();
    }
        return;
    case Qt::Key_V:
    {
        if (event->modifiers() == Qt::ControlModifier)
            this->paste();
    }
        return;
    default:
        break;
    }

    QTreeView::keyPressEvent(event);
}

//!
//! \brief DraggableTreeView::focusOutEvent
//!
void DraggableTreeView::focusOutEvent(QFocusEvent *)
{
    qDebug() << "Focus Out";
    this->clearSelection();
}
