#include <QDebug>
#include "messageswidget.h"

//!
//! \brief MessagesWidget::MessagesWidget
//! \param parent
//!
MessagesWidget::MessagesWidget(QWidget *parent)
    : QWidget(parent),
      _currentObject(nullptr)
{
    this->_layout = new QHBoxLayout(this);

    this->_log = new QTextEdit(this);
    this->_log->setReadOnly(true);
    this->_layout->addWidget(this->_log);

    this->setLayout(this->_layout);
}

//!
//! \brief MessagesWidget::~MessagesWidget
//!
MessagesWidget::~MessagesWidget()
{

}

//!
//! \brief MessagesWidget::clear
//!
void MessagesWidget::clear()
{
    if (this->_currentObject != nullptr)
    {
        this->disconnect(this->_currentObject, 0, this, 0);
        this->_currentObject = nullptr;
    }
    this->_log->clear();
}

//!
//! \brief MessagesWidget::setCurrentObject
//! \param object
//!
void MessagesWidget::setCurrentObject(Object *object)
{
    this->clear();

    if (object)
    {
        foreach (const Object::LogEntry &entry, object->objectLog())
        {
            this->appendActiveLog(entry);
        }

        this->connect(object,   SIGNAL(logAppended(Object::LogEntry)),
                      this,     SLOT(appendActiveLog(Object::LogEntry)));
        this->connect(object,   SIGNAL(destroyed()),
                      this,     SLOT(objectDestroyed()));

        this->_currentObject = object;
    }
}

//!
//! \brief MessagesWidget::objectDestroyed
//!
void MessagesWidget::objectDestroyed()
{
    this->_currentObject = nullptr;
}

//!
//! \brief MessagesWidget::appendActiveLog
//! \param entry
//!
void MessagesWidget::appendActiveLog(const Object::LogEntry &entry)
{
    this->_log->append(QString("[%1]: %2").arg(entry._eventTime.toString("HH:mm:ss.zzz"))
                                          .arg(entry._message));
}

