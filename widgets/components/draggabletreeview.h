#ifndef DRAGGABLETREEVIEW_H
#define DRAGGABLETREEVIEW_H

#include <QDropEvent>
#include <QTreeView>
#include <QTimer>
#include <QKeyEvent>
#include <QFocusEvent>

#include "../../core/structure/elements/objectitemmodel.h"

//!
//! \brief The DraggableTreeView class
//!
class DraggableTreeView : public QTreeView
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit DraggableTreeView(QWidget *parent = 0);
    virtual ~DraggableTreeView();

    void setModel(ObjectItemModel *model);

signals:
    //!
    //! \brief itemDoubleClicked
    //! \param object
    //!
    void itemSelected(Object *object);

protected slots:
    void deleteItem();
    void selectItem(QModelIndex index);
    void copy();
    void paste();
    void showContextMenu(const QPoint &pos);

protected:
    void keyPressEvent(QKeyEvent *event);
    void focusOutEvent(QFocusEvent *);

private:
    ObjectItemModel *_model;
};

#endif // DRAGGABLETREEVIEW_H
