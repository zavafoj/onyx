#include "consolewidget.h"
#include <QDebug>

//!
//! \brief ConsoleWidget::ConsoleWidget
//! \param parent
//!
ConsoleWidget::ConsoleWidget(QWidget *parent)
    : QWidget(parent)
{
    this->initialize();

    this->connect(this->_input,     SIGNAL(consoleQuery(QString)),
                  this,             SLOT(consoleQuery(QString)));
}

//!
//! \brief ConsoleWidget::~ConsoleWidget
//!
ConsoleWidget::~ConsoleWidget()
{

}

//!
//! \brief ConsoleWidget::initialize
//!
void ConsoleWidget::initialize()
{
    this->_layout = new QGridLayout (this);
    this->_layout->setContentsMargins(5, 5, 5, 5);

    this->_console = new QTextEdit (this);
    this->_console->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    this->_console->setReadOnly(true);
    this->_layout->addWidget(this->_console,    0,  0,  1,  1);

    this->_input = new ConsoleInput (this);
    this->_input->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    this->_layout->addWidget(this->_input,      1,  0,  1,  1);

    this->setWindowTitle("Console");
    const QPoint parentCenter = this->parentWidget()->geometry().center();
    this->setGeometry(parentCenter.x(),
                      parentCenter.y(),
                      600, 480);
}

//!
//! \brief ConsoleWidget::appendConsole
//! \param entry
//!
void ConsoleWidget::appendConsole(const Object::LogEntry &entry)
{
    this->_console->append(QString("[%1]:%2")
                        .arg(entry._eventTime.toString("HH:mm:ss.zzz"))
                           .arg(entry._message));
}

//!
//! \brief ConsoleWidget::appendConsole
//! \param entry
//!
void ConsoleWidget::appendConsole(const QString &entry)
{
    this->_console->append(entry);
}

//!
//! \brief ConsoleWidget::objectInserted
//! \param object
//!
void ConsoleWidget::objectInserted(Object *object)
{
    switch (object->type())
    {
    case Object::OT_Crossover:
    case Object::OT_Semaphore:
    case Object::OT_Train:
        this->connect(object,       SIGNAL(logAppended(Object::LogEntry)),
                      this,         SLOT(appendConsole(Object::LogEntry)));
        break;
    default:
        break;
    }
}

//!
//! \brief ConsoleWidget::consoleQuery
//! \param query
//!
void ConsoleWidget::consoleQuery(const QString &query)
{
    const QStringList objects = query.split(".");

    try
    {
        const int objSize = objects.size();
        if (objSize == 1)
        {
            if (objects[0] == "info")
            {
                this->appendConsole("<span style=\"color:green;\">" + tr("<br />Info (command construction): <br />"
                                                                         "group.ident(ident_args).method(method_args)<br />"
                                                                         "group - objectGroup (semaphores, trains)<br />"
                                                                         "ident - identification (byName, byId)<br />"
                                                                         "ident_args - identification arguments (name, number)<br />"
                                                                         "method - object method(get, set)<br />"
                                                                         "method_args - method arguments(name)<br />")
                                  + "</span>");
            }
            else
                throw tr("No Command specified");

            return;
        }
        else if (objSize != 3)
            throw tr("No Command specified");

        if (objects[0] == "semaphores")
        {
            QString identification = objects[1];

            // split by identification
            int pthsBegin = identification.indexOf('(');
            int pthsEnd = identification.indexOf(')');

            if (pthsBegin < 0 || pthsEnd < 0)
                throw tr("Method has no proper parenthesis");
            identification.truncate(pthsBegin);

            if (identification == "byName")
            {
                QString name = objects[1].mid(pthsBegin + 1, pthsEnd - pthsBegin - 1);
                Semaphore *semaphore = this->_rootItem->semaphoresGroup()->findChild<Semaphore*>(name);
                if (!semaphore)
                    throw tr("No such SEMAPHORE: '%1'").arg(name);

                qDebug() << semaphore;

                QString method = objects[2];

                // split by method
                pthsBegin = method.indexOf('(');
                pthsEnd = method.indexOf(')');

                if (pthsBegin < 0 || pthsEnd < 0)
                    throw tr("Method has no proper parenthesis");
                method.truncate(pthsBegin);

                if (method == "setState")
                {
                    QString arg = objects[2].mid(pthsBegin + 1, pthsEnd - pthsBegin - 1);
                    if (arg == "green")
                        semaphore->setState(Semaphore::SS_Green);
                    else if (arg == "yellow")
                        semaphore->setState(Semaphore::SS_Yellow);
                    else if (arg == "red")
                        semaphore->setState(Semaphore::SS_Red);
                    else
                        throw tr("Unspecified Semaphore State");
                }
                else if (method == "getState")
                {
                    QString state;
                    switch(semaphore->state())
                    {
                    case Semaphore::SS_Green:
                        state = "Green";
                        break;
                    case Semaphore::SS_Yellow:
                        state = "Yellow";
                        break;
                    case Semaphore::SS_Red:
                        state = "Red";
                        break;
                    default:
                        state = "Unknown";
                        break;
                    }

                    this->appendConsole(QString("<span style=\"color:blue\">Semaphore '%1' State: %2 </span>")
                                        .arg(name)
                                        .arg(state));
                }
                else
                    throw tr("Invalid method '%1'").arg(method);
            }
            else
                throw tr("Invalid identification. Expected: 'byId' or 'byName'");
        }
        else if (objects[0] == "trains")
        {
            QString identification = objects[1];

            // split by identification
            int pthsBegin = identification.indexOf('(');
            int pthsEnd = identification.indexOf(')');

            if (pthsBegin < 0 || pthsEnd < 0)
                throw tr("Method has no proper parenthesis");
            identification.truncate(pthsBegin);

            if (identification == "byName")
            {
                QString name = objects[1].mid(pthsBegin + 1, pthsEnd - pthsBegin - 1);
                Train *train = this->_rootItem->trainsGroup()->findChild<Train*>(name);
                if (!train)
                    throw tr("No such TRAIN: '%1'").arg(name);

                qDebug() << train;

                QString method = objects[2];

                // split by method
                pthsBegin = method.indexOf('(');
                pthsEnd = method.indexOf(')');

                if (pthsBegin < 0 || pthsEnd < 0)
                    throw tr("Method has no proper parenthesis");
                method.truncate(pthsBegin);

                if (method == "setDirection")
                {
                    QString arg = objects[2].mid(pthsBegin + 1, pthsEnd - pthsBegin - 1);
                    if (arg == "forward")
                        train->setDirection(Train::TD_Forward);
                    else if (arg == "backward")
                        train->setDirection(Train::TD_Backward);
                    else
                        throw tr("Unspecified Train Direction");
                }
                else if (method == "start")
                {
                    if (train->movement() == Train::TM_Stopped)
                    {
                        train->setMovement(Train::TM_Moving_ACC);
                    }
                    else
                    {
                        throw tr("Train is already moving");
                    }
                }
                else if (method == "stop")
                {
                    if (train->movement() != Train::TM_Stopped)
                    {
                        train->setStopDemand(true);
                    }
                    else
                    {
                        throw tr("Train is already stopped");
                    }
                }
                else if (method == "getSpeed")
                {
                    this->appendConsole(QString("<span style=\"color:blue\">Train '%1' velocity: %2 </span>")
                                        .arg(name)
                                        .arg(QString::number(train->velocity())));
                }
                else if (method == "getDirection")
                {
                    QString state;
                    switch(train->direction())
                    {
                    case Train::TD_Forward:
                        state = "Forward";
                        break;
                    case Train::TD_Backward:
                        state = "Backward";
                        break;
                    default:
                        state = "Unknown";
                        break;
                    }

                    this->appendConsole(QString("<span style=\"color:blue\">Train '%1' Direction: %2 </span>")
                                        .arg(name)
                                        .arg(state));
                }
                else if (method == "getMovement")
                {
                    QString state;
                    switch(train->movement())
                    {
                    case Train::TM_Moving_ACC:
                        state = "Accelerating";
                        break;
                    case Train::TM_Moving_AVG:
                        state = "Moving with constant velocity";
                        break;
                    case Train::TM_Moving_DEC:
                        state = "Deccelerating";
                        break;
                    case Train::TM_Stopped:
                        state = "Stopped";
                        break;
                    default:
                        state = "Unknown";
                        break;
                    }

                    this->appendConsole(QString("<span style=\"color:blue\">Train '%1' is: %2 </span>")
                                        .arg(name)
                                        .arg(state));
                }
                else
                    throw tr("Invalid method '%1'").arg(method);
            }
            else
                throw tr("Invalid identification. Expected: 'byId' or 'byName'");
        }
        else if (objects[0] == "crossovers")
        {
            QString identification = objects[1];

            // split by identification
            int pthsBegin = identification.indexOf('(');
            int pthsEnd = identification.indexOf(')');

            if (pthsBegin < 0 || pthsEnd < 0)
                throw tr("Method has no proper parenthesis");
            identification.truncate(pthsBegin);

            if (identification == "byName")
            {
                QString name = objects[1].mid(pthsBegin + 1, pthsEnd - pthsBegin - 1);
                Crossover *crossover = this->_rootItem->crossoversGroup()->findChild<Crossover*>(name);
                if (!crossover)
                    throw tr("No such CROSSOVER: '%1'").arg(name);

                qDebug() << crossover;

                QString method = objects[2];

                // split by method
                pthsBegin = method.indexOf('(');
                pthsEnd = method.indexOf(')');

                if (pthsBegin < 0 || pthsEnd < 0)
                    throw tr("Method has no proper parenthesis");
                method.truncate(pthsBegin);

                if (method == "setState")
                {
                    QString arg = objects[2].mid(pthsBegin + 1, pthsEnd - pthsBegin - 1);
                    if (arg == "main")
                        crossover->setState(Crossover::CS_Main);
                    else if (arg == "branch")
                        crossover->setState(Crossover::CS_Branch);
                    else
                        throw tr("Unspecified Crossover State");
                }
                else if (method == "getState")
                {
                    QString state;
                    switch(crossover->state())
                    {
                    case Crossover::CS_Main:
                        state = "Main";
                        break;
                    case Crossover::CS_Branch:
                        state = "Branch";
                        break;
                    default:
                        state = "Unknown";
                        break;
                    }

                    this->appendConsole(QString("<span style=\"color:blue\">Crossover '%1' State: %2 </span>")
                                        .arg(name)
                                        .arg(state));
                }
                else
                    throw tr("Invalid method '%1'").arg(method);
            }
            else
                throw tr("Invalid identification. Expected: 'byId' or 'byName'");
        }
    }
    catch(const QString &exception)
    {
        this->appendConsole(QString("<span style=\"color:red;\">Console Exception: "
                                    + exception
                                    + "</span>"));
    }
}

//!
//! \brief ConsoleWidget::rootItem
//! \return
//!
RootItem *ConsoleWidget::rootItem() const
{
    return this->_rootItem;
}

//!
//! \brief ConsoleWidget::setRootItem
//! \param rootItem
//!
void ConsoleWidget::setRootItem(RootItem *rootItem)
{
    this->_rootItem = rootItem;

    this->connect(this->_rootItem,          SIGNAL(elementLoaded(Object*)),
                  this,                     SLOT(objectInserted(Object*)));
}
