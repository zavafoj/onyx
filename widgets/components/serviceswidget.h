#ifndef SERVICESWIDGET_H
#define SERVICESWIDGET_H

#include <QWidget>
#include <QTextEdit>
#include <QHBoxLayout>

#include "../../core/structure/elements/elements.h"

class ServicesWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ServicesWidget(QWidget *parent = 0);
    virtual ~ServicesWidget();

signals:

public slots:

private:
    QHBoxLayout *_layout;

};

#endif // SERVICESWIDGET_H
