#ifndef BUTTONBOX_H
#define BUTTONBOX_H

#include <QToolBox>
#include <QEvent>

#include "editorbutton.h"
#include <QGridLayout>
#include <QContextMenuEvent>

//!
//! \brief The ButtonBox class
//!
class ButtonBox : public QToolBox
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit ButtonBox(QWidget *parent = 0);
    virtual ~ButtonBox();
    Object::ObjectType currentType() const;

    bool locked() const;
    void setLocked(bool locked);

public slots:
    void clear();

signals:
    //!
    //! \brief currentTypeChanged
    //! \param newType
    //!
    void currentTypeChanged(Object::ObjectType newType);

protected:
    void addButtonsToLayout(const QList<Object::ObjectType> &buttonTypes,
                            QGridLayout *layout);
    void retranslate();
    void changeEvent(QEvent *event);
    void contextMenuEvent(QContextMenuEvent *event);

protected slots:
    void elementToggled(bool toggled);
    void setCurrentType(Object::ObjectType newType);
    void placeTrainClicked();

private:
    QWidget *_railroadObjects;
    QWidget *_dynamicObjects;

    QGridLayout *_railroadObjectsLayout;
    QGridLayout *_dynamicObjectsLayout;

    Object::ObjectType _currentType;

    bool _locked;
};

#endif // BUTTONBOX_H
