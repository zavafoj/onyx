#include "editorbutton.h"

//!
//! \brief EditorButton::EditorButton
//! \param type
//! \param parent
//!
EditorButton::EditorButton(Object::ObjectType type, QWidget *parent) :
    QWidget(parent),
    _type(type)
{
    this->_button = new QPushButton(this);
    this->_button->setCheckable(true);
    this->_button->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    this->_button->setMinimumSize(65, 65);
    this->_button->setMaximumSize(65, 65);

    this->connect(this->_button,            SIGNAL(clicked(bool)),
                  this,                     SIGNAL(itemChecked(bool)));

    this->connect(this->_button,            SIGNAL(clicked()),
                  this,                     SIGNAL(clicked()));

    this->_titleLabel = new QLabel(this);
    this->_titleLabel->setSizePolicy(QSizePolicy::Minimum,
                                     QSizePolicy::Fixed);
    this->_titleLabel->setAlignment(Qt::AlignHCenter);

    this->_layout = new QVBoxLayout(this);

    this->_layout->addWidget(this->_button);
    this->_layout->addWidget(this->_titleLabel);

    QString elementName = QString(":/elements/images/Elements/%1.png")
                                    .arg(Object::getObjectTypeString(this->_type));
    this->_button->setIcon(QIcon(elementName));

    this->setLayout(this->_layout);
    this->retranslate();
}

//!
//! \brief EditorButton::~EditorButton
//!
EditorButton::~EditorButton()
{

}

//!
//! \brief EditorButton::type
//! \return
//!
Object::ObjectType EditorButton::type() const
{
    return this->_type;
}

//!
//! \brief EditorButton::isToggled
//! \return
//!
bool EditorButton::isToggled() const
{
    return this->_button->isChecked();
}

//!
//! \brief EditorButton::setToggled
//! \param toggled
//!
void EditorButton::setToggled(bool toggled)
{
    this->_button->setChecked(toggled);
}

//!
//! \brief EditorButton::setCheckable
//! \param checkable
//!
void EditorButton::setCheckable(bool checkable)
{
    this->_button->setCheckable(checkable);
}

//!
//! \brief EditorButton::retranslate
//!
void EditorButton::retranslate()
{
    QString elementName = Object::getObjectTypeTrString(this->_type);
    this->_titleLabel->setText(elementName);

    this->_button->setToolTip(tr("Select %1 as current element to place")
                              .arg(elementName));
}

//!
//! \brief EditorButton::changeEvent
//! \param event
//!
void EditorButton::changeEvent(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::LanguageChange :
    {
        this->retranslate();
    }
        break;

    default:
    {

    }
    }

    QWidget::changeEvent(event);
}

