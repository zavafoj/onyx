#include <QDebug>
#include "consoleinput.h"

#define CONSOLE_WIDGET_MAX_COMMANDS 100

//!
//! \brief ConsoleInput::ConsoleInput
//! \param parent
//!
ConsoleInput::ConsoleInput(QWidget *parent) :
    QLineEdit(parent),

    _commandIndex(0)
{
    this->installEventFilter(this);
}

//!
//! \brief ConsoleInput::~ConsoleInput
//!
ConsoleInput::~ConsoleInput(void)
{

}

//!
//! \brief ConsoleInput::eventFilter
//! \param object
//! \param event
//! \return
//!
bool ConsoleInput::eventFilter(QObject *object, QEvent *event)
{
    if (object == this)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
            Q_ASSERT (keyEvent);

            if (keyEvent->key() == Qt::Key_Return)
            {
                this->_commandIndex = 0;
                const QString inputText = this->text();

                if (inputText.isEmpty())
                    return false;

                emit this->consoleQuery(inputText);

                if (this->_commands.size() >= CONSOLE_WIDGET_MAX_COMMANDS)
                {
                    this->_commands.pop_back();
                }

                this->_commands.push_front(inputText);

                this->clear();
            }

            else if (keyEvent->key() == Qt::Key_Up)
            {
                this->setText(this->_commands.isEmpty()
                              ? ""
                              : this->_commands.at((this->_commandIndex < this->_commands.size()-1)
                                                   ? (this->_commandIndex++)
                                                   : this->_commands.size()-1));
            }

            else if (keyEvent->key() == Qt::Key_Down)
            {
                this->setText(this->_commands.isEmpty()
                              ? ""
                              : this->_commands.at((this->_commandIndex > 0)
                                                   ? (this->_commandIndex--)
                                                   : 0));
            }

            else
            {
                QLineEdit::keyPressEvent(keyEvent);
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return QLineEdit::eventFilter(object, event);
    }
}
