#include <QMessageBox>

#include "mapeditor.h"
#include "ui_mapeditor.h"

//!
//! \brief MapEditor::MapEditor
//! \param parent
//!
MapEditor::MapEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MapEditor),
    _type(Object::OT_Unknown),
    _valid(false)
{
    ui->setupUi(this);

    this->ui->graphicsView->setMouseTracking(true);
    this->ui->graphicsView->setAcceptDrops(true);

    this->connect(this->ui->buttonBox,          SIGNAL(currentTypeChanged(Object::ObjectType)),
                  this,                         SLOT(elementChanged(Object::ObjectType)));

    this->connect(this->ui->buttonBox,          SIGNAL(currentTypeChanged(Object::ObjectType)),
                  this->ui->graphicsView,       SLOT(setCurrentType(Object::ObjectType)));

    GraphicsScene *scene = this->ui->graphicsView->getScene();
    this->connect(this->ui->objectsList,        SIGNAL(itemSelected(Object*)),
                  scene,                        SLOT(hoverObject(Object*)));

    this->connect(this->ui->graphicsView,       SIGNAL(objectSelected(Object*)),
                  this->ui->objectProperties,   SLOT(setCurrentObject(Object*)));

    this->connect(this->ui->graphicsView,       SIGNAL(elementPlaced()),
                  this->ui->buttonBox,          SLOT(clear()));

    this->connect(this->ui->lockButton,         SIGNAL(clicked()),
                  this,                         SLOT(lockButtonClicked()));

    this->connect(this->ui->unlockButton,       SIGNAL(clicked()),
                  this,                         SLOT(unlockButtonClicked()));

    this->_rootItem = new RootItem();
    this->_model = new ObjectItemModel(this->_rootItem, this->ui->objectsList);

    this->connect(this->_rootItem,          SIGNAL(childRemoved()),
                  this->ui->objectsList,    SLOT(reset()));

    this->ui->objectsList->setModel(this->_model);
    this->ui->graphicsView->setRootItem(this->_rootItem);

    this->retranslate();
}

//!
//! \brief MapEditor::~MapEditor
//!
MapEditor::~MapEditor()
{
    delete ui;
    this->_rootItem->deleteLater();
}

//!
//! \brief MapEditor::type
//! \return
//!
Object::ObjectType MapEditor::type() const
{
    return this->_type;
}

//!
//! \brief MapEditor::buttonBox
//! \return
//!
ButtonBox *MapEditor::buttonBox() const
{
    return this->ui->buttonBox;
}

//!
//! \brief MapEditor::rootItem
//! \return
//!
RootItem *MapEditor::rootItem() const
{
    return this->_rootItem;
}

//!
//! \brief MapEditor::show
//!
void MapEditor::show()
{
    QWidget::show();
    this->ui->graphicsView->reset();
}

//!
//! \brief MapEditor::clear
//!
void MapEditor::clear()
{
    this->ui->graphicsView->scene()->clear();
    this->unlockButtonClicked();
}

//!
//! \brief MapEditor::elementChanged
//! \param newType
//!
void MapEditor::elementChanged(Object::ObjectType newType)
{
    if (this->_type != newType)
    {
        this->_type = newType;
        this->setCurrentElement(Object::getObjectTypeTrString(this->_type));
    }
}

//!
//! \brief MapEditor::retranslate
//!
void MapEditor::retranslate()
{
    this->setCurrentElement(Object::getObjectTypeTrString(this->_type));
}

//!
//! \brief MapEditor::changeEvent
//! \param event
//!
void MapEditor::changeEvent(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::LanguageChange :
    {
        this->ui->retranslateUi(this);
        this->retranslate();
    }
        break;

    default:
    {

    }
    }

    QWidget::changeEvent(event);
}

//!
//! \brief MapEditor::closeEvent
//! \param event
//!
void MapEditor::closeEvent(QCloseEvent *event)
{
    if (this->_valid)
    {
        QMessageBox b;
        b.setWindowTitle(tr("Question"));
        b.setIcon(QMessageBox::Question);
        b.setText(tr("Do you want to copy editor data to simulator?"));
        b.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        int ret = b.exec();

        switch ((QMessageBox::StandardButton) ret)
        {
        case QMessageBox::Yes:
            emit this->copyDataRequested();
            break;
        case QMessageBox::No:
            break;
        default:
            event->ignore();
            break;
        }
    }
    else
        QWidget::closeEvent(event);

    emit this->hidden();
}

//!
//! \brief MapEditor::setCurrentElement
//! \param elementString
//!
void MapEditor::setCurrentElement(const QString &elementString)
{
    this->ui->currentLabel->setText(tr("Current Element: %1")
                                    .arg(elementString));
}

//!
//! \brief MapEditor::unlockButtonClicked
//!
void MapEditor::unlockButtonClicked()
{
    this->ui->lockButton->setEnabled(true);
    this->ui->unlockButton->setEnabled(false);

    this->ui->graphicsView->getScene()->uncheckConnections();
    this->ui->buttonBox->setLocked(false);
    this->ui->elementsBox->setVisible(true);

    this->_valid = false;
}

//!
//! \brief MapEditor::lockButtonClicked
//!
void MapEditor::lockButtonClicked()
{
    this->ui->lockButton->setEnabled(false);
    this->ui->unlockButton->setEnabled(true);

    this->setCursor(QCursor(Qt::WaitCursor));
    try
    {
        this->ui->graphicsView->getScene()->checkConnections();
    }
    catch (QString errstr)
    {
        this->setCursor(QCursor(Qt::ArrowCursor));
        this->unlockButtonClicked();

        QMessageBox b;
        b.setWindowTitle(tr("Error"));
        b.setIcon(QMessageBox::Critical);
        b.setText(errstr);
        b.addButton(tr("Ok"),QMessageBox::AcceptRole);
        b.exec();
        return;
    }

    this->setCursor(QCursor(Qt::ArrowCursor));
    this->ui->buttonBox->setLocked(true);
    this->ui->elementsBox->setVisible(false);

    this->_valid = true;
}

//!
//! \brief MapEditor::valid
//! \return
//!
bool MapEditor::valid() const
{
    return this->_valid;
}
