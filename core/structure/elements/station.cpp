#include "station.h"

//!
//! \brief Station::Station
//! \param parent
//!
Station::Station(Object *parent) :
    Rail(parent)
{
    this->_objType = OT_Station;
    this->_firstNodePosition  = QPoint(-60, 0);
    this->_secondNodePosition = QPoint( 60, 0);
}

//!
//! \brief Station::~Station
//!
Station::~Station()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief Semaphore::read
//! \param stream
//!
void Station::read(QDataStream &stream)
{
    // Read Position
    QPointF position;
    stream >> position;
    this->setPosition(position);

    // Read Object Name
    QString name;
    stream >> name;
    this->setObjectName(name);

    // Read Station Name
    QString stationName;
    stream >> stationName;
    this->setStationName(stationName);

    // Read First Node Position
    QPoint firstPosition;
    stream >> firstPosition;
    this->setFirstNodePosition(firstPosition);

    // Read Second Node Position
    QPoint secondPosition;
    stream >> secondPosition;
    this->setSecondNodePosition(secondPosition);
}

//!
//! \brief Station::write
//! \param stream
//!
void Station::write(QDataStream &stream) const
{
    stream << this->_objType
           << this->_position
           << this->objectName()
           << this->_stationName
           << this->_firstNodePosition
           << this->_secondNodePosition;
}

//!
//! \brief Station::readXML
//! \param fragment
//!
void Station::readXML(QDomElement &fragment)
{
    this->_position.setX(fragment.attribute("PositionX").toDouble());
    this->_position.setY(fragment.attribute("PositionY").toDouble());

    this->setObjectName(fragment.attribute("Name"));

    this->setStationName(fragment.attribute("StationName"));

    this->_firstNodePosition.setX(fragment.attribute("FirstNodeX").toInt());
    this->_firstNodePosition.setY(fragment.attribute("FirstNodeY").toInt());

    this->_secondNodePosition.setX(fragment.attribute("SecondNodeX").toInt());
    this->_secondNodePosition.setY(fragment.attribute("SecondNodeY").toInt());
}

//!
//! \brief Station::writeXML
//! \param stream
//!
void Station::writeXML(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Station");

    stream.writeAttribute("PositionX", QString::number(this->_position.x()));
    stream.writeAttribute("PositionY", QString::number(this->_position.y()));

    stream.writeAttribute("Name", this->objectName());

    stream.writeAttribute("StationName", this->_stationName);

    stream.writeAttribute("FirstNodeX", QString::number(this->_firstNodePosition.x()));
    stream.writeAttribute("FirstNodeY", QString::number(this->_firstNodePosition.y()));

    stream.writeAttribute("SecondNodeX", QString::number(this->_secondNodePosition.x()));
    stream.writeAttribute("SecondNodeY", QString::number(this->_secondNodePosition.y()));

    stream.writeEndElement();
}

//!
//! \brief Station::keys
//! \return
//!
QStringList Station::keys() const
{
    QStringList keys;
    keys << "Name"
         << "StationName"
         << "Position"
         << "FirstNodePosition"
         << "SecondNodePosition";
    return keys;
}

//!
//! \brief Station::stationName
//! \return
//!
QString Station::stationName() const
{
    return this->_stationName;
}

//!
//! \brief Station::setStationName
//! \param stationName
//!
void Station::setStationName(const QString &stationName)
{
    if (this->_stationName != stationName)
    {
        this->_stationName = stationName;
        emit this->stationNameChanged(stationName);
        emit this->modelUpdated();
    }
}

