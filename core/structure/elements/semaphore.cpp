#include "semaphore.h"

//!
//! \brief Semaphore::Semaphore
//! \param parent
//!
Semaphore::Semaphore(Object *parent) :
    Rail(parent),
    _state(SS_Green),
    _vMax(50)
{
    this->_objType = OT_Semaphore;
}

//!
//! \brief Semaphore::~Semaphore
//!
Semaphore::~Semaphore()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief Semaphore::read
//! \param stream
//!
void Semaphore::read(QDataStream &stream)
{
    // Read Position
    QPointF position;
    stream >> position;
    this->setPosition(position);

    // Read Object Name
    QString name;
    stream >> name;
    this->setObjectName(name);

    // Read First Node Position
    QPoint firstPosition;
    stream >> firstPosition;
    this->setFirstNodePosition(firstPosition);

    // Read Second Node Position
    QPoint secondPosition;
    stream >> secondPosition;
    this->setSecondNodePosition(secondPosition);

    // Read Semaphore State
    int state;
    stream >> state;
    this->setState((SemaphoreStates) state);

    // Read vMax
    int vMax;
    stream >> vMax;
    this->setVMax(vMax);
}

//!
//! \brief Semaphore::write
//! \param stream
//!
void Semaphore::write(QDataStream &stream) const
{
    stream << this->_objType
           << this->_position
           << this->objectName()
           << this->_firstNodePosition
           << this->_secondNodePosition
           << this->_state
           << this->_vMax;
}

//!
//! \brief Semaphore::readXML
//! \param fragment
//!
void Semaphore::readXML(QDomElement &fragment)
{
    this->_position.setX(fragment.attribute("PositionX").toDouble());
    this->_position.setY(fragment.attribute("PositionY").toDouble());

    this->setObjectName(fragment.attribute("Name"));

    this->_firstNodePosition.setX(fragment.attribute("FirstNodeX").toInt());
    this->_firstNodePosition.setY(fragment.attribute("FirstNodeY").toInt());

    this->_secondNodePosition.setX(fragment.attribute("SecondNodeX").toInt());
    this->_secondNodePosition.setY(fragment.attribute("SecondNodeY").toInt());

    this->setState((SemaphoreStates) fragment.attribute("State").toInt());
    this->setVMax(fragment.attribute("vMax").toInt());
}

//!
//! \brief Semaphore::writeXML
//! \param stream
//!
void Semaphore::writeXML(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Semaphore");

    stream.writeAttribute("PositionX", QString::number(this->_position.x()));
    stream.writeAttribute("PositionY", QString::number(this->_position.y()));

    stream.writeAttribute("Name", this->objectName());

    stream.writeAttribute("FirstNodeX", QString::number(this->_firstNodePosition.x()));
    stream.writeAttribute("FirstNodeY", QString::number(this->_firstNodePosition.y()));

    stream.writeAttribute("SecondNodeX", QString::number(this->_secondNodePosition.x()));
    stream.writeAttribute("SecondNodeY", QString::number(this->_secondNodePosition.y()));

    stream.writeAttribute("State",  QString::number(this->_state));
    stream.writeAttribute("vMax", QString::number(this->_vMax));

    stream.writeEndElement();
}

//!
//! \brief Semaphore::keys
//! \return
//!
QStringList Semaphore::keys() const
{
    QStringList keys;
    keys << "Name"
         << "Position"
         << "FirstNodePosition"
         << "SecondNodePosition"
         << "State"
         << "vMax";

    return keys;
}

//!
//! \brief Semaphore::state
//! \return
//!
Semaphore::SemaphoreStates Semaphore::state() const
{
    return this->_state;
}

//!
//! \brief Semaphore::vMax
//! \return
//!
int Semaphore::vMax() const
{
    return this->_vMax;
}

//!
//! \brief Semaphore::setState
//! \param state
//!
void Semaphore::setState(const SemaphoreStates &state)
{
    if (this->_state != state)
    {
        this->_state = state;
        emit this->stateChanged(state);
        emit this->modelUpdated();

        QString stateStr;
        switch (state)
        {
        case SS_Green:
            stateStr = tr("Green");
            break;
        case SS_Yellow:
            stateStr = tr("Yellow");
            break;
        case SS_Red:
            stateStr = tr("Red");
            break;
        default:
            stateStr = tr("Unknown");
        }

        this->appendLog(tr("Semaphore %1 state changed to: %2")
                        .arg(this->objectName())
                        .arg(stateStr));
    }
}

//!
//! \brief Semaphore::setVMax
//! \param vMax
//!
void Semaphore::setVMax(int vMax)
{
    if (this->_vMax != vMax)
    {
        this->_vMax = vMax;
        emit this->vMaxChanged(vMax);
    }
}

