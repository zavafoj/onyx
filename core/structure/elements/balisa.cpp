#include "balisa.h"

//!
//! \brief Balisa::Balisa
//! \param parent
//!
Balisa::Balisa(Object *parent) :
    Object(parent)
{
    this->_objType = OT_Balisa;
}

//!
//! \brief Balisa::~Balisa
//!
Balisa::~Balisa()
{

}

//!
//! \brief Balisa::read
//! \param stream
//!
void Balisa::read(QDataStream &stream)
{
    // Read Position
    QPointF position;
    stream >> position;
    this->setPosition(position);

    // Read Object Name
    QString name;
    stream >> name;
    this->setObjectName(name);
}

//!
//! \brief Balisa::write
//! \param stream
//!
void Balisa::write(QDataStream &stream) const
{
    stream << this->_objType
           << this->_position
           << this->objectName();
}

//!
//! \brief Balisa::readXML
//! \param fragment
//!
void Balisa::readXML(QDomElement &fragment)
{
    this->_position.setX(fragment.attribute("PositionX").toDouble());
    this->_position.setY(fragment.attribute("PositionY").toDouble());

    this->setObjectName(fragment.attribute("Name"));
}

//!
//! \brief Balisa::writeXML
//! \param stream
//!
void Balisa::writeXML(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Balisa");

    stream.writeAttribute("PositionX", QString::number(this->_position.x()));
    stream.writeAttribute("PositionY", QString::number(this->_position.y()));

    stream.writeAttribute("Name", this->objectName());
    stream.writeEndElement();
}

//!
//! \brief Balisa::keys
//! \return
//!
QStringList Balisa::keys() const
{
    QStringList keys;
    keys << "Name"
         << "Position";
    return keys;
}
