#ifndef CROSSOVER_H
#define CROSSOVER_H

#include "object.h"

//!
//! \brief The Crossover class
//!
class Crossover : public Object
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

    Q_ENUMS (CrossoverStates)
    Q_PROPERTY(QPoint FirstNodePosition READ firstNodePosition WRITE setFirstNodePosition NOTIFY firstNodePositionChanged)
    Q_PROPERTY(QPoint SecondNodePosition READ secondNodePosition WRITE setSecondNodePosition NOTIFY secondNodePositionChanged)
    Q_PROPERTY(QPoint BranchNodePosition READ branchNodePosition WRITE setBranchNodePosition NOTIFY branchNodePositionChanged)
    Q_PROPERTY(CrossoverStates State READ state WRITE setState NOTIFY stateChanged)

public:
    enum CrossoverStates{
        CS_Main     =       0x01,
        CS_Branch   =       0x02,
        CS_Unknown  =         -1
    };

    explicit Crossover(Object *parent = 0);
    virtual ~Crossover();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    virtual QStringList keys() const;

    QPoint firstNodePosition() const;
    QPoint secondNodePosition() const;
    QPoint branchNodePosition() const;

    Crossover::CrossoverStates state() const;

signals:
    //!
    //! \brief firstNodePositionChanged
    //! \param position
    //!
    void firstNodePositionChanged(const QPoint &position);
    //!
    //! \brief secondNodePositionChanged
    //! \param position
    //!
    void secondNodePositionChanged(const QPoint &position);
    //!
    //! \brief branchNodePositionChanged
    //! \param position
    //!
    void branchNodePositionChanged(const QPoint &position);
    //!
    //! \brief stateChanged
    //! \param state
    //!
    void stateChanged(Crossover::CrossoverStates state);

public slots:
    void setFirstNodePosition(const QPoint &firstNodePosition);
    void setSecondNodePosition(const QPoint &secondNodePosition);
    void setBranchNodePosition(const QPoint &branchNodePosition);
    void setState(const Crossover::CrossoverStates &state);

private:
    CrossoverStates _state;
    QPoint _firstNodePosition;
    QPoint _secondNodePosition;
    QPoint _branchNodePosition;
};

#endif // CROSSOVER_H
