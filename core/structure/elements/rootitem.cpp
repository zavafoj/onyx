#include "rootitem.h"

//!
//! \brief RootItem::RootItem
//! \param parent
//!
RootItem::RootItem(Object *parent):
    Object(parent)
{
    this->_objType = OT_Root;
    this->setObjectName("Root");

    this->_railsGroup = new ObjectGroup(this);
    this->_crossoversGroup = new ObjectGroup(this);
    this->_semaphoresGroup = new ObjectGroup(this);
    this->_balisesGroup = new ObjectGroup(this);
    this->_terminatorsGroup = new ObjectGroup(this);
    this->_stationsGroup = new ObjectGroup(this);
    this->_trainsGroup = new ObjectGroup(this);

    this->retranslate();
}

//!
//! \brief RootItem::~RootItem
//!
RootItem::~RootItem()
{

}

//!
//! \brief RootItem::read
//! \param stream
//!
void RootItem::read(QDataStream &stream)
{
    QTime time;
    time.start();
    this->_railsGroup->read(stream);
    this->_crossoversGroup->read(stream);
    this->_semaphoresGroup->read(stream);
    this->_balisesGroup->read(stream);
    this->_terminatorsGroup->read(stream);
    this->_stationsGroup->read(stream);
    this->_trainsGroup->read(stream);
    qDebug() << "StreamReading" << time.elapsed();
}

//!
//! \brief RootItem::write
//! \param stream
//!
void RootItem::write(QDataStream &stream) const
{
    QTime time;
    time.start();
    this->_railsGroup->write(stream);
    this->_crossoversGroup->write(stream);
    this->_semaphoresGroup->write(stream);
    this->_balisesGroup->write(stream);
    this->_terminatorsGroup->write(stream);
    this->_stationsGroup->write(stream);
    this->_trainsGroup->write(stream);
    qDebug() << "StreamWriting" << time.elapsed();
}

//!
//! \brief RootItem::readXML
//! \param fragment
//!
void RootItem::readXML(QDomElement &fragment)
{
    QTime time;
    time.start();
    QList <ObjectGroup*> groupList;
    groupList << this->_railsGroup
              << this->_crossoversGroup
              << this->_semaphoresGroup
              << this->_balisesGroup
              << this->_terminatorsGroup
              << this->_stationsGroup
              << this->_trainsGroup;

    int children = fragment.childNodes().count();
    for (int i = 0; i < children; i++)
    {
        QDomElement element = fragment.childNodes().at(i).toElement();
        if (element.nodeName() == "Group")
        {
            ObjectGroup *child = groupList.at(i);
            if (element.attribute("Name") == child->objectName())
                child->readXML(element);
        }
        else
        {
            throw "Unable to read";
        }
    }
    qDebug() << "XMLReading" << time.elapsed();
}

//!
//! \brief RootItem::writeXML
//! \param stream
//!
void RootItem::writeXML(QXmlStreamWriter &stream) const
{
    QTime time;
    time.start();
    stream.writeStartElement("Root");

    this->_railsGroup->writeXML(stream);
    this->_crossoversGroup->writeXML(stream);
    this->_semaphoresGroup->writeXML(stream);
    this->_balisesGroup->writeXML(stream);
    this->_terminatorsGroup->writeXML(stream);
    this->_stationsGroup->writeXML(stream);
    this->_trainsGroup->writeXML(stream);

    stream.writeEndElement();
    qDebug() << "XMLWriting" << time.elapsed();
}

//!
//! \brief RootItem::railsGroup
//! \return
//!
ObjectGroup *RootItem::railsGroup() const
{
    return this->_railsGroup;
}

//!
//! \brief RootItem::crossoversGroup
//! \return
//!
ObjectGroup *RootItem::crossoversGroup() const
{
    return this->_crossoversGroup;
}

//!
//! \brief RootItem::semaphoresGroup
//! \return
//!
ObjectGroup *RootItem::semaphoresGroup() const
{
    return this->_semaphoresGroup;
}

//!
//! \brief RootItem::balisesGroup
//! \return
//!
ObjectGroup *RootItem::balisesGroup() const
{
    return this->_balisesGroup;
}

//!
//! \brief RootItem::terminatorsGroup
//! \return
//!
ObjectGroup *RootItem::terminatorsGroup() const
{
    return this->_terminatorsGroup;
}

//!
//! \brief RootItem::stationsGroup
//! \return
//!
ObjectGroup *RootItem::stationsGroup() const
{
    return this->_stationsGroup;
}

//!
//! \brief RootItem::trainsGroup
//! \return
//!
ObjectGroup *RootItem::trainsGroup() const
{
    return this->_trainsGroup;
}

//!
//! \brief RootItem::keys
//! \return
//!
QStringList RootItem::keys() const
{
    return QStringList();
}

//!
//! \brief RootItem::retranslate
//!
void RootItem::retranslate()
{
    this->_railsGroup->setObjectName(tr("Rails"));
    this->_crossoversGroup->setObjectName(tr("Crossovers"));
    this->_semaphoresGroup->setObjectName(tr("Semaphores"));
    this->_balisesGroup->setObjectName(tr("Balises"));
    this->_terminatorsGroup->setObjectName(tr("Terminators"));
    this->_stationsGroup->setObjectName(tr("Stations"));
    this->_trainsGroup->setObjectName(tr("Trains"));

    emit this->updateView();
}

//!
//! \brief RootItem::addElement
//! \param type
//! \param pos
//! \return
//!
Object *RootItem::createElement(Object::ObjectType type,
                                const QPointF &pos)
{
    Object *newObject;
    switch (type)
    {
    case Object::OT_Rail:
        newObject = new Rail(this->_railsGroup);
        break;
    case Object::OT_Crossover:
        newObject = new Crossover(this->_crossoversGroup);
        break;
    case Object::OT_Semaphore:
        newObject = new Semaphore(this->_semaphoresGroup);
        break;
    case Object::OT_Balisa:
        newObject = new Balisa(this->_balisesGroup);
        break;
    case Object::OT_Terminator:
        newObject = new Terminator(this->_terminatorsGroup);
        break;
    case Object::OT_Station:
        newObject = new Station(this->_stationsGroup);
        break;
    case Object::OT_Train:
        newObject = new Train(this->_trainsGroup);
        break;
    default:
        return nullptr;
    }

    newObject->setPosition(pos);
    return newObject;
}

//!
//! \brief RootItem::clear
//!
void RootItem::clear()
{
    this->_railsGroup->removeChildren();
    this->_crossoversGroup->removeChildren();
    this->_semaphoresGroup->removeChildren();
    this->_balisesGroup->removeChildren();
    this->_terminatorsGroup->removeChildren();
    this->_stationsGroup->removeChildren();
    this->_trainsGroup->removeChildren();
}

//!
//! \brief RootItem::loadElement
//! \param object
//!
void RootItem::loadElement(Object *object)
{
    emit this->elementLoaded(object);
}
