#include "train.h"

//!
//! \brief Train::Train
//! \param parent
//!
Train::Train(Object *parent) :
    Object(parent),
    _priority(0),
    _acceleration(0.3),
    _decceleration(0.15),
    _maxVelocity(50),
    _mass(42.0),
    _velocity(0),
    _stopDemand(false),
    _distanceAlert(false),

    _willBeAdded(false),
    _addDemand(false),

    _direction(TD_Forward),
    _movement(TM_Stopped)
{
    this->_objType = OT_Train;
}

//!
//! \brief Train::~Train
//!
Train::~Train()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief Train::read
//! \param stream
//!
void Train::read(QDataStream &stream)
{
    // Read Position
    QPointF position;
    stream >> position;
    this->setPosition(position);

    // Read Object Name
    QString name;
    stream >> name;
    this->setObjectName(name);

    // Read Acceleration
    qreal acceleration;
    stream >> acceleration;
    this->setAcceleration(acceleration);

    // Read Decceleration
    qreal decceleration;
    stream >> decceleration;
    this->setDecceleration(decceleration);

    // Read Max Velocity
    int vmax;
    stream >> vmax;
    this->setMaxVelocity(vmax);

    // Read Mass
    qreal mass;
    stream >> mass;
    this->setMass(mass);

    // Read Priority
    int priority;
    stream >> priority;
    this->setPriority(priority);
}

//!
//! \brief Train::write
//! \param stream
//!
void Train::write(QDataStream &stream) const
{
    stream << this->_objType
           << this->_position
           << this->objectName()
           << this->_acceleration
           << this->_decceleration
           << this->_maxVelocity
           << this->_mass
           << this->_priority;
}

//!
//! \brief Train::readXML
//! \param fragment
//!
void Train::readXML(QDomElement &fragment)
{
    this->_position.setX(fragment.attribute("PositionX").toDouble());
    this->_position.setY(fragment.attribute("PositionY").toDouble());

    this->setObjectName(fragment.attribute("Name"));

    this->setAcceleration(fragment.attribute("Acceleration").toDouble());
    this->setDecceleration(fragment.attribute("Decceleration").toDouble());

    this->setMaxVelocity(fragment.attribute("vMax").toInt());

    this->setMass(fragment.attribute("Mass").toDouble());
    this->setMass(fragment.attribute("Priority").toInt());
}

//!
//! \brief Train::writeXML
//! \param stream
//!
void Train::writeXML(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Train");

    stream.writeAttribute("PositionX", QString::number(this->_position.x()));
    stream.writeAttribute("PositionY", QString::number(this->_position.y()));

    stream.writeAttribute("Name", this->objectName());

    stream.writeAttribute("Acceleration", QString::number(this->_acceleration));
    stream.writeAttribute("Decceleration", QString::number(this->_decceleration));

    stream.writeAttribute("vMax", QString::number(this->_maxVelocity));

    stream.writeAttribute("Mass", QString::number(this->_mass));
    stream.writeAttribute("Priority", QString::number(this->_priority));

    stream.writeEndElement();
}

//!
//! \brief Train::keys
//! \return
//!
QStringList Train::keys() const
{
    QStringList keys;

    keys << "Name"
         << "Position"
         << "Acceleration"
         << "Velocity"
         << "Mass";

    return keys;
}

//!
//! \brief Train::retranslate
//!
void Train::retranslate()
{

}

//!
//! \brief Train::willBeRemoved
//! \return
//!
bool Train::willBeRemoved() const
{
    return this->_willBeRemoved;
}

//!
//! \brief Train::setWillBeRemoved
//! \param willBeRemoved
//!
void Train::setWillBeRemoved(bool willBeRemoved)
{
    this->_willBeRemoved = willBeRemoved;
}

//!
//! \brief Train::removeDemand
//! \return
//!
bool Train::removeDemand() const
{
    return this->_removeDemand;
}

//!
//! \brief Train::setRemoveDemand
//! \param removeDemand
//!
void Train::setRemoveDemand(bool removeDemand)
{
    this->_removeDemand = removeDemand;
}

//!
//! \brief Train::addDemand
//! \return
//!
bool Train::addDemand() const
{
    return this->_addDemand;
}

//!
//! \brief Train::setAddDemand
//! \param addDemand
//!
void Train::setAddDemand(bool addDemand)
{
    this->_addDemand = addDemand;
    qDebug() << "Setting AddDemand to" << addDemand;
}

//!
//! \brief Train::willBeAdded
//! \return
//!
bool Train::willBeAdded() const
{
    return this->_willBeAdded;
}

//!
//! \brief Train::setWillBeAdded
//! \param willBeAdded
//!
void Train::setWillBeAdded(bool willBeAdded)
{
    this->_willBeAdded = willBeAdded;
    qDebug() << "Setting WillBeAdded to" << willBeAdded;
}


//!
//! \brief Train::distanceAlert
//! \return
//!
bool Train::distanceAlert() const
{
    return this->_distanceAlert;
}

//!
//! \brief Train::setDistanceAlert
//! \param distanceAlert
//!
void Train::setDistanceAlert(bool distanceAlert)
{
    this->_distanceAlert = distanceAlert;
}

//!
//! \brief Train::addTrain
//! \param element
//!
void Train::addTrain(Train *element)
{
    this->_addedTrains.push_back(element);

    qreal cumulativeMass = this->_mass + element->mass();
    qDebug() << "CumulativeMass" << this->_mass << cumulativeMass << this->_acceleration << this->_decceleration;
    qreal acc = this->_acceleration * this->_mass / cumulativeMass;
    this->setAcceleration(acc);
    qreal dec = this->_decceleration * this->_mass / cumulativeMass;
    this->setDecceleration(dec);

    this->setMass(cumulativeMass);

    qDebug() << "CumulativeMass" << this->_acceleration << this->_decceleration;
    element->setWillBeAdded(false);

    this->appendLog(tr("Train connected with %1").arg(element->objectName()));
    element->appendLog(tr("Train connected with %1").arg(this->objectName()));
}

//!
//! \brief Train::popTrain
//! \return
//!
Train *Train::popTrain()
{
    if (this->_addedTrains.size() == 0)
        return nullptr;
    else
    {
        Train *element = this->_addedTrains.last();
        this->_addedTrains.pop_back();

        qreal trainMass = this->_mass - element->mass();
        qreal acc = this->_acceleration * (this->_mass / trainMass);
        this->setAcceleration(acc);
        qreal dec = this->_decceleration * (this->_mass / trainMass);
        this->setDecceleration(dec);
        this->setMass(trainMass);

        return element;
    }
}

//!
//! \brief Train::addedComponents
//! \return
//!
int Train::addedComponents()
{
    return this->_addedTrains.size();
}


//!
//! \brief Train::priority
//! \return
//!
int Train::priority() const
{
    return this->_priority;
}

//!
//! \brief Train::stopDemand
//! \return
//!
bool Train::stopDemand() const
{
    return this->_stopDemand;
}

//!
//! \brief Train::velocity
//! \return
//!
int Train::velocity() const
{
    return this->_velocity;
}

//!
//! \brief Train::setVelocity
//! \param velocity
//!
void Train::setVelocity(int velocity)
{
    if (this->_velocity != velocity)
    {
        this->_velocity = velocity;
        emit this->velocityChanged(this->_velocity);
    }
}


//!
//! \brief Train::currentObject
//! \return
//!
Object *Train::currentObject() const
{
    return this->_currentObject;
}

//!
//! \brief Train::setCurrentObject
//! \param currentObject
//!
void Train::setCurrentObject(Object *currentObject)
{
    this->_currentObject = currentObject;
}


//!
//! \brief Train::movement
//! \return
//!
Train::TrainMovement Train::movement() const
{
    return this->_movement;
}

//!
//! \brief Train::setMovement
//! \param movement
//!
void Train::setMovement(const TrainMovement &movement)
{
    if (this->_willBeAdded || this->_addDemand)
        return;
    if (this->_movement != movement)
    {
        this->_movement = movement;
        switch (movement)
        {
        case Train::TM_Moving_ACC:
            this->appendLog(tr("Train %1 is accelerating").arg(this->objectName()));
            break;
        case Train::TM_Moving_AVG:
            this->appendLog(tr("Train %1 is moving with constant velocity").arg(this->objectName()));
            break;
        case Train::TM_Moving_DEC:
            this->appendLog(tr("Train %1 is slowing down").arg(this->objectName()));
            break;
        case Train::TM_Stopped:
            this->appendLog(tr("Train %1 has stopped.").arg(this->objectName()));
            this->_stopDemand = false;
            this->setVelocity(0);
            break;
        }

        emit this->movemendChanged(this->_movement);
    }
}

//!
//! \brief Train::setPriority
//! \param priority
//!
void Train::setPriority(int priority)
{
    if (this->_priority != priority)
    {
        this->_priority = priority;
        emit this->priorityChanged(priority);
    }
}

//!
//! \brief Train::findAnotherPath
//!
void Train::findAnotherPath()
{
    if (this->_movement == TM_Stopped)
        emit this->findPathRequired(this);
}

//!
//! \brief Train::requestConnection
//!
void Train::requestConnection()
{
    if (this->_movement == TM_Stopped)
        emit this->connectionRequested(this);
}

//!
//! \brief Train::requestDisconnection
//!
void Train::requestDisconnection()
{
    if (this->_movement == TM_Stopped)
        emit this->disconnectionRequested(this);
}

//!
//! \brief Train::direction
//! \return
//!
Train::TrainDirection Train::direction() const
{
    return this->_direction;
}

//!
//! \brief Train::setDirection
//! \param direction
//!
void Train::setDirection(const TrainDirection &direction)
{
    if (this->_willBeAdded || this->_addDemand)
        return;
    if (this->_direction != direction)
    {
        this->_direction = direction;
        emit this->directionChanged(this->_direction);
    }
}

//!
//! \brief Train::decceleration
//! \return
//!
qreal Train::decceleration() const
{
    return this->_decceleration;
}

//!
//! \brief Train::acceleration
//! \return
//!
qreal Train::acceleration() const
{
    return this->_acceleration;
}

//!
//! \brief Train::velocity
//! \return
//!
int Train::maxVelocity() const
{
    return this->_maxVelocity;
}

//!
//! \brief Train::mass
//! \return
//!
qreal Train::mass() const
{
    return this->_mass;
}

//!
//! \brief Train::setAcceleration
//! \param acceleration
//!
void Train::setAcceleration(const qreal &acceleration)
{
    if (this->_acceleration != acceleration)
    {
        this->_acceleration = acceleration;
        emit this->accelerationChanged(acceleration);
    }
}

//!
//! \brief Train::setVelocity
//! \param velocity
//!
void Train::setMaxVelocity(const int &velocity)
{
    if (this->_maxVelocity != velocity)
    {
        this->_maxVelocity = velocity;
        emit this->maxVelocityChanged(velocity);
    }
}

//!
//! \brief Train::setMass
//! \param mass
//!
void Train::setMass(const qreal &mass)
{
    if(this->_mass != mass)
    {
        this->_mass = mass;
        emit this->massChanged(mass);
    }
}

//!
//! \brief Train::setDecceleration
//! \param decceleration
//!
void Train::setDecceleration(const qreal &decceleration)
{
    if (this->_decceleration != decceleration)
    {
        this->_decceleration = decceleration;
        emit this->deccelerationChanged(decceleration);
    }
}

//!
//! \brief Train::setStopDemand
//! \param stopDemand
//!
void Train::setStopDemand(bool stopDemand)
{
    if (this->_willBeAdded || this->_addDemand)
        return;
    if (this->_stopDemand != stopDemand)
    {
        this->_stopDemand = stopDemand;
        emit this->stopDemandChanged(stopDemand);
    }
}
