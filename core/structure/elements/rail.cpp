#include "rail.h"

//!
//! \brief Rail::Rail
//! \param parent
//!
Rail::Rail(Object *parent) :
    Object(parent),
    _firstNodePosition(QPoint(-10, 0)),
    _secondNodePosition(QPoint(10, 0)),
    _zLevel(0)
{
    this->_objType = Object::OT_Rail;
}

//!
//! \brief Rail::~Rail
//!
Rail::~Rail()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief Rail::read
//! \param stream
//!
void Rail::read(QDataStream &stream)
{
    // Read Position
    QPointF position;
    stream >> position;
    this->setPosition(position);

    // Read Object Name
    QString name;
    stream >> name;
    this->setObjectName(name);

    // Read First Node Position
    QPoint firstPosition;
    stream >> firstPosition;
    this->setFirstNodePosition(firstPosition);

    // Read Second Node Position
    QPoint secondPosition;
    stream >> secondPosition;
    this->setSecondNodePosition(secondPosition);

    // Read Z-level
    int zlevel;
    stream >> zlevel;
    this->setZLevel(zlevel);
}

//!
//! \brief Rail::write
//! \param stream
//!
void Rail::write(QDataStream &stream) const
{
    stream << this->_objType
           << this->_position
           << this->objectName()
           << this->_firstNodePosition
           << this->_secondNodePosition
           << this->_zLevel;
}

//!
//! \brief Rail::readXML
//! \param fragment
//!
void Rail::readXML(QDomElement &fragment)
{
    this->_position.setX(fragment.attribute("PositionX").toDouble());
    this->_position.setY(fragment.attribute("PositionY").toDouble());

    this->setObjectName(fragment.attribute("Name"));

    this->_firstNodePosition.setX(fragment.attribute("FirstNodeX").toInt());
    this->_firstNodePosition.setY(fragment.attribute("FirstNodeY").toInt());

    this->_secondNodePosition.setX(fragment.attribute("SecondNodeX").toInt());
    this->_secondNodePosition.setY(fragment.attribute("SecondNodeY").toInt());

    this->setZLevel(fragment.attribute("zLevel").toInt());
}

//!
//! \brief Rail::writeXML
//! \param stream
//!
void Rail::writeXML(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Rail");

    stream.writeAttribute("PositionX", QString::number(this->_position.x()));
    stream.writeAttribute("PositionY", QString::number(this->_position.y()));

    stream.writeAttribute("Name", this->objectName());

    stream.writeAttribute("FirstNodeX", QString::number(this->_firstNodePosition.x()));
    stream.writeAttribute("FirstNodeY", QString::number(this->_firstNodePosition.y()));

    stream.writeAttribute("SecondNodeX", QString::number(this->_secondNodePosition.x()));
    stream.writeAttribute("SecondNodeY", QString::number(this->_secondNodePosition.y()));

    stream.writeAttribute("zLevel", QString::number(this->_zLevel));

    stream.writeEndElement();
}

//!
//! \brief Rail::keys
//! \return
//!
QStringList Rail::keys() const
{
    QStringList keys;
    keys << "Name"
         << "Position"
         << "FirstNodePosition"
         << "SecondNodePosition"
         << "zLevel";

    return keys;
}

//!
//! \brief Rail::firstNodePosition
//! \return
//!
QPoint Rail::firstNodePosition() const
{
    return this->_firstNodePosition;
}

//!
//! \brief Rail::secondNodePosition
//! \return
//!
QPoint Rail::secondNodePosition() const
{
    return this->_secondNodePosition;
}

//!
//! \brief Rail::zLevel
//! \return
//!
int Rail::zLevel() const
{
    return this->_zLevel;
}

//!
//! \brief Rail::setFirstNodePosition
//! \param firstNodePosition
//!
void Rail::setFirstNodePosition(const QPoint &firstNodePosition)
{
    if (this->_firstNodePosition != firstNodePosition)
    {
        this->_firstNodePosition = firstNodePosition;
        emit this->firstNodePositionChanged(firstNodePosition);
    }
}

//!
//! \brief Rail::setSecondNodePosition
//! \param secondNodePosition
//!
void Rail::setSecondNodePosition(const QPoint &secondNodePosition)
{
    if (this->_secondNodePosition != secondNodePosition)
    {
        this->_secondNodePosition = secondNodePosition;
        emit this->secondNodePositionChanged(secondNodePosition);
    }
}

//!
//! \brief Rail::setZLevel
//! \param zLevel
//!
void Rail::setZLevel(int zLevel)
{
    if (this->_zLevel != zLevel)
    {
        this->_zLevel = zLevel;
        emit this->zLevelChanged(zLevel);
    }
}


