#ifndef DEVICEITEMMODEL_H
#define DEVICEITEMMODEL_H

#include <QAbstractItemModel>
#include <QMimeData>
#include <QIODevice>
#include <QDataStream>

#include "rootitem.h"

//!
//! \brief The ObjectItemModel class
//!
class ObjectItemModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    //! Konstruktor klasy - jako parametr przyjmuje węzeł główny - niewidoczny obiekt
    explicit ObjectItemModel(RootItem *rootItem, QObject *parent = 0);
    //! Destruktor klasy
    virtual ~ObjectItemModel();

    //! Metoda zwracająca ilość kolumn modelu dla danego liścia w drzewie
    int columnCount ( const QModelIndex& parent = QModelIndex() ) const;
    //! Metoda zwracająca liczbę wierszy w modelu dla danego liścia w drzewie
    int rowCount ( const QModelIndex& parent = QModelIndex() ) const;
    //! Metoda zwracająca wskaźnik na obiekt przechowywany pod danym indeksem
    QVariant data ( const QModelIndex& index,
                    int role = Qt::DisplayRole ) const;
    //! Metoda zwracająca opisy używane w nagłówkach
    QVariant headerData( int section,
                         Qt::Orientation,
                         int role = Qt::DisplayRole ) const;

    //! Metoda zwracająca indeks dla podanego położenia
    QModelIndex index ( int row,
                        int column,
                        const QModelIndex& parent = QModelIndex() ) const;
    //! Metoda zwracająca indeks rodzica dla podanego położenia
    QModelIndex parent ( const QModelIndex & index ) const;

    //! Metoda zwracająca główny węzeł struktury
    Object *rootItem() const;
    //! Metoda ustawiająca główny węzeł struktury
    void setRootItem(RootItem *rootItem);
    //! Metoda czyszcząca model
    void clear();

public slots:
    //! Sygnał zgłaszany przy modyfikacji danych
    void update(void);

protected slots:
    //! Gniazdo wychwytujące reset ustawień
    void resetRoot(void);

private:
    //! Główny węzeł
    RootItem *_rootItem;
};

#endif // DEVICEITEMMODEL_H
