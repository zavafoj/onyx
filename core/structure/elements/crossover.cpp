#include "crossover.h"

//!
//! \brief Crossover::Crossover
//! \param parent
//!
Crossover::Crossover(Object *parent) :
    Object(parent),
    _state(CS_Unknown),
    _firstNodePosition(-10,0),
    _secondNodePosition(10,0),
    _branchNodePosition(10,-10)
{
    this->_objType = OT_Crossover;
}

//!
//! \brief Crossover::~Crossover
//!
Crossover::~Crossover()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief Crossover::read
//! \param stream
//!
void Crossover::read(QDataStream &stream)
{
    // Read Position
    QPointF position;
    stream >> position;
    this->setPosition(position);

    // Read Object Name
    QString name;
    stream >> name;
    this->setObjectName(name);

    // Read First Node Position
    QPoint firstPosition;
    stream >> firstPosition;
    this->setFirstNodePosition(firstPosition);

    // Read Second Node Position
    QPoint secondPosition;
    stream >> secondPosition;
    this->setSecondNodePosition(secondPosition);

    // Read Branch Node Position
    QPoint branchPosition;
    stream >> branchPosition;
    this->setBranchNodePosition(branchPosition);

    // Read Crossover State
    int state;
    stream >> state;
    this->setState((CrossoverStates) state);
}

//!
//! \brief Crossover::write
//! \param stream
//!
void Crossover::write(QDataStream &stream) const
{
    stream << this->_objType
           << this->_position
           << this->objectName()
           << this->_firstNodePosition
           << this->_secondNodePosition
           << this->_branchNodePosition
           << this->_state;
}

//!
//! \brief Crossover::readXML
//! \param fragment
//!
void Crossover::readXML(QDomElement &fragment)
{
    this->_position.setX(fragment.attribute("PositionX").toDouble());
    this->_position.setY(fragment.attribute("PositionY").toDouble());

    this->setObjectName(fragment.attribute("Name"));

    this->_firstNodePosition.setX(fragment.attribute("FirstNodeX").toInt());
    this->_firstNodePosition.setY(fragment.attribute("FirstNodeY").toInt());

    this->_secondNodePosition.setX(fragment.attribute("SecondNodeX").toInt());
    this->_secondNodePosition.setY(fragment.attribute("SecondNodeY").toInt());

    this->_branchNodePosition.setX(fragment.attribute("BranchNodeX").toInt());
    this->_branchNodePosition.setY(fragment.attribute("BranchNodeY").toInt());

    this->setState((CrossoverStates) fragment.attribute("State").toInt());
}

//!
//! \brief Crossover::writeXML
//! \param stream
//!
void Crossover::writeXML(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Crossover");

    stream.writeAttribute("PositionX", QString::number(this->_position.x()));
    stream.writeAttribute("PositionY", QString::number(this->_position.y()));

    stream.writeAttribute("Name", this->objectName());

    stream.writeAttribute("FirstNodeX", QString::number(this->_firstNodePosition.x()));
    stream.writeAttribute("FirstNodeY", QString::number(this->_firstNodePosition.y()));

    stream.writeAttribute("SecondNodeX", QString::number(this->_secondNodePosition.x()));
    stream.writeAttribute("SecondNodeY", QString::number(this->_secondNodePosition.y()));

    stream.writeAttribute("BranchNodeX", QString::number(this->_branchNodePosition.x()));
    stream.writeAttribute("BranchNodeY", QString::number(this->_branchNodePosition.y()));

    stream.writeAttribute("State",  QString::number(this->_state));

    stream.writeEndElement();
}

//!
//! \brief Crossover::keys
//! \return
//!
QStringList Crossover::keys() const
{
    QStringList keys;
    keys << "Name"
         << "Position"
         << "FirstNodePosition"
         << "SecondNodePosition"
         << "BranchNodePosition"
         << "State";
    return keys;
}

//!
//! \brief Crossover::firstNodePosition
//! \return
//!
QPoint Crossover::firstNodePosition() const
{
    return this->_firstNodePosition;
}

//!
//! \brief Crossover::secondNodePosition
//! \return
//!
QPoint Crossover::secondNodePosition() const
{
    return this->_secondNodePosition;
}

//!
//! \brief Crossover::branchNodePosition
//! \return
//!
QPoint Crossover::branchNodePosition() const
{
    return this->_branchNodePosition;
}

//!
//! \brief Crossover::setFirstNodePosition
//! \param firstNodePosition
//!
void Crossover::setFirstNodePosition(const QPoint &firstNodePosition)
{
    if (this->_firstNodePosition != firstNodePosition)
    {
        this->_firstNodePosition = firstNodePosition;
        emit this->firstNodePositionChanged(firstNodePosition);
    }
}

//!
//! \brief Crossover::setSecondNodePosition
//! \param secondNodePosition
//!
void Crossover::setSecondNodePosition(const QPoint &secondNodePosition)
{
    if (this->_secondNodePosition != secondNodePosition)
    {
        this->_secondNodePosition = secondNodePosition;
        emit this->firstNodePositionChanged(secondNodePosition);
    }
}

//!
//! \brief Crossover::setBranchNodePosition
//! \param branchNodePosition
//!
void Crossover::setBranchNodePosition(const QPoint &branchNodePosition)
{
    if (this->_branchNodePosition != branchNodePosition)
    {
        this->_branchNodePosition = branchNodePosition;
        emit this->branchNodePositionChanged(branchNodePosition);
    }
}

//!
//! \brief Crossover::state
//! \return
//!
Crossover::CrossoverStates Crossover::state() const
{
    return this->_state;
}

//!
//! \brief Crossover::setState
//! \param state
//!
void Crossover::setState(const CrossoverStates &state)
{
    if (this->_isExhaused)
        return;

    if (this->_state != state)
    {
        this->_state = state;
        emit this->stateChanged(state);
        emit this->modelUpdated();

        QString stateStr;
        switch (state)
        {
        case CS_Main:
            stateStr = tr("Main");
            break;
        case CS_Branch:
            stateStr = tr("Branch");
            break;
        default:
            stateStr = tr("Unknown");
        }

        this->appendLog(tr("Crossover %1 state changed to: %2")
                        .arg(this->objectName())
                        .arg(stateStr));
    }
}

