#ifndef ROUTEINSPECTOR_H
#define ROUTEINSPECTOR_H

#include <QObject>
#include "route.h"

//!
//! \brief The RouteInspector class
//!
class RouteInspector : public QObject
{
    Q_OBJECT
public:
    explicit RouteInspector(QObject *parent = 0);
    virtual ~RouteInspector();

    QHash<Object*, QList<Route*> > _possibleRoutes;
    QList <Train*> _trains;

signals:

public slots:
    void assignRoute(Object *o, Route *r);

};

#endif // ROUTEINSPECTOR_H
