#ifndef OBJECT_H
#define OBJECT_H

#include <QObject>
#include <QXmlStreamReader>
#include <QtXml>
#include <QDebug>
#include <QGraphicsItem>
#include <QTime>

//!
//! \brief The Object class
//! Klasa abstrakcyjna dla każdego elementu isniejącego
//! w modelu.
class Object : public QObject
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")
    //! Deklaracja pola wyliczeniowego, aby był dostępny w trybie skryptowym
    Q_ENUMS (ObjectType)
    //! Właściwość 'Name'
    Q_PROPERTY (QString Name READ objectName WRITE setObjectName)
    //! Właściwość 'Position'
    Q_PROPERTY (QPointF Position READ position WRITE setPosition NOTIFY positionChanged)

public:
    static QTime simulationTime;
    //! Struktura określająca wpis do logu
    struct LogEntry
    {
        //! Zdarzenie w czasie
        QTime _eventTime;
        //! Wiadomość zawarta w logu
        QString _message;
    };

    //! Typ wyliczeniowy określający typ obiektu
    enum ObjectType
    {
        OT_Root         =   0x00,
        OT_Train        =   0x01,
        OT_Semaphore    =   0x02,
        OT_Balisa       =   0x03,
        OT_Crossover    =   0x04,
        OT_Rail         =   0x05,
        OT_Terminator   =   0x06,
        OT_Station      =   0x07,
        OT_Group        =   0x10,
        OT_DetectionRect=   0x11,

        OT_Unknown      =     -1
    };

    //! Unikalny Id obiektu w systemie - wspólny dla całej klasy
    static quint64 uniqueId;
    //! Metoda pobierająca Unikalnegy Id przypisanego do obiektu
    int getUniqueId() const;
    //! Metoda zwiększająca licznik UniqueId
    static void increaseUniqueId();

    //! Konstruktor
    explicit Object(Object *parent = 0);
    //! Destruktor
    virtual ~Object();

    //! Metoda zwracająca typ obiektu
    ObjectType type() const;
    //! Metoda ustawiająca typ obiektu
    void setType(const ObjectType &type);

    //! Metoda zwracająca listę z przypisanymi obiektami - 'dziećmi'
    QList<Object *> childrenItems() const;
    //! Metoda przypisująca listę dzieci do obiektu
    void setChildrenItems(const QList<Object *> &childrenItems);

    //! Metoda zwracająca obiekt, będący rodzicem danego obiektu
    Object *parentItem() const;
    //! Metoda ustawiająca rodzica danego obiektu
    void setParentItem(Object *parentItem);

    //! Metoda zwracająca ilość dzieci przypisanych do obiektu
    int childCount() const;
    //! Metoda zwracająca dziecko przypisane po określonym Id lub nullptr
    Object *child (int id);
    //! Metoda zwracająca Id obiektu będącego dzieckiem danego obiektu
    int childId(Object *object);
    //! Metoda zwracająca informację, czy zawiera określony obiekt w liście dzieci
    bool containsChild(Object *object);

    //! Metoda dodająca dziecko do listy dzieci
    bool addChild(Object *child);
    //! Metoda usuwająca dziecko z listy dzieci po wskaźniku na dziecko
    bool removeChild(Object *child);
    //! Metoda usuwająca dziecko z listy dzieci po Id dziecka
    bool removeChild(int id);
    //! Metoda wymuszająca emisję sygnału powiadamiającego o dodaniu dziecka
    void emitAddChild(Object *child);
    //! Metoda usuwająca wszystkie dzieci
    void removeChildren(void);

    //! Wirtualna metoda odczytująca fragment strumienia danych
    //! i przypisująca go do określonego obiektu (np. ładowanie z pliku)
    virtual void read(QDataStream &stream) = 0;
    //! Wirtualna metoda zapisująca obiekt do strumienia danych
    virtual void write(QDataStream &stream) const = 0;

    //! Wirtualna metoda odczytująca fragment pliku XML i zapisująca
    //! ustawienia do obiektu
    virtual void readXML(QDomElement &fragment) = 0;
    //! Wirtualna metoda zapisująca obiekt w postaci węzła XML
    virtual void writeXML(QXmlStreamWriter &stream) const = 0;

    //! Wirtualna metoda zwracająca klucze - argumenty występujące w obiekcie
    virtual QStringList keys() const = 0;

    //! Metoda zwracająca pozycję środka obiektu w symulatorze
    QPointF position() const;

    //! Statyczna metoda zwracająca ciąg znaków obiektu
    //! przypisanych do konkretnego typu
    static QString getObjectTypeString(Object::ObjectType type);
    //! Statyczna metoda zwracająca ciąg znaków obiektu
    //! przypisanych do konkretnego typu w zależności od wybranego tłumaczenia
    static QString getObjectTypeTrString(Object::ObjectType type);

    //! Metoda zwracająca typ obiektu przez podany ciąg znaków
    static Object::ObjectType getObjectTypeFromString(const QString &typeName);

    //! Metoda zwracająca pełen log obiektu
    QList<LogEntry> objectLog() const;

    bool isExhaused() const;

signals:
    //!
    //! \brief childAdded
    //! Sygnał zgłaszany przy dodaniu obiektu do listy dzieci
    //! \param child
    void childAdded(Object *child);
    //!
    //! \brief updateView
    //! Sygnał zgłaszany do odświeżenia drzewa obiektów (zmiana parametrów)
    void updateView(void);
    //!
    //! \brief positionChanged
    //! Sygnał zgłaszany przy zmianie pozycji obiektu
    //! \param position
    void positionChanged(const QPointF position);
    //!
    //! \brief childRemoved
    //! Sygnał zgłaszany przy usunięciu obiektu
    void childRemoved();
    //!
    //! \brief modelUpdated
    //! Sygnał zgłaszany przy zmianie drzewa obiektów
    void modelUpdated();
    //!
    //! \brief logAppended
    //! \param entry
    //! Sygnał zgłaszany przez zmiany w logach
    void logAppended(const Object::LogEntry &entry);

public slots:
    //! Gniazdo ustawiające pozycję obiektu
    void setPosition(QPointF position);
    //! Gniazdo ustawiające określony parametr przez dowolną wartość
    void setPropertySlot(const char *name, const QVariant &value);
    //! Gniazdo ustawiające nazwę obiektu
    void setName(const QString &name);
    //! Gniazdo dopisujące wiadomość do logu
    void appendLog(const QString &message);
    //! Gniazdo ustawiające zajętość
    void setIsExhaused(bool isExhaused);

protected slots:
    //! Gniazdą usuwające konkretne dziecko
    virtual void removeChildInternalSlot();

protected:
    //! Gniazdo obsługujące zmiany interfejsu użytkownika (zmiana języka)
    void changeEvent(QEvent *event);

protected:
    //! Pole unikalnego Id dla danego obiektu
    quint64 _uniqueId;
    //! Pole listy dzieci przypisanych do obiektu
    QList<Object *> _childrenItems;
    //! Pole rodzica obiektu
    Object *_parentItem;
    //! Pole określające typ obiektu
    ObjectType _objType;
    //! Pole określające pozycję obiektu
    QPointF _position;
    //! Pole logu
    QList<LogEntry> _log;
    //! Zajętość toru
    bool _isExhaused;
    int _noOfTrains;
};
//! Operacje strumieniowania
QDataStream &operator<<(QDataStream &stream, const Object &object);
QDataStream &operator>>(QDataStream &stream, Object &object);
QDataStream &operator<<(QDataStream &stream, const Object *object);
QDataStream &operator>>(QDataStream &stream, Object *object);

#endif //! OBJECT_H
