#ifndef TERMINATOR_H
#define TERMINATOR_H

#include "object.h"

//!
//! \brief The Terminator class
//!
class Terminator : public Object
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")
    Q_PROPERTY(QPoint branchNodePosition READ branchNodePosition WRITE setBranchNodePosition NOTIFY branchNodePositionChanged)

public:
    explicit Terminator(Object *parent = 0);
    virtual ~Terminator();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    virtual QStringList keys() const;

    QPoint branchNodePosition() const;

signals:
    void branchNodePositionChanged(const QPoint &position);

public slots:
    void setBranchNodePosition(const QPoint &branchNodePosition);

private:
    QPoint _branchNodePosition;
};

#endif // TERMINATOR_H
