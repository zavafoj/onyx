#ifndef ROUTEMANAGER_H
#define ROUTEMANAGER_H

#include <QObject>
#include "route.h"

//!
//! \brief The RouteManager class
//!
class RouteManager : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("Author", "Wojciech Ossowski")

public:
    explicit RouteManager(QObject *parent = 0);
    ~RouteManager();

signals:

public slots:
//    void buildRoute();

private:

};

#endif // ROUTEMANAGER_H
