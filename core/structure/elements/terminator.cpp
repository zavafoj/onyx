#include "terminator.h"

//!
//! \brief Terminator::Terminator
//! \param parent
//!
Terminator::Terminator(Object *parent) :
    Object(parent),
    _branchNodePosition(QPoint(-10, 0))
{
    this->_objType = OT_Terminator;
}

//!
//! \brief Terminator::~Terminator
//!
Terminator::~Terminator()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief Terminator::read
//! \param stream
//!
void Terminator::read(QDataStream &stream)
{
    // Read Position
    QPointF position;
    stream >> position;
    this->setPosition(position);

    // Read Object Name
    QString name;
    stream >> name;
    this->setObjectName(name);

    // Read Branch Node Position
    QPoint branchPosition;
    stream >> branchPosition;
    this->setBranchNodePosition(branchPosition);
}

//!
//! \brief Terminator::write
//! \param stream
//!
void Terminator::write(QDataStream &stream) const
{
    stream << this->_objType
           << this->_position
           << this->objectName()
           << this->_branchNodePosition;
}

//!
//! \brief Terminator::readXML
//! \param fragment
//!
void Terminator::readXML(QDomElement &fragment)
{
    this->_position.setX(fragment.attribute("PositionX").toDouble());
    this->_position.setY(fragment.attribute("PositionY").toDouble());

    this->setObjectName(fragment.attribute("Name"));

    this->_branchNodePosition.setX(fragment.attribute("BranchNodeX").toInt());
    this->_branchNodePosition.setY(fragment.attribute("BranchNodeY").toInt());
}

//!
//! \brief Terminator::writeXML
//! \param stream
//!
void Terminator::writeXML(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Terminator");

    stream.writeAttribute("PositionX", QString::number(this->_position.x()));
    stream.writeAttribute("PositionY", QString::number(this->_position.y()));

    stream.writeAttribute("Name", this->objectName());

    stream.writeAttribute("BranchNodeX", QString::number(this->_branchNodePosition.x()));
    stream.writeAttribute("BranchNodeY", QString::number(this->_branchNodePosition.y()));

    stream.writeEndElement();
}

//!
//! \brief Terminator::keys
//! \return
//!
QStringList Terminator::keys() const
{
    QStringList keys;

    keys << "Name"
         << "Position"
         << "BranchNodePosition";

    return keys;
}

//!
//! \brief Terminator::branchNodePosition
//! \return
//!
QPoint Terminator::branchNodePosition() const
{
    return this->_branchNodePosition;
}

//!
//! \brief Terminator::setBranchNodePosition
//! \param branchNodePosition
//!
void Terminator::setBranchNodePosition(const QPoint &branchNodePosition)
{
    if (this->_branchNodePosition != branchNodePosition)
    {
        this->_branchNodePosition = branchNodePosition;
        emit this->branchNodePositionChanged(branchNodePosition);
    }
}
