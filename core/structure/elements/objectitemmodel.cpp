#include "objectitemmodel.h"
#include <QDebug>
#include <QMessageBox>
#include <QTreeView>

//!
//! \brief ObjectItemModel::ObjectItemModel
//! \param parent
//!
ObjectItemModel::ObjectItemModel(RootItem *rootItem,
                                 QObject *parent) :
    QAbstractItemModel(parent)
{
    this->_rootItem = rootItem;

    this->connect(this->_rootItem,          SIGNAL(updateView()),
                  this,                     SLOT(update()));
}

//!
//! \brief ObjectItemModel::~ObjectItemModel
//!
ObjectItemModel::~ObjectItemModel()
{

}

//!
//! \brief ObjectItemModel::columnCount
//! \return
//!
int ObjectItemModel::columnCount(const QModelIndex &) const
{
    return 2;
}

//!
//! \brief ObjectItemModel::rowCount
//! \param parent
//! \return
//!
int ObjectItemModel::rowCount(const QModelIndex &parent) const
{
    if (!this->_rootItem)
        return 0;

    Object *parentItem;

    if (!parent.isValid())
        parentItem = this->_rootItem;
    else
        parentItem = static_cast<Object*>(parent.internalPointer());

    return parentItem->childCount();
}

//!
//! \brief ObjectItemModel::data
//! \param index
//! \param role
//! \return
//!
QVariant ObjectItemModel::data(const QModelIndex &index,
                               int role) const
{
    if (!index.isValid())
        return QVariant();

    Object *obj = static_cast<Object*> (index.internalPointer());

    switch (role)
    {
    case Qt::DisplayRole:
        switch (index.column())
        {
        case 0:
            if (obj->objectName().isEmpty())
                return tr("%1 id: %2").arg(Object::getObjectTypeTrString(obj->type()))
                        .arg(QString::number(obj->getUniqueId()));
            else
                return obj->objectName();

        case 1:
            switch (obj->type())
            {
            case Object::OT_Group:
                return tr("Elements: %1").arg(obj->childCount());
            default:
                return obj->metaObject()->className();
            }
        }
        break;
    case Qt::DecorationRole:
        break;
    case Qt::ToolTip:
        break;
    case Qt::TextColorRole:
        break;
    }
    return QVariant();
}

//!
//! \brief ObjectItemModel::headerData
//! \param section
//! \param role
//! \return
//!
QVariant ObjectItemModel::headerData(int section, Qt::Orientation orient, int role) const
{
    if (orient != Qt::Horizontal || role != Qt::DisplayRole)
        return QVariant();

    switch (section)
    {
    case 0:
        return tr("Object");
    case 1:
        return tr("Properties");
    case 2:
        return tr("Child Count");
    }
    return QVariant();
}

//!
//! \brief ObjectItemModel::index
//! \param row
//! \param column
//! \param parent
//! \return
//!
QModelIndex ObjectItemModel::index(int row, int column, const QModelIndex &parent) const
{
    Object *parentObject;

    if (!parent.isValid())
        parentObject = this->_rootItem;
    else
        parentObject = static_cast<Object*> (parent.internalPointer());

    if (row >= 0 && row < parentObject->childCount())
        return createIndex(row, column, parentObject->child(row));
    else
        return QModelIndex();
}

//!
//! \brief ObjectItemModel::parent
//! \param index
//! \return
//!
QModelIndex ObjectItemModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    Object *indexObject = qobject_cast<Object*>((Object*)index.internalPointer() );
    if (!indexObject)
        return QModelIndex();

    Object *parentObject = indexObject->parentItem();

    if(parentObject == this->_rootItem || !parentObject)
        return QModelIndex();

    Object *grandParentObject = parentObject->parentItem();
    if (!grandParentObject)
        return QModelIndex();

    return createIndex( grandParentObject->childId(parentObject), 0, parentObject );
}

//!
//! \brief ObjectItemModel::resetRoot
//!
void ObjectItemModel::resetRoot()
{
    this->_rootItem = 0x0;
}

//!
//! \brief ObjectItemModel::update
//!
void ObjectItemModel::update()
{
    emit this->layoutChanged();
}

//!
//! \brief ObjectItemModel::rootItem
//! \return
//!
Object *ObjectItemModel::rootItem() const
{
    return this->_rootItem;
}

//!
//! \brief ObjectItemModel::setRootItem
//! \param rootItem
//!
void ObjectItemModel::setRootItem(RootItem *rootItem)
{
    this->_rootItem = rootItem;
}

//!
//! \brief ObjectItemModel::clear
//!
void ObjectItemModel::clear()
{
    this->beginResetModel();

    this->_rootItem->clear();

    this->endResetModel();
}

