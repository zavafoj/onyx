#include "routeinspector.h"

//!
//! \brief RouteInspector::RouteInspector
//! \param parent
//!
RouteInspector::RouteInspector(QObject *parent)
    : QObject(parent)
{

}

//!
//! \brief RouteInspector::~RouteInspector
//!
RouteInspector::~RouteInspector()
{

}

//!
//! \brief RouteInspector::createConnection
//! \param o
//! \param r
//!
void RouteInspector::assignRoute(Object *o, Route *r)
{
    if (this->_possibleRoutes[o].empty())
    {
        QList<Route*> list;
        this->_possibleRoutes.insert(o, list);
    }

    this->_possibleRoutes[o].push_back(r);

    this->connect(r,        SIGNAL(stateChanged(Route::RouteStates)),
                  this,     SLOT(routeStateChanged(Route::RouteStates)));

}
