#ifndef ROUTE_H
#define ROUTE_H

#include "elements.h"
#include <QPainterPath>

//!
//! \brief The Route class
//!
class Route : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("Author", "Wojciech Ossowski")

    Q_ENUMS(RouteStates)
    Q_PROPERTY(Route::RouteStates state READ state WRITE setState NOTIFY stateChanged)

public:
    enum RouteStates
    {
        RS_Available            =    1,
        RS_Unavailable          =   -1
    };

    explicit Route(QObject *parent = 0);
    Route (Route *r);
    virtual ~Route();

    void pushToRoute(Object *obj);
    void pushToRoute(Crossover *c, Crossover::CrossoverStates state);

    RouteStates state() const;
    int zLevel() const;
    int vMax() const;

    QList<QPointF> path() const;
    QPainterPath &painterPath();
    void createPath();
    static bool qPointFFuzzyCompare(QPointF *first, QPointF *second);
    void setExhaused(bool exhaused);
    bool exhaused() const;

    QList<Object *> elements() const;
    void setElements(const QList<Object *> &elements);

    Crossover::CrossoverStates crossoverSettings(Crossover *c) const;

    void initState();

    QPointF beginPoint() const;

    QPointF endPoint() const;
    void setEndPoint(const QPointF &endPoint);

signals:
    //!
    //! \brief routeStateChanged
    //! \param state
    //!
    void stateChanged(Route::RouteStates state);
    void pushedToRoute(Object *obj);

public slots:
    void setZLevel(int zLevel);
    void setVMax(int vMax);
    void setState(const RouteStates &state);
    void setActive();
    void crossoverStateChanged();

private:
    Route *_previous;
    Route *_next;
    QHash <Crossover*, Crossover::CrossoverStates> _crossoversSettings;
    QList <Object *> _elements;
    int _vMax;
    int _zLevel;
    RouteStates _state;
    QList<QPointF> _path;
    QPainterPath _painterPath;
    bool _exhaused;
    QPointF _beginPoint;
    QPointF _endPoint;
};

#endif // ROUTE_H
