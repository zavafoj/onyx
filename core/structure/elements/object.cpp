#include "object.h"
#include <QDebug>

quint64 Object::uniqueId = 0;
QTime Object::simulationTime = QTime::currentTime();

//!
//! \brief Object::Object
//! \param parent
//!
Object::Object(Object *parent) :
    QObject(parent),
    _parentItem(nullptr),
    _objType(OT_Unknown),
    _isExhaused(false),
    _noOfTrains(0)
{
    this->_parentItem = parent;

    this->_uniqueId = uniqueId;

    increaseUniqueId();

    if (parent)
        parent->addChild(this);

    this->appendLog("Object Created");
}

//!
//! \brief Object::~Object
//!
Object::~Object()
{
    qDeleteAll(this->_childrenItems);
    this->_childrenItems.clear();
}

//!
//! \brief Object::type
//! \return
//!
Object::ObjectType Object::type() const
{
    return this->_objType;
}

//!
//! \brief Object::setType
//! \param type
//!
void Object::setType(const ObjectType &type)
{
    this->_objType = type;
}

//!
//! \brief Object::childrenItems
//! \return
//!
QList<Object *> Object::childrenItems() const
{
    return this->_childrenItems;
}

//!
//! \brief Object::setChildrenItems
//! \param childrenItems
//!
void Object::setChildrenItems(const QList<Object *> &childrenItems)
{
    this->_childrenItems = childrenItems;
}

//!
//! \brief Object::parentItem
//! \return
//!
Object *Object::parentItem() const
{
    if (this->_parentItem)
        return this->_parentItem;

    return nullptr;
}

//!
//! \brief Object::setParentItem
//! \param parentItem
//!
void Object::setParentItem(Object *parentItem)
{
    this->_parentItem = parentItem;
}

//!
//! \brief Object::addChild
//! \param child
//!
bool Object::addChild(Object *child)
{
    if (!this->_childrenItems.contains(child))
    {
        this->_childrenItems.append(child);
        this->connect(child,        SIGNAL(destroyed()),
                      this,         SLOT(removeChildInternalSlot()),
                      Qt::DirectConnection);

        this->connect(child,        SIGNAL(updateView()),
                      this,         SIGNAL(updateView()));

        this->connect(child,        SIGNAL(childRemoved()),
                      this,         SIGNAL(childRemoved()));

        emit this->childAdded(child);
        emit this->updateView();
        return true;
    }
    return false;
}

//!
//! \brief Object::removeChild
//! \param child
//! \return
//!
bool Object::removeChild(Object *child)
{
    bool removed = this->_childrenItems.removeOne(child);
    emit this->childRemoved();
    return removed;
}

//!
//! \brief Object::removeChild
//! \param id
//! \return
//!
bool Object::removeChild(int id)
{
    if (id < 0 || id >= this->_childrenItems.count())
        return false;

    this->removeChild(this->_childrenItems.at(id));
    emit this->updateView();

    return true;
}

//!
//! \brief Object::emitAddChild
//! \param child
//!
void Object::emitAddChild(Object *child)
{
    emit this->childAdded(child);
    emit this->updateView();
}

//!
//! \brief Object::removeChildrenRecursively
//!
void Object::removeChildren()
{
    foreach (Object *o, this->_childrenItems)
    {
        o->removeChildren();
        this->removeChild(o);
    }
    emit this->updateView();
}

//!
//! \brief Object::childCount
//! \return
//!
int Object::childCount() const
{
    return this->_childrenItems.count();
}

//!
//! \brief Object::child
//! \param id
//! \return
//!
Object *Object::child(int id)
{
    return this->_childrenItems.value(id);
}

//!
//! \brief Object::childId
//! \param object
//! \return
//!
int Object::childId(Object *object)
{
    if (object)
        return this->_childrenItems.indexOf(object);
    else
        return -1;
}

//!
//! \brief Object::containsObject
//! \param object
//! \return
//!
bool Object::containsChild(Object *object)
{
    return this->_childrenItems.contains(object);
}

//!
//! \brief Object::removeChildInternalSlot
//!
void Object::removeChildInternalSlot()
{
    Object *c = static_cast<Object*> (this->sender());

    this->removeChild(c);
}

//!
//! \brief Object::isExhaused
//! \return
//!
bool Object::isExhaused() const
{
    return _isExhaused;
}

//!
//! \brief Object::setIsExhaused
//! \param isExhaused
//!
void Object::setIsExhaused(bool isExhaused)
{
    this->_noOfTrains = isExhaused ? this->_noOfTrains + 1 : this->_noOfTrains - 1;

    this->_isExhaused = this->_noOfTrains != 0;
}

//!
//! \brief Object::getObjectTypeString
//! \param type
//! \return
//!
QString Object::getObjectTypeString(Object::ObjectType type)
{
    switch (type)
    {
    case OT_Train:
        return "Train";
    case OT_Semaphore:
        return "Semaphore";
    case OT_Balisa:
        return "Balisa";
    case OT_Crossover:
        return "Crossover";
    case OT_Rail:
        return "Rail";
    case OT_Terminator:
        return "Terminator";
    case OT_Station:
        return "Station";
    case OT_Group:
        return "Group";
    default:
        return "Unknown";
    }
}

//!
//! \brief Object::getObjectTypeTrString
//! \param type
//! \return
//!
QString Object::getObjectTypeTrString(Object::ObjectType type)
{
    switch (type)
    {
    case OT_Train:
        return tr("Train");
    case OT_Semaphore:
        return tr("Semaphore");
    case OT_Balisa:
        return tr("Balisa");
    case OT_Crossover:
        return tr("Crossover");
    case OT_Rail:
        return tr("Rail");
    case OT_Terminator:
        return tr("Terminator");
    case OT_Station:
        return tr("Station");
    case OT_Group:
        return tr("Group");
    default:
        return tr("None");
    }
}

//!
//! \brief Object::getObjectTypeFromString
//! \param typeName
//! \return
//!
Object::ObjectType Object::getObjectTypeFromString(const QString &typeName)
{
    if (typeName == "Train")
        return OT_Train;
    if (typeName == "Semaphore")
        return OT_Semaphore;
    if (typeName == "Balisa")
        return OT_Balisa;
    if (typeName == "Crossover")
        return OT_Crossover;
    if (typeName == "Rail")
        return OT_Rail;
    if (typeName == "Terminator")
        return OT_Terminator;
    if (typeName == "Station")
        return OT_Station;
    if (typeName == "Group")
        return OT_Group;
    else
        return OT_Unknown;
}

//!
//! \brief Object::objectLog
//! \return
//!
QList<Object::LogEntry> Object::objectLog() const
{
    return this->_log;
}

//!
//! \brief Object::position
//! \return
//!
QPointF Object::position() const
{
    return this->_position;
}

//!
//! \brief Object::setPosition
//! \param position
//!
void Object::setPosition(QPointF position)
{
    if (this->_position != position)
    {
        this->_position = position;
        emit this->positionChanged(position);
        emit this->updateView();
    }
}

//!
//! \brief Object::setPropertySlot
//! \param name
//! \param value
//!
void Object::setPropertySlot(const char *name, const QVariant &value)
{
    this->setProperty(name, value);
    emit this->updateView();
}

//!
//! \brief Object::setName
//! \param name
//!
void Object::setName(const QString &name)
{
    this->setObjectName(name);
    emit this->updateView();
}

//!
//! \brief Object::appendLog
//! \param message
//!
void Object::appendLog(const QString &message)
{
    LogEntry le;
    le._eventTime = Object::simulationTime;
    le._message = message;

    this->_log.append(le);
    emit this->logAppended(le);
}

//!
//! \brief Object::getUniqueId
//! \return
//!
int Object::getUniqueId() const
{
    return this->_uniqueId;
}

//!
//! \brief Object::increaseUniqueId
//!
void Object::increaseUniqueId()
{
    uniqueId++;
}

//!
//! \brief operator <<
//! \param stream
//! \param object
//! \return
//!
QDataStream &operator<<(QDataStream &stream, const Object &object)
{
    object.write(stream);

    return stream;
}

//!
//! \brief operator >>
//! \param stream
//! \param object
//! \return
//!
QDataStream &operator>>(QDataStream &stream, Object &object)
{
    object.read(stream);

    return stream;
}

//!
//! \brief operator <<
//! \param stream
//! \param object
//! \return
//!
QDataStream &operator<<(QDataStream &stream, const Object *object)
{
    object->write(stream);

    return stream;
}

//!
//! \brief operator >>
//! \param stream
//! \param object
//! \return
//!
QDataStream &operator>>(QDataStream &stream, Object *object)
{
    object->read(stream);

    return stream;
}
