#ifndef BALISAGROUP_H
#define BALISAGROUP_H

#include "object.h"

//!
//! \brief The ObjectGroup class
//!
class ObjectGroup : public Object
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit ObjectGroup(Object *parent = 0);
    virtual ~ObjectGroup();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    virtual QStringList keys() const;

protected:
    Object *createObjectByType(Object::ObjectType type);

};

#endif // BALISAGROUP_H
