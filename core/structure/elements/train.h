#ifndef TRAIN_H
#define TRAIN_H

#include "object.h"

//!
//! \brief The Train class
//!
class Train : public Object
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")
    Q_PROPERTY(qreal acceleration READ acceleration WRITE setAcceleration NOTIFY accelerationChanged)
    Q_PROPERTY(qreal mass READ mass WRITE setMass NOTIFY massChanged)
    Q_PROPERTY(qreal maxVelocity READ maxVelocity WRITE setMaxVelocity NOTIFY maxVelocityChanged)
    Q_PROPERTY(qreal decceleration READ decceleration WRITE setDecceleration NOTIFY deccelerationChanged)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY priorityChanged)
    Q_ENUMS (TrainDirection)
    Q_ENUMS (TrainMovement)

public:
    enum TrainDirection {TD_Forward, TD_Backward};
    enum TrainMovement {TM_Moving_ACC, TM_Moving_DEC, TM_Moving_AVG, TM_Stopped};
    explicit Train(Object *parent = 0);
    virtual ~Train();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    virtual QStringList keys() const;

    qreal acceleration() const;
    int maxVelocity() const;
    qreal mass() const;
    qreal decceleration() const;

    TrainDirection direction() const;
    TrainMovement movement() const;

    Object *currentObject() const;
    void setCurrentObject(Object *currentObject);

    int velocity() const;
    void setVelocity(int velocity);

    bool stopDemand() const;

    int priority() const;

    bool distanceAlert() const;
    void setDistanceAlert(bool distanceAlert);

    void addTrain(Train *element);
    Train *popTrain();
    int addedComponents();

    bool willBeAdded() const;
    void setWillBeAdded(bool willBeAdded);

    bool addDemand() const;
    void setAddDemand(bool addDemand);

    bool removeDemand() const;
    void setRemoveDemand(bool removeDemand);

    bool willBeRemoved() const;
    void setWillBeRemoved(bool willBeRemoved);

signals:
    void accelerationChanged(const qreal &acceleration);
    void maxVelocityChanged(const int &maxVelocity);
    void massChanged(const qreal &mass);
    void deccelerationChanged(const qreal &decceleration);
    void velocityChanged(const int &velocity);
    void stopDemandChanged(bool stopDemand);
    void directionChanged(const Train::TrainDirection &direction);
    void movemendChanged(const Train::TrainMovement &movement);
    void priorityChanged(const int &priority);
    void findPathRequired(Train *train);
    void connectionRequested(Train *train);
    void disconnectionRequested(Train *train);

public slots:
    void setAcceleration(const qreal &acceleration);
    void setMaxVelocity(const int &maxVelocity);
    void setMass(const qreal &mass);
    void setDecceleration(const qreal &decceleration);
    void setStopDemand(bool stopDemand);
    void setDirection(const TrainDirection &direction);
    void setMovement(const TrainMovement &movement);
    void setPriority(int priority);
    void findAnotherPath();
    void requestConnection();
    void requestDisconnection();

protected slots:
    void retranslate();

private:
    int _priority;
    qreal _acceleration;
    qreal _decceleration;
    int _maxVelocity;
    qreal _mass;
    int _velocity;
    bool _stopDemand;
    bool _distanceAlert;

    bool _willBeAdded;
    bool _addDemand;
    bool _removeDemand;
    bool _willBeRemoved;
    QList <Train*> _addedTrains;

    TrainDirection _direction;
    TrainMovement _movement;
    Object *_currentObject;
};

#endif // TRAIN_H
