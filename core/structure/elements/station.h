#ifndef STATION_H
#define STATION_H

#include "rail.h"

//!
//! \brief The Station class
//!
class Station : public Rail
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")
    Q_PROPERTY(QString stationName READ stationName WRITE setStationName NOTIFY stationNameChanged)

public:
    explicit Station(Object *parent = 0);
    virtual ~Station();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    virtual QStringList keys() const;

    QString stationName() const;

signals:
    void stationNameChanged(const QString &stationName);

public slots:
    void setStationName(const QString &stationName);

private:
    QString _stationName;
};

#endif // STATION_H
