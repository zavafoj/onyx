#include "route.h"

//!
//! \brief Route::Route
//! \param parent
//!
Route::Route(QObject *parent):
    QObject(parent),
    _previous(nullptr),
    _next(nullptr),
    _vMax(20),
    _zLevel(0),
    _state(Route::RS_Available),
    _exhaused(false)
{

}

//!
//! \brief Route::Route
//! \param r
//!
Route::Route(Route *r) :
    Route (r->parent())
{
    this->setZLevel(r->zLevel());
    this->setVMax(r->vMax());
    foreach (Object *element, r->elements())
    {
        if (element->type() == Object::OT_Crossover)
        {
            Crossover *co = (Crossover *) element;
            this->pushToRoute(co, r->crossoverSettings(co));
        }
        else
        {
            this->pushToRoute(element);
        }
    }
}

//!
//! \brief Route::~Route
//!
Route::~Route()
{

}

//!
//! \brief Route::pushToRoute
//! \param obj
//!
void Route::pushToRoute(Object *obj)
{
    this->_elements.append(obj);
    if (obj->type() == Object::OT_Rail)
    {
        int zlevel = ((Rail* ) obj)->zLevel();
        this->_zLevel = (this->_zLevel < zlevel) ? zlevel
                                                 : this->_zLevel;
    }
}

//!
//! \brief Route::pushToRoute
//! \param c
//! \param state
//!
void Route::pushToRoute(Crossover *c, Crossover::CrossoverStates state)
{
    this->connect(c,               SIGNAL(stateChanged(Crossover::CrossoverStates)),
                  this,             SLOT(crossoverStateChanged()));

    this->_crossoversSettings.insert(c, state);
    this->_elements.append(c);
}

//!
//! \brief Route::state
//! \return
//!
Route::RouteStates Route::state() const
{
    return this->_state;
}

//!
//! \brief Route::zLevel
//! \return
//!
int Route::zLevel() const
{
    return this->_zLevel;
}

//!
//! \brief Route::vMax
//! \return
//!
int Route::vMax() const
{
    Object *object = this->_elements.first();
    if (object->type() == Object::OT_Semaphore)
        return ((Semaphore*) object)->vMax();
    return this->_vMax;
}

//!
//! \brief Route::path
//! \return
//!
QList<QPointF> Route::path() const
{
    return this->_path;
}

//!
//! \brief Route::painterPath
//! \return
//!
QPainterPath &Route::painterPath()
{
    return this->_painterPath;
}

//!
//! \brief Route::createPath
//!
void Route::createPath()
{
    this->_path.clear();
    Object *firstElement = this->_elements.first();
    QPointF beginPoint;
    bool skip = false;

    if (firstElement->type() == Object::OT_Terminator)
    {
        beginPoint = ((Terminator*) firstElement)->branchNodePosition() + firstElement->position();
        skip = true;
    }
    else
        beginPoint = ((Semaphore*) firstElement)->firstNodePosition() + firstElement->position();

    this->_path.push_back(beginPoint);

    foreach (Object *element, this->_elements)
    {
        if (skip || element == this->_elements.last())
        {
            skip = false;
            continue;
        }

        QPointF lastPosition = this->_path.last();
        QPointF itemPosition = element->position();
        switch (element->type())
        {
        case Object::OT_Terminator:
            this->_path.push_back(((Terminator*) element)->branchNodePosition() + itemPosition);
            break;
        case Object::OT_Rail:
        case Object::OT_Station:
        {
            QPointF p1 = ((Rail*) element)->firstNodePosition() + itemPosition;
            QPointF p2 = ((Rail*) element)->secondNodePosition() + itemPosition;

            if (qPointFFuzzyCompare(&p1, &lastPosition))
                this->_path.push_back(p2);
            else
                this->_path.push_back(p1);
        }
            break;
        case Object::OT_Crossover:
        {
            QPointF p1 = ((Crossover*) element)->firstNodePosition() + itemPosition;
            QPointF p2 = ((Crossover*) element)->secondNodePosition() + itemPosition;
            QPointF p3 = ((Crossover*) element)->branchNodePosition() + itemPosition;

            Crossover::CrossoverStates crossoverSetting = this->_crossoversSettings[(Crossover*) element];

            if (qPointFFuzzyCompare(&p1, &lastPosition))
            {
                if (crossoverSetting == Crossover::CS_Branch)
                    this->_path.push_back(p3);
                else
                    this->_path.push_back(p2);
            }
            else
                this->_path.push_back(p1);
        }
            break;
        case Object::OT_Semaphore:
        {
            QPointF p1 = ((Semaphore*) element)->firstNodePosition() + itemPosition;
            QPointF p2 = ((Semaphore*) element)->secondNodePosition() + itemPosition;

            if (qPointFFuzzyCompare(&p1, &lastPosition))
                this->_path.push_back(p2);
            else
                this->_path.push_back(p1);
        }
            break;
        default:
            break;
        }
    }

    QPolygonF polygon;
    foreach (QPointF point, this->_path)
    {
        polygon << point;
    }
    QPainterPath path;
    path.addPolygon(polygon);

    this->_painterPath = path;

    this->_beginPoint = this->_path.first();
    this->_endPoint = this->_path.last();
}

//!
//! \brief Route::qPointFFuzzyCompare
//! \param first
//! \param second
//! \return
//!
bool Route::qPointFFuzzyCompare(QPointF *first, QPointF *second)
{
    if (qFuzzyCompare(first->rx(), second->rx()))
        if (qFuzzyCompare(first->ry(), second->ry()))
            return true;

    return false;
}

//!
//! \brief Route::setExhaused
//! \param exhaused
//!
void Route::setExhaused(bool exhaused)
{
    foreach (Object *obj, this->_elements)
    {
        obj->setIsExhaused(exhaused);
    }
    this->_exhaused = exhaused;
}

//!
//! \brief Route::exhaused
//! \return
//!
bool Route::exhaused() const
{
    return this->_exhaused;
}

//!
//! \brief Route::setZLevel
//! \param zLevel
//!
void Route::setZLevel(int zLevel)
{
    this->_zLevel = zLevel;
}

//!
//! \brief Route::setVMax
//! \param vMax
//!
void Route::setVMax(int vMax)
{
    this->_vMax = vMax;
}

//!
//! \brief Route::setState
//! \param state
//!
void Route::setState(const Route::RouteStates &state)
{
    if (this->_state != state)
    {
        this->_state = state;
        emit this->stateChanged(state);
    }
}

//!
//! \brief Route::setActive
//!
void Route::setActive()
{
    foreach (Object *obj, this->_elements)
    {
        if (obj == this->_elements.first() || obj == this->_elements.last())
            continue;

        if (obj->isExhaused())
        {
            qDebug() << "Exhaused" << obj << this->_elements.indexOf(obj);
            return;
        }
    }

    foreach (Crossover *co, this->_crossoversSettings.keys())
    {
        co->setState(this->_crossoversSettings[co]);
    }

    Object *first = this->_elements.first();
    if (first->type() == Object::OT_Semaphore)
    {
        Semaphore *s = (Semaphore*) first;
        s->setState(Semaphore::SS_Green);
    }
}

//!
//! \brief Route::crossoverStateChanged
//!
void Route::crossoverStateChanged()
{
    Crossover *co = qobject_cast<Crossover*>(this->sender());

    if (!co)
        return;

    if (!this->_crossoversSettings.contains(co))
        return;

    if (co->state() != this->_crossoversSettings[co])
    {
        if (this->_state == RS_Available)
        {
            this->setState(RS_Unavailable);

            Semaphore *s = qobject_cast<Semaphore*>(this->_elements.first());
            if (s)
                s->setState(Semaphore::SS_Yellow);
        }
    }
    else
    {
        if (this->_state == RS_Unavailable)
        {
            foreach(Crossover *iterator, this->_crossoversSettings.keys())
            {
                if (this->_crossoversSettings[iterator] != iterator->state())
                    return;
            }
            this->setState(RS_Available);
        }
    }
}

//!
//! \brief Route::beginPoint
//! \return
//!
QPointF Route::beginPoint() const
{
    return this->_beginPoint;
}

//!
//! \brief Route::endPoint
//! \return
//!
QPointF Route::endPoint() const
{
    return this->_endPoint;
}

//!
//! \brief Route::elements
//! \return
//!
QList<Object *> Route::elements() const
{
    return this->_elements;
}

//!
//! \brief Route::setElements
//! \param elements
//!
void Route::setElements(const QList<Object *> &elements)
{
    this->_elements = elements;
}

//!
//! \brief Route::crossoverSettings
//! \param c
//! \return
//!
Crossover::CrossoverStates Route::crossoverSettings(Crossover *c) const
{
    return this->_crossoversSettings[c];
}

//!
//! \brief Route::initState
//!
void Route::initState()
{
    this->_state = RS_Unavailable;
    foreach(Crossover *iterator, this->_crossoversSettings.keys())
    {
        if (this->_crossoversSettings[iterator] != iterator->state())
            return;
    }

    this->_state = RS_Available;
}

