#ifndef BALISA_H
#define BALISA_H

#include "object.h"

//!
//! \brief The Balisa class
//!
class Balisa : public Object
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit Balisa(Object *parent = 0);
    virtual ~Balisa();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    virtual QStringList keys() const;

};

#endif // BALISA_H
