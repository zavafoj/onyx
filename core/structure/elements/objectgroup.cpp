#include "elements.h"

//!
//! \brief ObjectGroup::ObjectGroup
//! \param parent
//!
ObjectGroup::ObjectGroup(Object *parent):
    Object(parent)
{
    this->_objType = OT_Group;
}

//!
//! \brief ObjectGroup::~ObjectGroup
//!
ObjectGroup::~ObjectGroup()
{
//    qDebug() << Q_FUNC_INFO;
}

//!
//! \brief ObjectGroup::read Odczyt metodą strumieniowania
//! \param stream Strumień danych
//!
void ObjectGroup::read(QDataStream &stream)
{
    // Uzyskaj wskaźnik na element główny - obiekt klasy RootItem
    RootItem *root = (RootItem*) this->parentItem();

    // Wczytaj z bufora typ obiektu
    int objType;
    stream >> objType;

    // Wczytaj z bufora liczbę dzieci
    int childCount;
    stream >> childCount;

    // Dla każdego dziecka hipotetycznie znajdującego się w strumieniu:
    for (int i = 0; i < childCount; ++i)
    {
        // Wczytaj z bufora typ obiektu
        int type;
        stream >> type;

        // Spróbuj utworzyć obiekt z wczytanego typu korzystając z metody fabrykującej
        Object *createdObject = this->createObjectByType((Object::ObjectType) type);

        // Jeżeli obiekt został stworzony (wskaźnik nie ma przypisanego adresu 0x0)
        if (createdObject)
        {
            // Wczytaj konfigurację z bufora do nowopowstałego obiektu
            createdObject->read(stream);
            // Zasygnalizuj utworzenie elementu do obiektu nadrzędnego
            root->loadElement(createdObject);
        }
    }
}

//!
//! \brief ObjectGroup::write
//! \param stream
//!
void ObjectGroup::write(QDataStream &stream) const
{
    stream << this->_objType
           << this->childCount();

    foreach (Object *child, this->_childrenItems)
    {
        child->write(stream);
    }
}

//!
//! \brief ObjectGroup::readXML
//! \param fragment
//!
void ObjectGroup::readXML(QDomElement &fragment)
{
    RootItem *root = (RootItem*) this->parentItem();

    try
    {
        int children = fragment.childNodes().count();
        for (int i = 0; i < children; i++)
        {
            QDomElement element = fragment.childNodes().at(i).toElement();
            Object::ObjectType type = Object::getObjectTypeFromString(element.nodeName());
            Object *createdObject = this->createObjectByType(type);

            if (createdObject)
            {
                createdObject->readXML(element);
                root->loadElement(createdObject);
            }
        }
    }
    catch(...)
    {
        throw "Unable to read";
    }
}

//!
//! \brief ObjectGroup::writeXML
//! \param stream
//!
void ObjectGroup::writeXML(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Group");
    stream.writeAttribute("Name", this->objectName());
    Object* childIterator;
    foreach (childIterator, this->childrenItems())
    {
        childIterator->writeXML(stream);
    }
    stream.writeEndElement();
}

//!
//! \brief ObjectGroup::keys
//! \return
//!
QStringList ObjectGroup::keys() const
{
    return QStringList();
}

//!
//! \brief ObjectGroup::createObjectByType Metoda tworząca obiekt konkretnego typu
//! \param type
//!
Object* ObjectGroup::createObjectByType(Object::ObjectType type)
{
    // Deklaracja wskaźnika klasy abstrakcyjnej Object na adres 0x0
    Object *object = nullptr;
    // W zależności od argumentu utwórz przypisany objekt.
    // Nowopowstałe obiekty są 'co najmniej' typem klasy abstrakcyjnej Object,
    // więc istnieje możliwość wykorzystania mechanizmu polimorfizmu.
    switch (type)
    {
    case Object::OT_Rail:
        object = new Rail(this);
        break;
    case Object::OT_Crossover:
        object = new Crossover(this);
        break;
    case Object::OT_Semaphore:
        object = new Semaphore(this);
        break;
    case Object::OT_Terminator:
        object = new Terminator(this);
        break;
    case Object::OT_Station:
        object = new Station(this);
        break;
    case Object::OT_Balisa:
        object = new Balisa(this);
        break;
    case Object::OT_Train:
        object = new Train(this);
        break;
    default:
        break;
    }
    // Jako wartość wyjściową zwróć wskaźnik na nowopowstały obiekt lub adres 0x0
    return object;
}
