#ifndef RAIL_H
#define RAIL_H

#include "object.h"

//!
//! \brief The Rail class
//!
class Rail : public Object
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")
    Q_PROPERTY(QPoint FirstNodePosition READ firstNodePosition WRITE setFirstNodePosition NOTIFY firstNodePositionChanged)
    Q_PROPERTY(QPoint SecondNodePosition READ secondNodePosition WRITE setSecondNodePosition NOTIFY secondNodePositionChanged)
    Q_PROPERTY(int zLevel READ zLevel WRITE setZLevel NOTIFY zLevelChanged)

public:
    explicit Rail(Object *parent = 0);
    virtual ~Rail();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    virtual QStringList keys() const;

    QPoint firstNodePosition() const;
    QPoint secondNodePosition() const;

    int zLevel() const;

signals:
    void firstNodePositionChanged(const QPoint &position);
    void secondNodePositionChanged(const QPoint &position);
    void zLevelChanged(int zLevel);

public slots:
    void setFirstNodePosition(const QPoint &firstNodePosition);
    void setSecondNodePosition(const QPoint &secondNodePosition);
    void setZLevel(int zLevel);

protected:
    QPoint _firstNodePosition;
    QPoint _secondNodePosition;
    int _zLevel;
};

#endif // RAIL_H
