#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include "rail.h"

//!
//! \brief The Semaphore class
//!
class Semaphore : public Rail
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")
    Q_ENUMS (SemaphoreStates)
    Q_PROPERTY(SemaphoreStates State READ state WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(int vMax READ vMax WRITE setVMax NOTIFY vMaxChanged)

public:
    enum SemaphoreStates
    {
        SS_Green           =       0x00,
        SS_Yellow          =       0x01,
        SS_Red             =       0x02,
        SS_Unknown         =         -1
    };

    explicit Semaphore(Object *parent = 0);
    virtual ~Semaphore();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    virtual QStringList keys() const;

    SemaphoreStates state() const;
    int vMax() const;

signals:
    //!
    //! \brief stateChanged
    //! \param state
    //!
    void stateChanged(SemaphoreStates state);
    //!
    //! \brief vMaxChanged
    //! \param vMax
    //!
    void vMaxChanged(int vMax);

public slots:
    void setState(const SemaphoreStates &state);
    void setVMax(int vMax);

private:
    SemaphoreStates _state;
    int _vMax;
};

#endif // SEMAPHORE_H
