#ifndef ROOTITEM_H
#define ROOTITEM_H

#include "objectgroup.h"
#include "elements.h"

//!
//! \brief The RootItem class
//!
class RootItem : public Object
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit RootItem(Object *parent = 0);
    virtual ~RootItem();

    virtual void read(QDataStream &stream);
    virtual void write(QDataStream &stream) const;

    virtual void readXML(QDomElement &fragment);
    virtual void writeXML(QXmlStreamWriter &stream) const;

    ObjectGroup *railsGroup() const;
    ObjectGroup *crossoversGroup() const;
    ObjectGroup *semaphoresGroup() const;
    ObjectGroup *balisesGroup() const;
    ObjectGroup *terminatorsGroup() const;
    ObjectGroup *stationsGroup() const;
    ObjectGroup *trainsGroup() const;

    virtual QStringList keys() const;

    Object *createElement(Object::ObjectType type, const QPointF &pos);

    void clear();

signals:
    void elementLoaded(Object *object);

public slots:
    void loadElement(Object *object);

protected slots:
    void retranslate();

private:
    ObjectGroup *_railsGroup;
    ObjectGroup *_crossoversGroup;
    ObjectGroup *_semaphoresGroup;
    ObjectGroup *_balisesGroup;
    ObjectGroup *_terminatorsGroup;
    ObjectGroup *_stationsGroup;
    ObjectGroup *_trainsGroup;
};

#endif // ROOTITEM_H
