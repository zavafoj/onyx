#ifndef NODE_H
#define NODE_H

#include <QObject>

class Node : public QObject
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit Node(QObject *parent = 0);

signals:

public slots:

};

#endif // NODE_H
