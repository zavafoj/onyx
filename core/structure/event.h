#ifndef EVENT_H
#define EVENT_H

#include <QObject>

//!
//! \brief The Event class
//!
class Event : public QObject
{
    Q_OBJECT
    Q_CLASSINFO ("Author", "Wojciech Ossowski")

public:
    explicit Event(QObject *parent = 0);

signals:

public slots:

};

#endif // EVENT_H
