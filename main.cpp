#include "mainwindow.h"
#include <QtSingleApplication>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QDir>
#include <QDebug>
#include <QSplashScreen>

void showSplash(MainWindow &w, QApplication &a)
{
    // animation
    QPixmap splashImg(":/gfx/images/Logo_KSDiR_rgb.png");
    QSplashScreen splash(splashImg);
    splash.setWindowOpacity(0.0);
    splash.setFont(QFont("Courrier", 10, 3));
    splash.show();

    QPropertyAnimation *pa = new QPropertyAnimation(&splash, "windowOpacity", &w);

    pa->setStartValue(0.0);
    pa->setEndValue(1.0);
    pa->setEasingCurve(QEasingCurve::Linear);
    pa->setDuration(300);
    pa->start(QAbstractAnimation::DeleteWhenStopped);


    pa = new QPropertyAnimation(&splash, "windowOpacity", &w);

    QTimer *timer = new QTimer;
    timer->start(2500);
    while (timer->remainingTime() > 0)
    {
        a.processEvents();
    }
    timer->deleteLater();

    pa->setStartValue(1.0);
    pa->setEndValue(0.0);
    pa->setEasingCurve(QEasingCurve::Linear);
    pa->setDuration(300);
    pa->start(QAbstractAnimation::DeleteWhenStopped);


    w.setWindowOpacity(0.0);
    w.show();

    pa = new QPropertyAnimation(&w, "windowOpacity", &w);

    pa->setStartValue(0.0);
    pa->setEndValue(1.0);
    pa->setEasingCurve(QEasingCurve::Linear);
    pa->setDuration(500);
    pa->start(QAbstractAnimation::DeleteWhenStopped);
    splash.finish(&w);
    // end animation
}

int main(int argc, char *argv[])
{
    QtSingleApplication a(argc, argv);
    QStringList args =  a.arguments();

    bool forceRun = args.contains("-force", Qt::CaseInsensitive) || args.contains("-f", Qt::CaseInsensitive);
//    bool trayRun = args.contains("-tray", Qt::CaseInsensitive);

    if (a.isRunning() && !forceRun)
    {
        a.sendMessage("show");
        return 0;
    }

    qRegisterMetaType<Object::LogEntry>("LogEntry");

    QDesktopWidget desktop;
    QRect window = desktop.availableGeometry();
    int width = 0.7*window.width();
    int height = 0.7*window.height();
    int xOffset = 0.5*(window.width() - width);
    int yOffset = 0.5*(window.height() - height);

    qApp->setApplicationName("Onyx");
    qApp->setApplicationVersion("0.1.0");
    qApp->setApplicationDisplayName("Onyx PKM Simulator");
    qApp->setOrganizationName("Wololo DevTeam");

    QDir configDir = qApp->applicationDirPath() + "/configurations";
    if(!configDir.exists()) configDir.mkpath(configDir.absolutePath());

    QDir presetsDir = qApp->applicationDirPath() + "/scripts";
    if(!presetsDir.exists()) presetsDir.mkpath(presetsDir.absolutePath());

    QDir mapDir = qApp->applicationDirPath() + "/map";
    if(!mapDir.exists()) mapDir.mkpath(mapDir.absolutePath());

    MainWindow w;
    w.setGeometry(xOffset, yOffset, width, height);
    showSplash(w, a);
    w.loadGlobalSettings();
    w.show();

    return a.exec();
}
