#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMessageBox>
#include <QApplication>
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QFontDialog>
#include <QFileInfo>
#include <QFile>
#include <QFileDialog>
#include <QDir>
#include <QInputDialog>

//!
//! \brief MainWindow::MainWindow
//! \param parent
//!
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _fullscreen(false)
{
    ui->setupUi(this);

    this->_rootItem = new RootItem();
    this->_model = new ObjectItemModel(this->_rootItem, this->ui->treeView);
    this->ui->treeView->setModel(this->_model);
    this->ui->graphicsView->setRootItem(this->_rootItem);

    this->connect(this->_rootItem,          SIGNAL(childRemoved()),
                  this->ui->treeView,       SLOT(reset()));

    this->_mapEditor = new MapEditor(this);
    this->_mapEditor->setWindowFlags(Qt::Window);

    // about windows
    this->connect(this->ui->actionAbout_Qt,                 SIGNAL(triggered()),
                  qApp,                                     SLOT(aboutQt()));

    this->connect(this->ui->actionAbout,                    SIGNAL(triggered()),
                  this,                                     SLOT(showAbout()));

    // loading stylesheets
    this->connect(this->ui->actionDark,                     SIGNAL(triggered()),
                  this,                                     SLOT(stylesheetLoadRequested()));

    this->connect(this->ui->actionLight,                    SIGNAL(triggered()),
                  this,                                     SLOT(stylesheetLoadRequested()));

    // Scaling
    this->connect(this->ui->actionZoomIn,                   SIGNAL(triggered()),
                  this,                                     SLOT(zoomIn()));

    this->connect(this->ui->actionZoomOut,                  SIGNAL(triggered()),
                  this,                                     SLOT(zoomOut()));

    this->connect(this->ui->actionFit_In_View,              SIGNAL(triggered()),
                  this->ui->graphicsView,                   SLOT(reset()));

    // Simulation part
    this->connect(this->ui->actionStart_Simulation,         SIGNAL(triggered()),
                  this,                                     SLOT(startSimulationClicked()));

    this->connect(this->ui->actionStop_Simulation,          SIGNAL(triggered()),
                  this,                                     SLOT(stopSimulationClicked()));

    GraphicsScene *scene = this->ui->graphicsView->getScene();
    this->connect(this->ui->treeView,                       SIGNAL(itemSelected(Object*)),
                  scene,                                    SLOT(hoverObject(Object*)));

    this->connect(scene,                                    SIGNAL(stopDemanded()),
                  this,                                     SLOT(stopSimulationClicked()));

    this->connect(this->ui->graphicsView,                   SIGNAL(objectSelected(Object*)),
                  this->ui->objectProperties,               SLOT(setCurrentObject(Object*)));

    this->ui->objectProperties->setIsLocked(true);

    this->connect(this->ui->graphicsView,                   SIGNAL(objectSelected(Object*)),
                  this->ui->messages,                       SLOT(setCurrentObject(Object*)));

    this->connect(this->ui->actionSimulation_Tick,          SIGNAL(triggered()),
                  this,                                     SLOT(setSimulationTick()));

    this->_simulationTimer = new QTimer(this);
    this->_simulationTimer->setInterval(100);
    this->connect(this->_simulationTimer,   SIGNAL(timeout()),
                  scene,                    SLOT(advance()));

    this->connect(this->_simulationTimer,   SIGNAL(timeout()),
                  this,                     SLOT(updateTimer()));

    this->ui->simulationTime->setContextMenuPolicy(Qt::CustomContextMenu);

    this->connect(this->ui->simulationTime, SIGNAL(customContextMenuRequested(QPoint)),
                  this,                     SLOT(setTime()));

    this->_console = new ConsoleWidget(this);
    this->_console->setWindowFlags(Qt::Window);
    this->_console->setRootItem(this->_rootItem);

    this->connect(this->ui->actionShow_Console,             SIGNAL(triggered()),
                  this->_console,                           SLOT(showNormal()));

    // Map Editor
    this->connect(this->ui->actionOpen_MapEditor,           SIGNAL(triggered()),
                  this,                                     SLOT(showMapEditor()));

    this->connect(this->_mapEditor,                         SIGNAL(copyDataRequested()),
                  this,                                     SLOT(loadMapFromEditor()));

    this->connect(this->_mapEditor,                         SIGNAL(hidden()),
                  this,                                     SLOT(hideMapEditor()));

    // Main Menu
    this->connect(this->ui->actionClose,                    SIGNAL(triggered()),
                  this,                                     SLOT(close()));

    this->connect(this->ui->actionNew_Configuration,        SIGNAL(triggered()),
                  this,                                     SLOT(clearConfiguration()));

    this->connect(this->ui->actionSave_Configuration,       SIGNAL(triggered()),
                  this,                                     SLOT(saveConfiguration()));

    this->connect(this->ui->actionLoad_Configuration,       SIGNAL(triggered()),
                  this,                                     SLOT(loadConfiguration()));

    // Settings Menu
    this->connect(this->ui->actionFont,                     SIGNAL(triggered()),
                  this,                                     SLOT(setFontClicked()));

    this->installTranslations();
    this->ui->simulationTime->setTime(Object::simulationTime);
}

//!
//! \brief MainWindow::~MainWindow
//!
MainWindow::~MainWindow()
{
    delete ui;
    this->_rootItem->deleteLater();
}

//!
//! \brief MainWindow::messageReceived
//! \param message
//!
void MainWindow::messageReceived(const QString &message)
{
    qDebug() << Q_FUNC_INFO << message;

    if (message == "show")
    {
        this->show();
        this->activateWindow();

        QMessageBox b(this);
        b.setWindowTitle(tr("Information"));
        b.setIcon(QMessageBox::Information);
        b.setText(tr("%1 is currently running.").arg(qApp->applicationName()));
        b.addButton(tr("Ok"),QMessageBox::AcceptRole);
        b.exec();
    }
}

//!
//! \brief MainWindow::installTranslations
//!
void MainWindow::installTranslations()
{
    QDir directory(qApp->applicationDirPath() + "/translations");
    QStringList translations = directory.entryList(QStringList("*.qm"));
    foreach (QString translationString, translations)
    {
        QLocale languageName(translationString);
        QAction *language = this->ui->menuLanguage->addAction(languageName.nativeLanguageName());
        language->setData(translationString);
        this->connect(language,             SIGNAL(triggered()),
                      this,                 SLOT(translationChangeClicked()));
    }
}

//!
//! \brief MainWindow::loadTranslation
//! \param translationFileName
//!
void MainWindow::loadTranslation(const QString &translationFileName)
{
    static QTranslator* translator = 0;

    if (translator)
    {
        qApp->removeTranslator(translator);
        delete translator;
    }

    translator = new QTranslator(0);

    if (translator->load(translationFileName))
    {
        qApp->installTranslator(translator);
        this->update();
        this->updateGlobalSetting("Translation", translationFileName);
    }
    else
    {
        QMessageBox b;
        b.setWindowTitle(tr("Information"));
        b.setIcon(QMessageBox::Information);
        b.setText(tr("Unable to load \n\"%1\"").arg(translationFileName));
        b.addButton(tr("Ok"),QMessageBox::AcceptRole);
        b.exec();
    }
}

//!
//! \brief MainWindow::loadGlobalSettings
//!
void MainWindow::loadGlobalSettings()
{
    QSettings mainSettings(qApp->applicationName() + ".ini", QSettings::IniFormat);
    mainSettings.beginGroup(qApp->applicationName());
    QStringList globalParameters = mainSettings.childKeys();
    foreach (QString parameter, globalParameters)
    {
        if (parameter == "Stylesheet")
        {
            this->loadStylesheet(mainSettings.value(parameter).toString());
        }
        else if (parameter == "Translation")
        {
            this->loadTranslation(mainSettings.value(parameter).toString());
        }
        else if (parameter == "Font")
        {
            QFont theFont = qvariant_cast<QFont>(mainSettings.value(parameter));
            qApp->setFont(theFont);
        }
    }
}

//!
//! \brief MainWindow::setFullScreen
//! \param fullscreen
//!
void MainWindow::setFullScreen(bool fullscreen)
{
    fullscreen ? this->showFullScreen() : this->showNormal();
    this->_fullscreen = fullscreen;
}

//!
//! \brief MainWindow::loadStylesheet
//!
void MainWindow::loadStylesheet(const QString &fileName)
{
    if (fileName.isEmpty())
    {
        this->setStyleSheet(fileName);
    }
    else
    {
        try
        {
            QFile file(fileName);
            if (file.open(QIODevice::ReadOnly))
            {
                QTextStream stream(&file);
                QString stylesheet = stream.readAll();
                file.close();
                this->setStyleSheet(stylesheet);
            }
            else
            {
                throw &fileName;
            }
        }
        catch(QString &s)
        {
            QMessageBox b(this);
            b.setWindowTitle(tr("Error"));
            b.setIcon(QMessageBox::Critical);
            b.setText(tr("Unable to open<br><b>%1</b>").arg(s));
            b.addButton(tr("Ok"),QMessageBox::AcceptRole);
            b.exec();
            return;
        }
        catch(...)
        {
            QMessageBox b(this);
            b.setWindowTitle(tr("Error"));
            b.setIcon(QMessageBox::Critical);
            b.setText(tr("An unexpected error has occured."));
            b.addButton(tr("Ok"),QMessageBox::AcceptRole);
            b.exec();
            return;
        }
    }

    this->updateGlobalSetting("Stylesheet", fileName);
}

//!
//! \brief MainWindow::stylesheetLoadRequested
//!
void MainWindow::stylesheetLoadRequested()
{
    QObject *sender = this->sender();
    if (sender == this->ui->actionDark)
    {
        QString currentStylesheet = qApp->applicationDirPath() + "/stylesheets/dark.css";

        this->loadStylesheet(currentStylesheet);
    }
    else if (sender == this->ui->actionLight)
    {
        this->loadStylesheet(QString());
    }
    else
    {
        QMessageBox b(this);
        b.setWindowTitle(tr("Error"));
        b.setIcon(QMessageBox::Critical);
        b.setText(tr("An unexpected error has occured."));
        b.addButton(tr("Ok"),QMessageBox::AcceptRole);
        b.exec();
    }
}

//!
//! \brief MainWindow::showAbout
//!
void MainWindow::showAbout()
{
    QMessageBox b(this);
    b.setWindowTitle(tr("About"));
    b.setIconPixmap(QPixmap(":/gfx/images/LogoIcon.png"));
    b.setText(tr("Onyx PKM Simulator\n"
                 "Created for Gdansk University of Technology,\n"
                 "Department of Decissions Systems & Robotics\n"
                 "\n"
                 "by Wojciech Ossowski, w.j.ossowski@gmail.com"));

    b.addButton(tr("Close"),QMessageBox::AcceptRole);
    b.exec();
}

//!
//! \brief MainWindow::zoomIn
//!
void MainWindow::zoomIn()
{
    qreal scaleFactor = 2;
    qreal factor = this->ui->graphicsView->transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if(0.0005 < factor && factor < 100)
        this->ui->graphicsView->scale(scaleFactor, scaleFactor);
}

//!
//! \brief MainWindow::zoomOut
//!
void MainWindow::zoomOut()
{
    qreal scaleFactor = 0.5;
    qreal factor = this->ui->graphicsView->transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if(0.0005 < factor && factor < 100)
        this->ui->graphicsView->scale(scaleFactor, scaleFactor);
}

//!
//! \brief MainWindow::showMapEditor
//!
void MainWindow::showMapEditor()
{
    this->_mapEditor->show();

    this->setOptionsEnabled(false);
}

//!
//! \brief MainWindow::hideMapEditor
//!
void MainWindow::hideMapEditor()
{
    this->setOptionsEnabled(true);
}

//!
//! \brief MainWindow::clearConfiguration
//! \param ask
//!
void MainWindow::clearConfiguration(bool ask)
{
    if (ask)
    {
        QMessageBox b;
        b.setWindowTitle(tr("Question"));
        b.setText(tr("Are you sure to create new configuration?"));
        b.addButton(tr("Yes"),QMessageBox::AcceptRole);
        b.addButton(tr("No"),QMessageBox::RejectRole);

        int ret = b.exec();
        if (ret != (int) QMessageBox::Rejected)
        {
            return;
        }
    }

    this->_mapEditor->clear();
    this->ui->graphicsView->scene()->clear();
    this->ui->messages->setCurrentObject(nullptr);
    this->ui->messages->clear();
}

//!
//! \brief MainWindow::saveConfiguration
//!
void MainWindow::saveConfiguration()
{
    QString selectedFilter;
    const QString fileName = QFileDialog::getSaveFileName(this,
                                                          tr("Save config as..."),
                                                          qApp->applicationDirPath() + "/Configurations",
                                                          tr("Binary files(*.pkm);;XML files (*.xml)"),
                                                          &selectedFilter);

    if (fileName.isEmpty())
        return;

    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly))
    {
        if (selectedFilter.startsWith('X'))
        {
            QXmlStreamWriter stream(&file);
            stream.setAutoFormatting(true);
            stream.writeStartDocument();
            this->_mapEditor->rootItem()->writeXML(stream);
            stream.writeEndDocument();
        }
        else
        {
            QDataStream stream (&file);
            this->_mapEditor->rootItem()->write(stream);
        }
        file.close();
        this->setWindowFilePath(fileName);
    }
}

//!
//! \brief MainWindow::loadConfiguration
//!
void MainWindow::loadConfiguration()
{
    QString selectedFilter;
    const QString fileName = QFileDialog::getOpenFileName(this,
                                                          tr("Open config..."),
                                                          qApp->applicationDirPath() + "/Configurations",
                                                          tr("Binary files(*.pkm);;XML files (*.xml)"),
                                                          &selectedFilter);

    if (fileName.isEmpty())
        return;

    QFile file(fileName);
    try
    {
        if (file.open(QIODevice::ReadOnly))
        {
            this->clearConfiguration(false);
            if (selectedFilter.startsWith('X'))
            {
                QDomDocument doc;
                if (!doc.setContent(&file))
                {
                    throw "Unable to read XML";
                }
                QDomElement element = doc.firstChildElement("Root");
                this->_mapEditor->rootItem()->readXML(element);
            }
            else
            {
                QDataStream stream (&file);

                this->_mapEditor->rootItem()->read(stream);
            }
        }
    }
    catch(...)
    {
        if (file.isOpen())
            file.close();

        this->handleOnReadException(fileName);
    }
}

//!
//! \brief MainWindow::handleOnReadException
//! \param fileName
//!
void MainWindow::handleOnReadException(const QString fileName)
{
    QMessageBox b;
    b.setWindowTitle(tr("Error"));
    b.setIcon(QMessageBox::Critical);
    b.setText(tr("File<br><b>\"%1\"</b><br>has invalid configuration.").arg(fileName));
    b.addButton(tr("Ok"),QMessageBox::AcceptRole);
    b.exec();

    this->setWindowFilePath(QString());
}

//!
//! \brief MainWindow::loadMapFromEditor metoda przenosi ustawienia mapy edytora do moduły symulacji
//!
void MainWindow::loadMapFromEditor()
{
    GraphicsScene *scene = this->ui->graphicsView->getScene();
    scene->setLocked(false);
    scene->hideNodePoints(false);
    scene->clear();

    QByteArray array;
    QDataStream writeStream(&array, QIODevice::WriteOnly);
    this->_mapEditor->rootItem()->write(writeStream);

    QDataStream readStream(&array, QIODevice::ReadOnly);
    this->_rootItem->read(readStream);

    // TODO: routing
    qApp->processEvents();
    scene->lock();
    scene->createRoutes();
    scene->hideNodePoints();
    scene->setLocked(true);
}

//!
//! \brief MainWindow::startSimulationClicked zdarzenie kliknięcia na start symulacji
//!
void MainWindow::startSimulationClicked()
{
    if (this->ui->actionStart_Simulation->isChecked())
        this->_simulationTimer->start();
    else
        this->_simulationTimer->stop();
}

//!
//! \brief MainWindow::stopSimulationClicked zdarzenie kliknięcia na stop symulacji
//!
void MainWindow::stopSimulationClicked()
{
    this->ui->actionStart_Simulation->setChecked(false);
    this->_simulationTimer->stop();
}

//!
//! \brief MainWindow::setFontClicked ładowanie czcionki
//!
void MainWindow::setFontClicked()
{
    QFontDialog d (this);
    qDebug() << qApp->font().family();
    d.setCurrentFont(qApp->font());
    if (d.exec() == QDialog::Accepted)
    {
        qApp->setFont(d.selectedFont());
        this->updateGlobalSetting("Font", d.selectedFont());
    }
}

//!
//! \brief MainWindow::translationChangeClicked ładowanie tłumaczeń
//!
void MainWindow::translationChangeClicked()
{
    QAction *selectedTranslation = qobject_cast<QAction *>(this->sender());
    if (selectedTranslation)
    {
        const QString translationPath = QString("%1/translations/%2").arg(qApp->applicationDirPath())
                .arg(selectedTranslation->data().toString());

        this->loadTranslation(translationPath);
    }
}

//!
//! \brief MainWindow::setSimulationTick pokazuje okno zmiany interwału symulacji
//!
void MainWindow::setSimulationTick()
{
    bool confirmed;
    int newInterval = QInputDialog::getInt(this,
                                           tr("Simulation Timer Interval"),
                                           tr("Insert new Simulation Timer Interval"),
                                           this->_simulationTimer->interval(),
                                           20, 5000, 1, &confirmed);

    if (confirmed)
    {
        this->_simulationTimer->setInterval(newInterval);
    }
}

//!
//! \brief MainWindow::updateTimer aktualizacja timera
//!
void MainWindow::updateTimer()
{
    Object::simulationTime = Object::simulationTime.addSecs(1);

    this->ui->simulationTime->setTime(Object::simulationTime);
}

//!
//! \brief MainWindow::setTime
//!
void MainWindow::setTime()
{
    if (!this->_simulationTimer->isActive())
    {
        QDialog d (this);
        d.setWindowTitle(tr("New Simulation Time"));
        QVBoxLayout layout (&d);

        QTimeEdit edit (&d);
        edit.setDisplayFormat("HH:mm:ss");
        edit.setTime(Object::simulationTime);
        layout.addWidget(&edit);

        QPushButton pb (&d);
        pb.setText(tr("Set New Simulation Time"));
        d.connect(&pb,      SIGNAL(clicked()),
                  &d,       SLOT(accept()));
        layout.addWidget(&pb);

        d.setLayout(&layout);

        if (d.exec())
        {
            Object::simulationTime = edit.time();
            this->ui->simulationTime->setTime(Object::simulationTime);
        }
    }
}

//!
//! \brief MainWindow::closeEvent zdarzenie wyjścia z programu
//! \param event QCloseEvent zdarzenie zamknięcia
//!
void MainWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();

    QMessageBox b(this);
    b.setWindowTitle(tr("Information"));
    b.setIcon(QMessageBox::Question);
    b.setText(tr("Are you sure to exit %1?").arg(qApp->applicationName()));
    b.addButton(tr("Ok"),QMessageBox::AcceptRole);
    b.addButton(tr("Cancel"),QMessageBox::RejectRole);
    int ret = b.exec();

    if (ret == (int) QMessageBox::AcceptRole)
        event->accept();
}

//!
//! \brief MainWindow::keyPressEvent przechwytywanie klawisza
//! \param event
//!
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_F:
        if (event->modifiers() == Qt::ControlModifier)
        {
            this->setFullScreen(!this->_fullscreen);
            event->accept();
        }
        break;
    case Qt::Key_Return:
    case Qt::Key_Enter:
        if (event->modifiers() == Qt::AltModifier)
        {
            this->setFullScreen(!this->_fullscreen);
            event->accept();
        }
        break;
    case Qt::Key_Escape:
        if (this->_fullscreen)
        {
            this->setFullScreen(!this->_fullscreen);
            event->accept();
        }
    default:
        break;
    }
}

//!
//! \brief MainWindow::changeEvent wychwytywanie zmiany języka w głównym oknie
//! \param event zdarzenie typu ChangeEvent
//!
void MainWindow::changeEvent(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::LanguageChange :
        this->ui->retranslateUi(this);
        break;
    case QEvent::WindowStateChange:
        if(!this->isMinimized())
            break;
        this->_trayIcon->show();
        break;
    default:
    {

    }
    }

    QMainWindow::changeEvent(event);
}

//!
//! \brief MainWindow::updateGlobalSetting aktualizacja ustawień globalnych z pliku INI
//! \param key klucz
//! \param value wartość
//!
void MainWindow::updateGlobalSetting(const QString &key, const QVariant &value)
{
    QSettings mainSettings(qApp->applicationName() + ".ini", QSettings::IniFormat);

    mainSettings.beginGroup(qApp->applicationName());
    mainSettings.setValue(key, value);
    mainSettings.endGroup();
    mainSettings.sync();
}

//!
//! \brief MainWindow::setOptionsEnabled ustawianie / wyłączanie widgetów
//! \param enable opcja włączenia
//!
void MainWindow::setOptionsEnabled(bool enable)
{
    this->centralWidget()->setEnabled(enable);
    this->ui->menuBar->setEnabled(enable);
    this->ui->configurationToolBar->setEnabled(enable);
    this->ui->treeViewDockWidget->setEnabled(enable);
    this->ui->objectInformationDockWidget->setEnabled(enable);
    this->ui->messagesDockWidget->setEnabled(enable);
    this->ui->simulationToolBar->setEnabled(enable);
    this->ui->zoomToolBar->setEnabled(enable);
}
